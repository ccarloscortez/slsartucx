FROM frolvlad/alpine-python-machinelearning

RUN apk add --no-cache --virtual=.build-dependencies g++ gfortran file binutils  musl-dev python3-dev openblas-dev && apk add libstdc++ openblas


RUN apk add --no-cache gcc python3-dev musl-dev postgresql-dev
RUN pip install --upgrade pip
RUN apk update \
&& apk upgrade \
&& apk add --no-cache bash \
&& apk add --no-cache --virtual=build-dependencies unzip \
&& apk add --no-cache curl \
&& apk add --no-cache openjdk8-jre
RUN echo "http://dl-8.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
RUN apk --no-cache --update-cache add gcc gfortran python python-dev py-pip build-base wget freetype-dev libpng-dev openblas-dev
RUN ln -s /usr/include/locale.h /usr/include/xlocale.h

RUN apk add --no-cache wget ca-certificates openssl git tar openssh
RUN rm -rf /var/cache/apk/*
RUN mkdir -p /root/.ssh
RUN echo -e "Host github.com\n\tStrictHostKeyChecking no\n" >> /root/.ssh/config
RUN git clone https://github.com/awslabs/amazon-kinesis-client-python.git

WORKDIR /amazon-kinesis-client-python
RUN python3 setup.py download_jars
RUN python3 setup.py install


# Setup flask application
ADD workers /workers
ADD src/common /src/common

WORKDIR /workers
# https://stackoverflow.com/a/42334357
RUN pip3 install certifi
RUN pip3 install -r /workers/requirements.txt
RUN chmod +x /workers/cx_train_model.sh
RUN chmod +x /workers/cx_train_model_properties.py
RUN chmod +x /workers/cx_train_model.py
ENTRYPOINT ["/workers/cx_train_model.sh"]