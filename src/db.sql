--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4 (Debian 10.4-2.pgdg90+1)
-- Dumped by pg_dump version 10.4 (Ubuntu 10.4-2.pgdg16.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cx_campaign; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cx_campaign (
    id_campaign character varying(255) NOT NULL,
    id_segment character varying(255),
    creation_time date,
    name character varying(255),
    "desc" character varying(255),
    email_title character varying(255),
    email_body character varying(255),
    email_images jsonb,
    frequency character varying(255),
    start_time date,
    end_time date,
    timezone character varying,
    id_cliente character varying(255),
    id_usuario character varying(255),
    id_mailtemplate character varying(255),
    replacement_data_url character varying(255),
    schedule_expression character varying,
    state boolean DEFAULT false NOT NULL
);


ALTER TABLE public.cx_campaign OWNER TO postgres;

--
-- Name: cx_campaign_stats; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cx_campaign_stats (
    id_c_stat character varying(255) NOT NULL,
    name character varying(255),
    id_campaign character varying(255),
    open_rate character varying(255),
    delivered character varying(255),
    id_cliente character varying(255),
    id_usuario character varying(255)
);


ALTER TABLE public.cx_campaign_stats OWNER TO postgres;

--
-- Name: cx_client_model; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cx_client_model (
    id_cliente character varying(255) NOT NULL,
    id_model character varying(255) NOT NULL,
    addition_date date,
    addition_user character varying(255)
);


ALTER TABLE public.cx_client_model OWNER TO postgres;

--
-- Name: cx_mailservice; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cx_mailservice (
    token_id character varying(255),
    id_cliente character varying(255),
    id_campaign character varying(255),
    server_token character varying(255),
    creation_time timestamp without time zone
);


ALTER TABLE public.cx_mailservice OWNER TO postgres;

--
-- Name: cx_mailtemplate; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cx_mailtemplate (
    id_mailtemplate character varying(255) NOT NULL,
    name character varying(255),
    "desc" character varying(255),
    creation_date date,
    title character varying(255),
    body character varying(255),
    images jsonb,
    id_cliente character varying(255),
    id_usuario character varying(255)
);


ALTER TABLE public.cx_mailtemplate OWNER TO postgres;

--
-- Name: cx_marketplace; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cx_marketplace (
    id_marketplace character varying(20) NOT NULL,
    name character varying(20),
    id_app character varying(20),
    id_model character varying(255),
    creation_time date
);


ALTER TABLE public.cx_marketplace OWNER TO postgres;

--
-- Name: cx_ml_metric; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cx_ml_metric (
    id_metric character varying(255) NOT NULL,
    type character varying(255),
    accuracy character varying(255),
    fscore character varying(255),
    recall character varying(255),
    "precision" character varying(255),
    roc character varying(255),
    confusion_matrix jsonb,
    roc_curve character varying,
    importances character varying
);


ALTER TABLE public.cx_ml_metric OWNER TO postgres;

--
-- Name: cx_ml_statistic; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cx_ml_statistic (
    id_statistic character varying(255) NOT NULL,
    report_name character varying(255),
    report_desc character varying(255),
    total_entries character varying(255),
    positive_level_name character varying(255),
    negative_level_name character varying(255),
    positive_level character varying(255),
    negative_level character varying(255),
    positive_level_csv character varying(255),
    negative_level_csv character varying(255),
    id_cliente character varying(255),
    id_usuario character varying(255)
);


ALTER TABLE public.cx_ml_statistic OWNER TO postgres;

--
-- Name: cx_model; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cx_model (
    id_model character varying(255) NOT NULL,
    category character varying(255),
    version character varying(255),
    "desc" json,
    industry character varying(255),
    name character varying(255),
    creation_time date
);


ALTER TABLE public.cx_model OWNER TO postgres;

--
-- Name: cx_notification_change; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cx_notification_change (
    id_notification_change character varying(255) NOT NULL,
    entity_type character varying(255),
    entity_id character varying(255),
    action_on_entity character varying(255),
    actor_id_usuario character varying(255),
    actor_id_cliente character varying(255),
    created_at timestamp without time zone,
    entity_name character varying(255)
);


ALTER TABLE public.cx_notification_change OWNER TO postgres;

--
-- Name: cx_prediction; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cx_prediction (
    id_prediction character varying(255) NOT NULL,
    id_training character varying(255),
    name character varying(255),
    creation_time date,
    on_dashboard boolean,
    id_job character varying(255),
    job_type character varying(255),
    job_start_time date,
    job_state character varying(255),
    input_csv_url character varying(255),
    output_csv_url character varying(255),
    job_finish_time date,
    id_cliente character varying(255),
    id_usuario character varying(255),
    id_statistic character varying(255)
);


ALTER TABLE public.cx_prediction OWNER TO postgres;

--
-- Name: cx_project; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cx_project (
    id_project character varying(255) NOT NULL,
    name character varying(255),
    "desc" character varying(255),
    creation_date date,
    id_cliente character varying(255),
    id_usuario character varying(255),
    from_address character varying(255)
);


ALTER TABLE public.cx_project OWNER TO postgres;

--
-- Name: cx_read_notification; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cx_read_notification (
    id_read_notification character varying(255) NOT NULL,
    id_usuario character varying(255),
    id_notification_change character varying(255),
    read_at timestamp without time zone
);


ALTER TABLE public.cx_read_notification OWNER TO postgres;

--
-- Name: cx_segment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cx_segment (
    id_segment character varying(255) NOT NULL,
    id_project character varying(255),
    name character varying(255),
    "desc" character varying(255),
    campaign_input_url character varying(255),
    total_entries character varying(255),
    creation_time date,
    id_cliente character varying(255),
    id_usuario character varying(255),
    channel_type character varying(255)
);


ALTER TABLE public.cx_segment OWNER TO postgres;

--
-- Name: cx_training; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cx_training (
    id_training character varying(255) NOT NULL,
    id_model character varying(255),
    state character varying(255),
    type character varying(255),
    subtype character varying(255),
    modeling_data jsonb,
    is_free character varying(255),
    id_cliente character varying(255),
    id_usuario character varying(255),
    id_metric character varying(255),
    creation_time date,
    name character varying(255),
    dataset_url_training character varying(255),
    saved_model character varying(255),
    training_label character varying(255)
);


ALTER TABLE public.cx_training OWNER TO postgres;

--
-- Data for Name: cx_campaign; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cx_campaign (id_campaign, id_segment, creation_time, name, "desc", email_title, email_body, email_images, frequency, start_time, end_time, timezone, id_cliente, id_usuario, id_mailtemplate, replacement_data_url, schedule_expression, state) FROM stdin;
cxcamp001	cxseg001	2018-06-17	churn-testing	churn 2018 06 17	hola esto es una prueba	<html> HOLA  que tal </html>	{}	weekly	2018-06-19	2018-06-19	UTC-4	vidasecurity	ccortez	cxtemplate001	{"name": {"client": "carlos"}}	\N	f
01CHCTZRNGXEKN460TV8RQ02FC	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHCVF60GRA4HJ31HCP0E7HNZ	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHCVFV66S5TQ3RQ47A94EKMX	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHCVGWS9TZWJYGRWMDD1QG3S	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHCVHAAAJXHZ97ZCHWH7YAWS	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHCVQFZ1KKTF02N5M27WH39E	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHCVW4683AJPTQJMTF3XJGXE	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHCYZ8RDM24D3HAMYHKEHE0Y	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD03C1P7WQNEJ5HPFNQM41N	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD03SWMC3Y5RQVGB3JQXHHT	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD047G0KGN6JWTQK0BAT5K1	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD04MQFHG17S9ERAW3J0K9J	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD09E4JQW3R0CCY9KJ3V3EV	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD09ZFKQW473D8N2A5JY87X	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD0AT7JJC74E68Y0C1RA53D	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD0C0VAPY76W6Q2S3JD74NM	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD0CMRDA8D62WYEPEE5Q55D	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD0CZ9JSZ13W81RJE8XMRRE	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD0DVZPAT5EZZ5RXA6NMJV3	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD0EB281R6KCAS3BXXFAHPW	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD0F4A0Z9KH5D8M5Q3GTAP6	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD0GQ90KB06ZX9K073FT25K	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD0JNFB4K0HZKBSEQFT1FVK	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD0KVF4G3QTF8Y6KW8W8GST	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD0M8A6A57HDG8CTY8P5Q9Y	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD0MEABBGTCZSKSRPGE73NX	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD0QM88H6GS3KRJYTP0WF32	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD0QWYHCRFC75BHVDZ9MCC2	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD0SKA9G6EKRK486BY8NJQP	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD0T9RZWPAR7MA20JS76RW1	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD0VTQR2K3BPDYZMJ9PM97C	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHD10PK05W8NX2HR8FT1R7GD	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHDBBY0A3WXDRPJ92W50C7SN	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHDBK9Y2WS3FVRPWRN4BWERX	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHDBKPF47YEMM0JDXAMHZDES	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHDBVMF90FY52AK5BF1CQN92	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHDBVSK75CG076C32TXY9F5A	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHDBVZ2T1VMXBETJC0AM50AS	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHDBY11NYP5CGGSJXZ2YJJCY	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHDBYB2T9HX0K5VD6NRZM3Y7	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	f
01CHDX9182DK13ESW6XVP68VE6	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	t
01CHQMX432AQW2DYA8K4SGC7PP	cxseg001	2018-07-06	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	t
01CHDX9BNWWG75KQB0HQT85AXS	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	t
01CHEZ9GY30DQD4R2Z4293MGDT	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	t
01CHEZ9VFHPSHKWBG6VE3FF305	cxseg001	2018-07-02	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	t
01CHQMW62X492ZNS2DFV66G6P8	cxseg001	2018-07-06	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	t
01CHQMWSGDF9G0JPB1PHJD0R5A	cxseg001	2018-07-06	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	t
01CHX2V8G67YB0X6BYM035H62K	cxseg001	2018-07-08	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	t
01CHX2T0NWA7TF169HGZVMPZ3D	cxseg001	2018-07-08	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	t
01CHX31C5P1XKNJDM7VFZS3B7T	cxseg001	2018-07-08	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	t
01CHX331B1S8TMPN1Z83PA5N3F	cxseg001	2018-07-08	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	t
01CHX33BGW12M0FCFNWXBEZWHR	cxseg001	2018-07-08	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * ? *)	t
01CMRR8BSS4WH8RY7N89F0N8G0	cxseg001	2018-08-12	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CMRRAD7AZDY8WP17ZPJBZ8FV	cxseg001	2018-08-12	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CMS2MP1XMDXEWQXXJT43F6GZ	cxseg001	2018-08-13	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CMS3J1VH6PPFYYBEE6RWSVSR	cxseg001	2018-08-13	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CMS3SM0TH5HSMTMH8S5DFQ9T	cxseg001	2018-08-13	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CP0BVMHM9DW7WEHHVSX9684B	cxseg001	2018-08-28	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CP0BXMESHBM3Z29E3131C5MF	cxseg001	2018-08-28	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CP0BYMM6K85BEP0NV0CBB8XB	cxseg001	2018-08-28	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CP0BZW0EFK72KE5X7RF23P9X	cxseg001	2018-08-28	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CP0D05917E2C41Q7TNY9D05A	cxseg001	2018-08-28	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CP0D2MZ5KEMH363FKR9HGJT5	cxseg001	2018-08-28	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CP0D472Z269R5ARPD7X6J9YD	cxseg001	2018-08-28	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CP0D8DTYJ26KH21TDMBQ8AYB	cxseg001	2018-08-28	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CP0E42J9PBG94D8BCQR3ZMG9	cxseg001	2018-08-28	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CP0ET2QXQPWJR6VAF3N6THDR	cxseg001	2018-08-28	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CP0EV5R6B5XK86XP56QWVC51	cxseg001	2018-08-28	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CP0EW0WA961RCN2QABE7MNNT	cxseg001	2018-08-28	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CP0EZHXSW1E3RKYH5EHD0WR8	cxseg001	2018-08-28	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CP0F383NN678JPDR6GJ1TA9W	cxseg001	2018-08-28	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CP0FMTY47GD65N2BKF5MRYYD	cxseg001	2018-08-28	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CP0FQ44QVMQEV3VVCA9FY9ZN	cxseg001	2018-08-28	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CP0FRQG6FW0ZZDD2BXR08K5N	cxseg001	2018-08-28	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
01CP0G06GQG9B2Z1DGDQTGKGCA	cxseg001	2018-08-28	campaign_test		\N	\N	{"img1": "url1", "img2": "ulr2"}	daily	2018-06-24	\N	\N	vidasecurity	ccortez	cxtemplate001	s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json	cron(0 10 * * 1,2,3,4,5,6,7 *)	t
\.


--
-- Data for Name: cx_campaign_stats; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cx_campaign_stats (id_c_stat, name, id_campaign, open_rate, delivered, id_cliente, id_usuario) FROM stdin;
cxcampstat001	reporte-churn	cxcamp001	0.78	1	vidasecurity	ccortez
\.


--
-- Data for Name: cx_client_model; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cx_client_model (id_cliente, id_model, addition_date, addition_user) FROM stdin;
vidasecurity	cxm001	2018-06-24	ccortez
\.


--
-- Data for Name: cx_mailservice; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cx_mailservice (token_id, id_cliente, id_campaign, server_token, creation_time) FROM stdin;
4216426	vidasecurity	01CNY3RXC37GA50H3PJG5PKWW8	36d0e25f-04af-41c3-8e3b-a1d0d1a91827	2018-08-27 11:34:23.17298
4216438	vidasecurity	01CNY46M3PFJP73QPNKAXKCSYM	5760cb49-96a1-44b5-8c95-5eabe0283af5	2018-08-27 11:41:57.475118
4216440	vidasecurity	01CNY4AHQTDGA1E4ABZHGR3P42	6e54aa3b-88a1-4619-bf43-2e5836591874	2018-08-27 11:44:01.208289
4216442	vidasecurity	01CNY4HAE05GSM2E8P56V6H5HK	47a34fa2-a093-4bf4-a252-45a72c715419	2018-08-27 11:47:43.415184
4216444	vidasecurity	01CNY4JPK25JCCX6BYDXESYDSA	6043b0a4-3d6f-4a9a-95e2-15fda2080df4	2018-08-27 11:48:28.398298
4216445	vidasecurity	01CNY4KN5CEQXPP295KP9X8WGR	4edb7024-73f1-4d18-b019-d400fec9fdc8	2018-08-27 11:48:59.625849
\.


--
-- Data for Name: cx_mailtemplate; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cx_mailtemplate (id_mailtemplate, name, "desc", creation_date, title, body, images, id_cliente, id_usuario) FROM stdin;
7271921	ejemplo de campaña producto	""	2018-07-02	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
7271886	ejemplo de campaña producto	""	2018-07-02	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CHD7AN3KYV102PWGXJ2PR8DM	ejemplo de campaña producto	""	2018-07-02	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CHD7ARC4TXCYFYP28Q4R0R8E	ejemplo de campaña producto	""	2018-07-02	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CHD7DRAMCRQ28CWTAA3RGAF6	ejemplo de campaña producto	""	2018-07-02	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CHD7PZK47MYJYVWY7X290W79	ejemplo de campaña producto	""	2018-07-02	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CHD7TJA1HXRA3XN873FDVGFS	ejemplo de campaña producto	""	2018-07-02	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CHDV4Q06H8N423TTTCWH81SM	ejemplo de campaña producto	""	2018-07-02	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CHEVHGXT66ZNMH3H3Z8A22XS	ejemplo de campaña producto	"new template"	2018-07-02	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CHEVJP5Z752D65PV7GMHQ83G	ejemplo de campaña producto	"new template"	2018-07-02	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CHEVKV391D1RJP96CY8J3TWS	ejemplo de campaña producto	"new template"	2018-07-02	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CHEVNXTYZNC6P72ZCWG3T2ZY	ejemplo de campaña producto	"new template"	2018-07-02	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CHEVPRF47SKMG4AB78MY5ARY	ejemplo de campaña producto	"new template"	2018-07-02	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CHEVSTE7YHDCYBM1DKQWM09Z	ejemplo de campaña producto	"new template"	2018-07-02	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CHEVWRXVZYEB0S96JKW0FKHT	ejemplo de campaña producto	new template	2018-07-02	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CHEVXK3R3ZRCZ0YC9MZGQXJR	ejemplo de campaña producto	new template	2018-07-02	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CMR2PM9ZB07JJK6A39MNTQNN	ejemplo de campaña producto	new template	2018-08-12	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CMR2QVY31NKFV25PQ842ESTM	ejemplo de campaña producto	new template	2018-08-12	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CMR2RFYQZ7DHGVBC492AXGA4	ejemplo de campaña producto	new template	2018-08-12	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CMR2S1025RQHG58PF60PT6JG	ejemplo de campaña producto	new template	2018-08-12	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CMR68CTV5PH30MK8ZXKMSB51	ejemplo de campaña producto	new template	2018-08-12	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CMR6A83VX23F3KV3BHT6WD5C	ejemplo de campaña producto	new template	2018-08-12	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CMR6BDQJA992040N4DT8GZN1	ejemplo de campaña producto	new template	2018-08-12	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CMR6CAJHH8TC8N9BW93F0KDR	ejemplo de campaña producto	new template	2018-08-12	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CMR6F3DN4RMHADPCZSD8NSAN	ejemplo de campaña producto	new template	2018-08-12	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CMR6FH3P2N9TC33GAKJS818D	ejemplo de campaña producto	new template	2018-08-12	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CMRHX6FASM3R8SZAH88DQXYH	ejemplo de campaña producto	new template	2018-08-12	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CMRNB20NHMEPDWJNAJQ95WTY	ejemplo de campaña producto	new template	2018-08-12	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CMRNDFPBZ9KRAWG08KNXACYX	ejemplo de campaña producto	new template	2018-08-12	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CMRNGNDAWW6EMBER9J5DXKH6	ejemplo de campaña producto	new template	2018-08-12	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CMRNJSSZRMF7RGRY55GY90BX	ejemplo de campaña producto	new template	2018-08-12	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CMRP2BJMHFSKZGXYKZ5YH40H	ejemplo de campaña producto	new template	2018-08-12	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CMRPX82QN95HER6AG0NHHZCC	ejemplo de campaña producto	new template	2018-08-12	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CMRQ01C3HFSD6FF6KPTCT75N	ejemplo de campaña producto	new template	2018-08-12	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CMRQKS4CDQ2QX00S1KXJ3WAY	ejemplo de campaña producto	new template	2018-08-12	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CMRQSNEDB6AH5G65JF21WSTX	ejemplo de campaña producto	new template	2018-08-12	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
01CMRQSZ95NDFD0V67MR0B33R0	ejemplo de campaña producto		2018-08-12	Te saludamos de {{company.name}}!	<html><body>Hola, {{name}}!<a href="http://artucx-eng.s3-website-us-east-1.amazonaws.com">Visita Nuestro Producto Artu CX</a></body></html>	\N	vidasecurity	ccortez
cxtemplate001	cambio	testing churn	2018-06-17	Ofertas fin de mes	<html> hola {{name.client}}, estas son nuestras ofertas</html>	{"img1": "url1", "img2": "url2"}	vidasecurity	ccortez
\.


--
-- Data for Name: cx_marketplace; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cx_marketplace (id_marketplace, name, id_app, id_model, creation_time) FROM stdin;
cxmkt001	artucx	artucx	cxm001	\N
cxmkt002	cobranza	artucobranza	cxm001	\N
\.


--
-- Data for Name: cx_ml_metric; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cx_ml_metric (id_metric, type, accuracy, fscore, recall, "precision", roc, confusion_matrix, roc_curve, importances) FROM stdin;
cxmetric001	training	0.9179	0.9002	0.9179	0.9401	0.9179	{"fn": "23", "fp": "57", "tn": "533", "tp": "361"}	\N	\N
01CNWAZ23GJKXTQ69R95HSV2G5	training	0.745275267050123	0.823361823361823	0.977672530446549	0.711122047244094	0.385983263598326	{"fn": 33.0, "fp": 587.0, "tn": 369.0, "tp": 1445.0}	{"fpr": [0.0, 0.6140167364016736, 1.0], "tpr": [0.0, 0.9776725304465493, 1.0], "aoc": 0.6818278970224378}	{"TIPOOPERACION_T": 0.05743269808737461, "FECFINVIG_2017/01/31": 0.0014055734992900833, "FECFINVIG_2017/02/28": 0.003115145771221116, "FECFINVIG_2017/03/31": 0.0009155445376085445, "FECFINVIG_2017/04/30": 0.0003172198034427282, "FECFINVIG_2017/05/31": 0.0007985679938181261, "FECFINVIG_2017/06/30": 0.0009178321327266247, "FECFINVIG_2017/07/31": 0.000689300396982987, "FECFINVIG_2017/08/31": 0.006421512090451385, "FECFINVIG_2017/09/30": 0.00022528731811066976, "FECFINVIG_2017/10/31": 0.0003057080714454948, "FECFINVIG_2017/11/30": 0.00019855062972325442, "FECFINVIG_2017/12/31": 0.02006386912480905, "FECFINVIG_2018/01/31": 0.016632592390159538, "FECFINVIG_2018/02/28": 0.006253041360359853, "FECFINVIG_2018/03/31": 0.006147463090566787, "FECFINVIG_2018/04/30": 0.011162235902899099, "FECFINVIG_2018/05/31": 0.015095367480307804, "FECFINVIG_2018/06/30": 0.00020983696449492712, "FECFINVIG_2018/07/31": 0.0015193140957510645, "FECFINVIG_2018/08/31": 0.00024441367591330793, "FECFINVIG_2018/09/30": 0.00044472142462021656, "FECFINVIG_2018/10/31": 0.00020439677595454785, "FECFINVIG_2018/11/30": 7.631467522361274e-05, "FECFINVIG_2018/12/31": 0.03324025257229115, "FECFINVIG_2019/01/31": 0.028138611976015776, "FECFINVIG_2019/02/28": 0.005129335094783106, "FECFINVIG_2019/03/31": 0.011964642483320145, "FECFINVIG_2019/04/30": 0.010998760340757306, "FECFINVIG_2019/05/31": 0.0215673986331051, "FECHA_INICIO_INTERMEDIARIO_1999/10/01": 0.010131636852705772, "FECHA_INICIO_INTERMEDIARIO_2001/01/01": 6.746532484184683e-05, "FECHA_INICIO_INTERMEDIARIO_2001/07/01": 0.0001945342244396237, "FECHA_INICIO_INTERMEDIARIO_2002/04/01": 0.0005378025344035422, "FECHA_INICIO_INTERMEDIARIO_2003/04/01": 0.00028449976356370176, "FECHA_INICIO_INTERMEDIARIO_2003/05/01": 0.000543760905676903, "FECHA_INICIO_INTERMEDIARIO_2003/06/01": 0.00021115177234286395, "FECHA_INICIO_INTERMEDIARIO_2003/08/01": 6.314254446271959e-05, "FECHA_INICIO_INTERMEDIARIO_2003/10/01": 2.237177653784503e-06, "FECHA_INICIO_INTERMEDIARIO_2003/12/01": 5.7605815274353664e-05, "FECHA_INICIO_INTERMEDIARIO_2004/04/01": 1.2437315399700145e-05, "FECHA_INICIO_INTERMEDIARIO_2004/06/01": 5.6624609003990445e-06, "FECHA_INICIO_INTERMEDIARIO_2004/07/01": 2.744402053802075e-05, "FECHA_INICIO_INTERMEDIARIO_2004/08/01": 0.00017265784569048517, "FECHA_INICIO_INTERMEDIARIO_2004/09/01": 2.04325645136533e-05, "FECHA_INICIO_INTERMEDIARIO_2004/10/01": 3.338413370100087e-06, "FECHA_INICIO_INTERMEDIARIO_2004/11/01": 0.008020752873487464, "FECHA_INICIO_INTERMEDIARIO_2005/04/01": 0.0, "FECHA_INICIO_INTERMEDIARIO_2005/05/01": 5.490574351902062e-05, "FECHA_INICIO_INTERMEDIARIO_2005/06/01": 4.50575200858557e-10, "FECHA_INICIO_INTERMEDIARIO_2005/07/01": 0.00019710768375807403, "FECHA_INICIO_INTERMEDIARIO_2005/09/01": 0.0, "FECHA_INICIO_INTERMEDIARIO_2005/10/01": 0.0, "FECHA_INICIO_INTERMEDIARIO_2005/12/01": 0.0, "FECHA_INICIO_INTERMEDIARIO_2006/02/01": 0.0001983112854286097, "FECHA_INICIO_INTERMEDIARIO_2006/03/01": 0.00014984172930254105, "FECHA_INICIO_INTERMEDIARIO_2006/04/01": 2.6632031532798967e-05, "FECHA_INICIO_INTERMEDIARIO_2006/05/01": 0.00047913273830744996, "FECHA_INICIO_INTERMEDIARIO_2007/02/01": 0.0010715944766816236, "FECHA_INICIO_INTERMEDIARIO_2007/03/01": 0.0, "FECHA_INICIO_INTERMEDIARIO_2007/04/02": 6.76344445724013e-05, "FECHA_INICIO_INTERMEDIARIO_2007/06/01": 0.000129157101161235, "FECHA_INICIO_INTERMEDIARIO_2007/10/01": 0.0005399257891531644, "FECHA_INICIO_INTERMEDIARIO_2007/11/01": 0.0002283340011106623, "FECHA_INICIO_INTERMEDIARIO_2008/03/01": 0.00017995573312090535, "FECHA_INICIO_INTERMEDIARIO_2008/07/01": 4.055181880541318e-05, "FECHA_INICIO_INTERMEDIARIO_2009/05/27": 0.0003199170477839976, "FECHA_INICIO_INTERMEDIARIO_2009/08/01": 8.604917372952078e-05, "FECHA_INICIO_INTERMEDIARIO_2009/12/01": 0.00011972319429783133, "FECHA_INICIO_INTERMEDIARIO_2010/01/14": 0.0, "FECHA_INICIO_INTERMEDIARIO_2010/03/15": 0.0, "FECHA_INICIO_INTERMEDIARIO_2010/04/01": 0.0, "FECHA_INICIO_INTERMEDIARIO_2010/04/12": 0.00262105905338592, "FECHA_INICIO_INTERMEDIARIO_2010/05/01": 0.00038635156040068663, "FECHA_INICIO_INTERMEDIARIO_2010/05/25": 0.0010833657370779531, "FECHA_INICIO_INTERMEDIARIO_2010/06/01": 4.500730604708433e-06, "FECHA_INICIO_INTERMEDIARIO_2010/08/01": 0.00011173441517079993, "FECHA_INICIO_INTERMEDIARIO_2010/09/01": 0.0, "FECHA_INICIO_INTERMEDIARIO_2010/10/01": 0.0, "FECHA_INICIO_INTERMEDIARIO_2010/10/05": 0.0, "FECHA_INICIO_INTERMEDIARIO_2010/11/01": 3.8489325381506586e-05, "FECHA_INICIO_INTERMEDIARIO_2010/12/01": 1.1358964611041415e-05, "FECHA_INICIO_INTERMEDIARIO_2011/03/01": 0.0003403872809877765, "FECHA_INICIO_INTERMEDIARIO_2011/04/01": 0.000384961585308551, "FECHA_INICIO_INTERMEDIARIO_2011/04/27": 4.966446075380223e-05, "FECHA_INICIO_INTERMEDIARIO_2011/05/01": 0.00015148508259234825, "FECHA_INICIO_INTERMEDIARIO_2011/06/02": 6.773569034592826e-05, "FECHA_INICIO_INTERMEDIARIO_2011/08/01": 1.9696801229119437e-06, "FECHA_INICIO_INTERMEDIARIO_2011/08/02": 0.0, "FECHA_INICIO_INTERMEDIARIO_2011/09/01": 5.191448270446937e-07, "FECHA_INICIO_INTERMEDIARIO_2011/10/01": 0.0, "FECHA_INICIO_INTERMEDIARIO_2011/11/01": 0.00010134526157567312, "FECHA_INICIO_INTERMEDIARIO_2011/11/14": 0.0009034917140506915, "FECHA_INICIO_INTERMEDIARIO_2012/01/01": 0.0001569258898601641, "FECHA_INICIO_INTERMEDIARIO_2012/03/01": 0.0018730448689701692, "FECHA_INICIO_INTERMEDIARIO_2012/04/01": 0.00018997646299193763, "FECHA_INICIO_INTERMEDIARIO_2012/04/10": 0.0, "FECHA_INICIO_INTERMEDIARIO_2012/05/01": 0.0019320344746994729, "FECHA_INICIO_INTERMEDIARIO_2012/06/01": 0.0006469684329680419, "FECHA_INICIO_INTERMEDIARIO_2012/07/19": 2.74339676601274e-06, "FECHA_INICIO_INTERMEDIARIO_2012/08/01": 0.0001838032267367109, "FECHA_INICIO_INTERMEDIARIO_2012/08/10": 4.832894950779646e-05, "FECHA_INICIO_INTERMEDIARIO_2012/09/01": 0.00011072482035570498, "FECHA_INICIO_INTERMEDIARIO_2012/10/01": 0.0, "FECHA_INICIO_INTERMEDIARIO_2012/11/01": 7.019077355612737e-05, "FECHA_INICIO_INTERMEDIARIO_2012/11/05": 0.0016568510288206975, "FECHA_INICIO_INTERMEDIARIO_2013/01/01": 0.0, "FECHA_INICIO_INTERMEDIARIO_2013/01/16": 0.0, "FECHA_INICIO_INTERMEDIARIO_2013/03/01": 0.002176283918067852, "FECHA_INICIO_INTERMEDIARIO_2013/05/01": 2.4468725086572093e-05, "FECHA_INICIO_INTERMEDIARIO_2013/05/06": 0.00021791978550417954, "FECHA_INICIO_INTERMEDIARIO_2013/06/01": 0.00026808547067863386, "FECHA_INICIO_INTERMEDIARIO_2013/06/14": 0.0, "FECHA_INICIO_INTERMEDIARIO_2013/07/01": 0.0, "FECHA_INICIO_INTERMEDIARIO_2013/08/01": 0.0008872483766928396, "FECHA_INICIO_INTERMEDIARIO_2013/09/01": 0.00019453589461426534, "FECHA_INICIO_INTERMEDIARIO_2013/09/02": 0.0005242774767706895, "FECHA_INICIO_INTERMEDIARIO_2013/09/30": 0.0, "FECHA_INICIO_INTERMEDIARIO_2013/10/01": 6.892373635636811e-05, "FECHA_INICIO_INTERMEDIARIO_2013/11/01": 0.0002084055601713797, "FECHA_INICIO_INTERMEDIARIO_2013/12/01": 9.478090574070348e-06, "FECHA_INICIO_INTERMEDIARIO_2014/01/01": 7.969966507587378e-05, "FECHA_INICIO_INTERMEDIARIO_2014/01/08": 0.01778254983195412, "FECHA_INICIO_INTERMEDIARIO_2014/02/01": 1.7309564300201195e-05, "FECHA_INICIO_INTERMEDIARIO_2014/03/01": 0.0003456928238000807, "FECHA_INICIO_INTERMEDIARIO_2014/03/28": 5.473340109958498e-05, "FECHA_INICIO_INTERMEDIARIO_2014/04/01": 0.009168576698856951, "FECHA_INICIO_INTERMEDIARIO_2014/04/11": 0.029242158079966357, "FECHA_INICIO_INTERMEDIARIO_2014/04/14": 0.009655695169780126, "FECHA_INICIO_INTERMEDIARIO_2014/04/21": 0.0, "FECHA_INICIO_INTERMEDIARIO_2014/05/01": 6.582608052141312e-07, "FECHA_INICIO_INTERMEDIARIO_2014/05/15": 0.0070912181031942976, "FECHA_INICIO_INTERMEDIARIO_2014/07/01": 0.00014411927532133112, "FECHA_INICIO_INTERMEDIARIO_2014/07/22": 0.003293445426525322, "FECHA_INICIO_INTERMEDIARIO_2014/09/01": 0.00020604615195023125, "FECHA_INICIO_INTERMEDIARIO_2014/10/01": 0.00013031174360037857, "FECHA_INICIO_INTERMEDIARIO_2014/10/27": 0.0, "FECHA_INICIO_INTERMEDIARIO_2014/11/01": 3.960304771867631e-07, "FECHA_INICIO_INTERMEDIARIO_2014/12/01": 9.640323958486972e-05, "FECHA_INICIO_INTERMEDIARIO_2014/12/17": 0.0, "FECHA_INICIO_INTERMEDIARIO_2015/01/01": 0.0, "FECHA_INICIO_INTERMEDIARIO_2015/03/01": 9.66155207140872e-05, "FECHA_INICIO_INTERMEDIARIO_2015/03/02": 0.0029739864970368935, "FECHA_INICIO_INTERMEDIARIO_2015/04/01": 0.0005803252465147599, "FECHA_INICIO_INTERMEDIARIO_2015/05/01": 0.00030255083844970865, "FECHA_INICIO_INTERMEDIARIO_2015/05/11": 1.548986915185883e-05, "FECHA_INICIO_INTERMEDIARIO_2015/05/15": 0.0, "FECHA_INICIO_INTERMEDIARIO_2015/06/01": 0.00027674286821182124, "FECHA_INICIO_INTERMEDIARIO_2015/06/10": 0.0, "FECHA_INICIO_INTERMEDIARIO_2015/07/01": 0.00022986211219967845, "FECHA_INICIO_INTERMEDIARIO_2015/08/01": 0.0018840526043559882, "FECHA_INICIO_INTERMEDIARIO_2015/09/01": 0.0007935149961455161, "FECHA_INICIO_INTERMEDIARIO_2015/10/01": 0.03774536919886921, "FECHA_INICIO_INTERMEDIARIO_2015/10/08": 0.00769303714682351, "FECHA_INICIO_INTERMEDIARIO_2015/11/01": 0.0, "FECHA_INICIO_INTERMEDIARIO_2015/11/09": 0.0, "FECHA_INICIO_INTERMEDIARIO_2015/12/01": 0.0012377175868267664, "FECHA_INICIO_INTERMEDIARIO_2015/12/22": 2.3947878276404165e-06, "FECHA_INICIO_INTERMEDIARIO_2016/01/01": 0.00014086629110640433, "FECHA_INICIO_INTERMEDIARIO_2016/01/05": 1.4147164450922122e-05, "FECHA_INICIO_INTERMEDIARIO_2016/01/11": 5.784039734052353e-06, "FECHA_INICIO_INTERMEDIARIO_2016/02/01": 9.119696954313475e-05, "FECHA_INICIO_INTERMEDIARIO_2016/03/01": 0.00010488102471226753, "FECHA_INICIO_INTERMEDIARIO_2016/03/14": 0.004156199172503814, "FECHA_INICIO_INTERMEDIARIO_2016/03/16": 0.0, "FECHA_INICIO_INTERMEDIARIO_2016/04/01": 0.00018249011594700056, "FECHA_INICIO_INTERMEDIARIO_2016/04/25": 0.0, "FECHA_INICIO_INTERMEDIARIO_2016/04/26": 0.0, "FECHA_INICIO_INTERMEDIARIO_2016/04/28": 2.6766831534296154e-06, "FECHA_INICIO_INTERMEDIARIO_2016/05/01": 0.001117130204703013, "FECHA_INICIO_INTERMEDIARIO_2016/06/01": 0.00011951453021581686, "FECHA_INICIO_INTERMEDIARIO_2016/06/10": 0.0, "FECHA_INICIO_INTERMEDIARIO_2016/06/21": 0.0, "FECHA_INICIO_INTERMEDIARIO_2016/07/01": 0.00019815447392076984, "FECHA_INICIO_INTERMEDIARIO_2016/08/08": 0.0004765976072835377, "FECHA_INICIO_INTERMEDIARIO_2016/08/25": 0.0, "FECHA_INICIO_INTERMEDIARIO_2016/08/30": 0.0, "FECHA_INICIO_INTERMEDIARIO_2016/11/11": 0.00010855818173342399, "FECHA_INICIO_INTERMEDIARIO_2016/12/30": 0.0, "FECHA_INICIO_INTERMEDIARIO_2017/03/01": 0.00020807529844677744, "FECHA_INICIO_INTERMEDIARIO_2017/04/01": 3.309092882875916e-05, "FECHA_INICIO_INTERMEDIARIO_2017/05/01": 0.0, "FECHA_INICIO_INTERMEDIARIO_2017/06/01": 0.00038318787637661624, "FECHA_INICIO_INTERMEDIARIO_2017/07/01": 0.0001777645479602761, "FECHA_INICIO_INTERMEDIARIO_2017/08/01": 0.00012961441928021085, "FECHA_INICIO_INTERMEDIARIO_2017/08/03": 0.001031647515407004, "FECHA_INICIO_INTERMEDIARIO_2017/09/01": 0.0, "FECHA_INICIO_INTERMEDIARIO_2017/10/01": 0.00010266561727409233, "FECHA_INICIO_INTERMEDIARIO_2018/02/27": 0.00023485376720058405, "FECHA_INICIO_INTERMEDIARIO_2018/04/01": 0.0005906025949241504, "PROD_9620MD0100B010": 0.0008548580934667199, "PROD_9640MD0102B001": 0.0005132881693467289, "PROD_9640MD0103B001": 0.00029026401950472914, "PROD_9640MD0104B001": 0.00010945566579867918, "PROD_9640MD0105B001": 0.016602324796158533, "PROD_9640MD0106B001": 3.3746118027003176e-06, "PROD_9640MD0107B001": 0.0007143783903110268, "PROD_9640MD0108B001": 0.0, "PROD_9660MD0100A001": 8.84985488627319e-05, "PROD_9660MD0100A002": 0.00016460061434719778, "PROD_9660MD0100A003": 0.001105188648723134, "PROD_9660MD0100B001": 0.0029927808352667126, "PROD_9660MD0100B002": 0.0055131273475392244, "PROD_9660MD0100B003": 0.00896006381349491, "PROD_9660MD0101A001": 3.0366394941847418e-05, "PROD_9660MD0101B001": 0.0005783739901225664, "PROD_9660MD0102A001": 0.0007361086359521037, "PROD_9660MD0102B001": 8.019494571844211e-05, "PROD_9730MD0100A006": 0.00020549936182809442, "PROD_9730MD0100A007": 3.372965510509308e-08, "PROD_9730MD0100B006": 0.004606625803445284, "PROD_9730MD0100B007": 0.0002852726408410019, "PROD_9740MD0102B001": 0.00042883690219582996, "PROD_9740MD0103B001": 6.218810952095476e-06, "PROD_9740MD0104B001": 7.82050782901446e-06, "PROD_9740MD0105B001": 0.0005896219629588222, "PROD_9740MD0106B001": 0.00011820134287044657, "PROD_9740MD0107B001": 0.0, "PROD_9740MD0108B001": 5.387012809403779e-05, "PROD_9750MD0100A001": 0.00044524009481651175, "PROD_9750MD0100A002": 0.0016357114945645395, "PROD_9750MD0100B001": 0.0005175173558534588, "PROD_9750MD0100B002": 0.00038599399605201415, "PROD_9750MD0101A001": 0.0, "PROD_9750MD0101B001": 3.5544662466794197e-07, "PROD_9750MD0102A001": 0.0, "PROD_9750MD0102B001": 0.0, "PROD_9820MD0100A006": 0.00030963044477521945, "PROD_9820MD0100A007": 0.04254075513000192, "NOMBREPRODUCTO_APV MAX 3.0.": 0.01850960608926492, "NOMBREPRODUCTO_DOMINIUM MAX 2.0.": 0.00042078554898983056, "NOMBREPRODUCTO_DOMINIUM MAX 3.0.": 0.006338579773087792, "NOMBREPRODUCTO_MAX PATRIMONIAL": 0.03972975275765412, "NOMBREPRODUCTO_MI APV SEGURO": 0.019986005314520557, "NOMBREPRODUCTO_MI PROYECTO SEGURO": 0.005153909352406051, "PERIDICIDADPAGO_MENSUAL": 0.040060875193575186, "PERIDICIDADPAGO_SEMESTRAL": 0.0, "PERIDICIDADPAGO_TRIMESTRAL": 0.0, "PERIDICIDADPAGO_\\u00c3\\u009aNICA (PRIMA)": 0.06074704611679829, "OPERACION_TRASPASO APV": 0.03456907960056085, "ACTIVIDADLABORAL_ACADEMICO": 0.00012028624335192163, "ACTIVIDADLABORAL_ACCOUNT MANAGER": 0.0, "ACTIVIDADLABORAL_ACTUARIO": 0.0, "ACTIVIDADLABORAL_ADMINISTRADOR": 0.002600372462778047, "ACTIVIDADLABORAL_ADMINISTRADOR DE CONTRATO": 0.0, "ACTIVIDADLABORAL_ADMINISTRADOR EMPRESAS": 0.0, "ACTIVIDADLABORAL_ADMINISTRADOR PUBLICO": 3.3451896861209446e-05, "ACTIVIDADLABORAL_ADMINISTRATIVOS": 2.6750208438138267e-05, "ACTIVIDADLABORAL_AGENTE": 6.381388289265015e-05, "ACTIVIDADLABORAL_AGENTE DE VENTAS": 0.00014420575421213402, "ACTIVIDADLABORAL_AGENTE DE VIAJES": 0.0, "ACTIVIDADLABORAL_AGRICULTOR": 0.0015305726757479034, "ACTIVIDADLABORAL_AGRONOMO": 0.0, "ACTIVIDADLABORAL_AMA DE CASA (DEL HOGAR)": 0.001815708946125473, "ACTIVIDADLABORAL_ANALISTA": 0.0022905429442329674, "ACTIVIDADLABORAL_ANALISTA CONTABLE": 0.0, "ACTIVIDADLABORAL_ANALISTA DE CONTROL": 0.0, "ACTIVIDADLABORAL_ANALISTA DE RIESGO": 3.6508826567292204e-07, "ACTIVIDADLABORAL_ANALISTA DE SISTEMA": 7.505926294316114e-05, "ACTIVIDADLABORAL_ANALISTA FINANCIERO": 0.0, "ACTIVIDADLABORAL_ANALISTA PROGRAMADOR": 0.0, "ACTIVIDADLABORAL_ANALISTA QUIMICO": 0.0, "ACTIVIDADLABORAL_ANESTESISTA": 0.0, "ACTIVIDADLABORAL_ARQUEOLOGO": 0.0, "ACTIVIDADLABORAL_ARQUITECTO": 0.00043544577732704514, "ACTIVIDADLABORAL_ARTISTA": 0.0, "ACTIVIDADLABORAL_ASESOR": 0.00031161311568474957, "ACTIVIDADLABORAL_ASESOR FINANCIERO": 0.0, "ACTIVIDADLABORAL_ASESOR JURIDICO": 0.0, "ACTIVIDADLABORAL_ASISTENTE": 0.0002658054921970071, "ACTIVIDADLABORAL_ASISTENTE COMERCIAL": 2.539879197501915e-05, "ACTIVIDADLABORAL_ASISTENTE DE GERENCIA": 0.0, "ACTIVIDADLABORAL_ASISTENTE DENTAL": 0.0, "ACTIVIDADLABORAL_ASISTENTE SOCIAL": 9.243206042480679e-05, "ACTIVIDADLABORAL_ATENCION PUBLICO": 0.0, "ACTIVIDADLABORAL_AUDITOR": 0.0010375775414863861, "ACTIVIDADLABORAL_AUXILIAR": 0.0, "ACTIVIDADLABORAL_AUXILIAR CONTABILIDAD": 0.0003337790815117953, "ACTIVIDADLABORAL_AUXILIAR DE FARMACIA": 1.097341998843839e-06, "ACTIVIDADLABORAL_AUXILIAR DE VUELO": 0.0, "ACTIVIDADLABORAL_BIBLIOTECARIO": 0.0, "ACTIVIDADLABORAL_BIBLIOTECOLOGA": 4.39852577746255e-05, "ACTIVIDADLABORAL_BIOQUIMICO": 0.0, "ACTIVIDADLABORAL_BODEGUERO": 0.0, "ACTIVIDADLABORAL_CAJERA": 0.0, "ACTIVIDADLABORAL_CARTOGRAFO": 0.0, "ACTIVIDADLABORAL_CHEF": 0.0, "ACTIVIDADLABORAL_CIRUJANO": 0.0, "ACTIVIDADLABORAL_CIRUJANO DENTISTA": 3.181321971991932e-05, "ACTIVIDADLABORAL_COBRADOR": 0.0, "ACTIVIDADLABORAL_COMERCIANTE EN VENTA": 3.2070420426911444e-06, "ACTIVIDADLABORAL_COMERCIANTE INDEPENDIENTE": 0.0, "ACTIVIDADLABORAL_COMERCIO": 0.0, "ACTIVIDADLABORAL_COMERCIO EXTERIOR": 0.0, "ACTIVIDADLABORAL_COMUNICADOR AUDIOVISUAL": 6.177816259852917e-05, "ACTIVIDADLABORAL_CONFECCION ROPA": 0.0, "ACTIVIDADLABORAL_CONSTRUCTOR CIVIL": 0.0002065668674565039, "ACTIVIDADLABORAL_CONSTRUCTORA": 0.0, "ACTIVIDADLABORAL_CONSULTOR": 0.00019934276280319485, "ACTIVIDADLABORAL_CONTADOR": 0.00108117775226738, "ACTIVIDADLABORAL_CONTADOR AUDITOR": 0.0003013465714481975, "ACTIVIDADLABORAL_CONTADOR PUBLICO Y AUDITOR": 0.0, "ACTIVIDADLABORAL_CONTRALOR": 2.1689883355505105e-07, "ACTIVIDADLABORAL_CONTRATISTA": 1.3015111934871718e-07, "ACTIVIDADLABORAL_CONTROL DE CALIDAD": 0.0, "ACTIVIDADLABORAL_CONTROL DE GESTION": 0.0, "ACTIVIDADLABORAL_CONTROLADOR DE TRANSITO A\\u00c3\\u0089REO": 0.00032096202517116213, "ACTIVIDADLABORAL_COORDINADOR": 0.00011669774635397249, "ACTIVIDADLABORAL_COORDINADOR ACADEMICO": 0.0, "ACTIVIDADLABORAL_COORDINADOR ADMINISTRATIVO": 0.0, "ACTIVIDADLABORAL_COORDINADOR PROYECTOS": 0.0, "ACTIVIDADLABORAL_CORREDOR DE PROPIEDADES": 0.0, "ACTIVIDADLABORAL_COSMETOLOGO": 0.0, "ACTIVIDADLABORAL_DECORADOR DE INTERIORES": 0.0, "ACTIVIDADLABORAL_DECORADORA": 0.0023289668164387374, "ACTIVIDADLABORAL_DEL HOGAR": 0.011231736004293126, "ACTIVIDADLABORAL_DENTISTA": 3.68010469860179e-05, "ACTIVIDADLABORAL_DESABOLLADOR": 0.0, "ACTIVIDADLABORAL_DESARROLLADOR DE SISTEMAS": 0.0, "ACTIVIDADLABORAL_DIBUJANTE": 0.0, "ACTIVIDADLABORAL_DIBUJANTE PROYECTISTA": 0.0, "ACTIVIDADLABORAL_DIGITADOR": 1.2239807683444513e-07, "ACTIVIDADLABORAL_DIRECTOR": 0.0003107449439952618, "ACTIVIDADLABORAL_DIRECTOR ACADEMICO": 1.9425186049492175e-06, "ACTIVIDADLABORAL_DISE\\u00c3\\u0091ADOR": 0.0001011306197748094, "ACTIVIDADLABORAL_DISE\\u00c3\\u0091ADOR GRAFICO": 0.0, "ACTIVIDADLABORAL_DOCENTE": 0.00029137842309959904, "ACTIVIDADLABORAL_ECONOMISTA": 0.0007667492232797032, "ACTIVIDADLABORAL_EDITOR": 0.0, "ACTIVIDADLABORAL_EDUCADOR": 2.2162770577679624e-06, "ACTIVIDADLABORAL_EDUCADORA DE PARVULO": 8.609994533145648e-07, "ACTIVIDADLABORAL_EDUCADORA DIFERENCIAL": 0.00020077160669690176, "ACTIVIDADLABORAL_EJECUTIVO": 0.0009433015299001806, "ACTIVIDADLABORAL_EJECUTIVO COMERCIAL": 0.0002355116398446245, "ACTIVIDADLABORAL_EJECUTIVO DE  CUENTAS": 0.0005422730972168561, "ACTIVIDADLABORAL_EJECUTIVO EN VENTAS": 0.0003346989740599672, "ACTIVIDADLABORAL_ELECTRICO": 0.0, "ACTIVIDADLABORAL_EMPRESARIA": 0.00031101186094670974, "ACTIVIDADLABORAL_EMPRESARIO": 0.00941997352345719, "ACTIVIDADLABORAL_ENCARGADO DE AREA": 0.0008804387583743668, "ACTIVIDADLABORAL_ENFERMERA": 0.001557713808733913, "ACTIVIDADLABORAL_ENFERMERO": 1.7901718780858272e-05, "ACTIVIDADLABORAL_ENTRENADOR": 0.0, "ACTIVIDADLABORAL_ESTETICISTA": 1.5976893026463373e-05, "ACTIVIDADLABORAL_ESTILISTA": 5.908607642676575e-05, "ACTIVIDADLABORAL_ESTUDIANTE": 0.0004130259866406944, "ACTIVIDADLABORAL_FISCAL": 0.0, "ACTIVIDADLABORAL_FISCALIZADOR": 0.0, "ACTIVIDADLABORAL_FISCALIZADOR TRIBUTARIO": 0.0, "ACTIVIDADLABORAL_FONOAUDIOLOGO": 0.00012795169108578038, "ACTIVIDADLABORAL_FOTOGRAFO": 0.0, "ACTIVIDADLABORAL_FUTBOLISTA": 5.1398682007508504e-05, "ACTIVIDADLABORAL_GEOGRAFO": 0.0, "ACTIVIDADLABORAL_GEOLOGO": 0.0, "ACTIVIDADLABORAL_GERENTE": 0.0006666488291833935, "ACTIVIDADLABORAL_GERENTE GENERAL": 0.00028030061947842915, "ACTIVIDADLABORAL_GESTOR": 0.0, "ACTIVIDADLABORAL_INDUSTRIAL": 8.70838038873432e-05, "ACTIVIDADLABORAL_INFORMATICO": 0.00037654702970296705, "ACTIVIDADLABORAL_INGENIERO": 0.0003552959727415706, "ACTIVIDADLABORAL_INGENIERO AGRONOMO": 0.0013739193754719603, "ACTIVIDADLABORAL_INGENIERO CIVIL": 0.007159681946230301, "ACTIVIDADLABORAL_INGENIERO COMERCIAL": 0.009166378986270173, "ACTIVIDADLABORAL_INGENIERO EN ADMINISTRACION DE EMPRESAS": 0.00011943652041896866, "ACTIVIDADLABORAL_INGENIERO EN PREVENCI\\u00c3\\u0093N DE RIESGO": 6.440553793092012e-05, "ACTIVIDADLABORAL_INGENIERO FORESTAL": 0.0, "ACTIVIDADLABORAL_INGENIERO INFORMATICO": 0.001848654838297295, "ACTIVIDADLABORAL_INGENIERO PROYECTOS": 1.3374987891864038e-06, "ACTIVIDADLABORAL_INSPECTOR": 0.00017916223275585974, "ACTIVIDADLABORAL_INSTRUCTOR": 0.0, "ACTIVIDADLABORAL_INVESTIGADOR": 0.00014953244302422453, "ACTIVIDADLABORAL_JEFE DE AREA": 0.005295855209111312, "ACTIVIDADLABORAL_JEFE DE CARRERA": 6.646769593840204e-06, "ACTIVIDADLABORAL_JEFE DE LOCAL": 7.192060306072518e-05, "ACTIVIDADLABORAL_JEFE DE PLANTA": 0.0, "ACTIVIDADLABORAL_JEFE DE PROYECTOS": 0.00020108834497692207, "ACTIVIDADLABORAL_JEFE DE TIENDA": 0.0, "ACTIVIDADLABORAL_JEFE ZONAL": 7.766092048031285e-05, "ACTIVIDADLABORAL_JUBILADO": 0.00945290256834773, "ACTIVIDADLABORAL_JUEZ": 3.937674519147973e-06, "ACTIVIDADLABORAL_KEY ACCOUNT MANAGER": 1.823356653489549e-05, "ACTIVIDADLABORAL_KINESIOLOGO": 5.801947160644789e-05, "ACTIVIDADLABORAL_LABORATORISTA DENTAL": 0.0, "ACTIVIDADLABORAL_LICENCIADO": 0.0, "ACTIVIDADLABORAL_LOCUTOR": 0.0, "ACTIVIDADLABORAL_MANAGER": 0.0, "ACTIVIDADLABORAL_MANTENIMIENTO": 0.0, "ACTIVIDADLABORAL_MARKETING": 0.0, "ACTIVIDADLABORAL_MARTILLERO PUBLICO": 0.0023031588483938804, "ACTIVIDADLABORAL_MATRON": 0.0, "ACTIVIDADLABORAL_MATRONA": 8.919525191994937e-05, "ACTIVIDADLABORAL_MECANICO": 0.00017660377784895224, "ACTIVIDADLABORAL_MEDICO": 0.0076299304185272875, "ACTIVIDADLABORAL_MEDICO VETERINARIO": 7.176023632717492e-05, "ACTIVIDADLABORAL_NUTRICIONISTA": 0.0014173541257714187, "ACTIVIDADLABORAL_ODONTOLOGO": 2.1284952990039003e-05, "ACTIVIDADLABORAL_OFTALMOLOGO": 0.0, "ACTIVIDADLABORAL_OPERADOR": 3.7025740989042566e-06, "ACTIVIDADLABORAL_OPERADOR MAQUINARIA PESADA": 0.0, "ACTIVIDADLABORAL_OPERADOR MESA DINERO": 0.0, "ACTIVIDADLABORAL_OPERARIO": 3.958143045974826e-05, "ACTIVIDADLABORAL_ORIENTADOR FAMILIAR": 0.0, "ACTIVIDADLABORAL_OTRAS ACTIVIDADES NO ESPECIFICADAS ALTO RIESGO***": 0.00020707541148165577, "ACTIVIDADLABORAL_OTRAS ACTIVIDADES NO ESPECIFICADAS RIESGO NORMAL": 0.0017291109645177702, "ACTIVIDADLABORAL_OTROS": 3.8601783527820884e-05, "ACTIVIDADLABORAL_PAISAJISTA": 0.0002159026543992092, "ACTIVIDADLABORAL_PASTOR MISIONERO": 0.0, "ACTIVIDADLABORAL_PENSIONADO": 0.005227635001753585, "ACTIVIDADLABORAL_PERIODISTA": 0.001887929034317847, "ACTIVIDADLABORAL_PILOTO": 1.2901559888549374e-05, "ACTIVIDADLABORAL_PILOTOS DE AVION, ALAS DELTA Y HELICOPTEROS ***": 0.0, "ACTIVIDADLABORAL_PLANIFICADOR, SUPERVISOR": 0.00011407292232540915, "ACTIVIDADLABORAL_PREPARADOR FISICO": 0.00010802125283233695, "ACTIVIDADLABORAL_PREVENSION DE RIESGO": 0.0004603176161621433, "ACTIVIDADLABORAL_PROCURADOR": 0.0, "ACTIVIDADLABORAL_PRODUCT MANAGER": 5.449586474406385e-05, "ACTIVIDADLABORAL_PRODUCTOR": 0.0, "ACTIVIDADLABORAL_PROFESOR": 0.0016710811764365104, "ACTIVIDADLABORAL_PROGRAMADOR": 0.0002124865601433963, "ACTIVIDADLABORAL_PROYECTISTA": 0.00038782136197826973, "ACTIVIDADLABORAL_PSICOLOGA": 1.8953336065583192e-07, "ACTIVIDADLABORAL_PSICOLOGO": 0.0010630983311034337, "ACTIVIDADLABORAL_PSICOPEDAGOGA": 0.0, "ACTIVIDADLABORAL_PSIQUIATRA": 0.0, "ACTIVIDADLABORAL_PUBLICISTA": 0.0029680787381178124, "ACTIVIDADLABORAL_QUIMICO": 0.0, "ACTIVIDADLABORAL_QUIMICO FARMACEUTICO": 3.7071146487098206e-05, "ACTIVIDADLABORAL_RECEPCIONISTA": 0.0, "ACTIVIDADLABORAL_RELACIONADOR PUBLICO": 0.0, "ACTIVIDADLABORAL_RELACIONISTA PUBLICO": 0.0, "ACTIVIDADLABORAL_REPORTERO": 4.168252426367952e-06, "ACTIVIDADLABORAL_REPRESENTACIONES": 8.787877638466714e-05, "ACTIVIDADLABORAL_REPRESENTANTE DE VENTAS": 0.0, "ACTIVIDADLABORAL_REPRESENTANTE LEGAL": 0.0, "ACTIVIDADLABORAL_REPRESENTANTE MEDICO": 1.7580904154531298e-06, "ACTIVIDADLABORAL_SECRETARIA": 0.00020546943227634396, "ACTIVIDADLABORAL_SOCIO": 0.0, "ACTIVIDADLABORAL_SOCIOLOGO": 0.0, "ACTIVIDADLABORAL_SOLDADOR": 3.0510871309521472e-05, "ACTIVIDADLABORAL_SOPORTE INFORMATICO": 0.0, "ACTIVIDADLABORAL_SUB DIRECTOR": 0.0, "ACTIVIDADLABORAL_SUB-GERENTE EMPRESA": 0.0006435388225399956, "ACTIVIDADLABORAL_SUPERVISOR": 0.0002190679408960998, "ACTIVIDADLABORAL_TECNICO": 0.00010250031145126171, "ACTIVIDADLABORAL_TECNICO ELECTRICO": 0.0, "ACTIVIDADLABORAL_TECNICO EN ENFERMERIA": 0.00010144218624073153, "ACTIVIDADLABORAL_TECNICO FINANCIERO": 0.0, "ACTIVIDADLABORAL_TECNOLOGO M\\u00c3\\u0089DICO": 0.00027255599791487493, "ACTIVIDADLABORAL_TERAPEUTA": 0.0, "ACTIVIDADLABORAL_TERAPEUTA OCUPACIONAL": 0.0, "ACTIVIDADLABORAL_TESORERO": 0.0, "ACTIVIDADLABORAL_TOPOGRAFO": 7.20453894356455e-05, "ACTIVIDADLABORAL_TRABAJADOR SOCIAL": 0.00016640253390310406, "ACTIVIDADLABORAL_TRADUCTOR": 1.2977725368492407e-06, "ACTIVIDADLABORAL_TRANSPORTISTAS RIESGO NORMAL": 0.0, "ACTIVIDADLABORAL_TRAUMATOLOGO": 0.0, "ACTIVIDADLABORAL_TRIPULANTE DE CABINA": 1.3826841606226881e-05, "ACTIVIDADLABORAL_VENDEDOR": 0.00018160947419938985, "ACTIVIDADLABORAL_VETERINARIO": 8.869936677852014e-05, "ACTIVIDADLABORAL_VISITADOR MEDICO": 0.0, "FECHAMOV_2016/01/08": 1.2135276212056779e-05, "FECHAMOV_2016/01/18": 0.0, "FECHAMOV_2016/01/19": 0.0, "FECHAMOV_2016/01/25": 0.0, "FECHAMOV_2016/02/01": 0.0, "FECHAMOV_2016/02/04": 1.3854811043205786e-06, "FECHAMOV_2016/02/08": 0.0, "FECHAMOV_2016/02/09": 0.0, "FECHAMOV_2016/02/16": 5.1754988004427294e-06, "FECHAMOV_2016/02/24": 0.0, "FECHAMOV_2016/02/25": 0.0, "FECHAMOV_2016/02/29": 0.0, "FECHAMOV_2016/03/02": 0.0, "FECHAMOV_2016/03/03": 6.382315791422223e-05, "FECHAMOV_2016/03/07": 3.282934834605122e-05, "FECHAMOV_2016/03/08": 0.0, "FECHAMOV_2016/03/09": 0.0, "FECHAMOV_2016/03/10": 0.0, "FECHAMOV_2016/03/11": 0.0, "FECHAMOV_2016/03/15": 0.0, "FECHAMOV_2016/03/17": 0.0, "FECHAMOV_2016/03/21": 5.2851455721867584e-05, "FECHAMOV_2016/03/23": 0.0, "FECHAMOV_2016/03/24": 0.0, "FECHAMOV_2016/03/28": 0.0, "FECHAMOV_2016/03/29": 5.5725753901146486e-05, "FECHAMOV_2016/03/30": 0.0, "FECHAMOV_2016/04/04": 0.0, "FECHAMOV_2016/04/05": 0.0, "FECHAMOV_2016/04/06": 0.0, "FECHAMOV_2016/04/07": 0.0, "FECHAMOV_2016/04/08": 0.0, "FECHAMOV_2016/04/11": 0.0, "FECHAMOV_2016/04/12": 0.0, "FECHAMOV_2016/04/13": 0.0, "FECHAMOV_2016/04/18": 0.0, "FECHAMOV_2016/04/19": 0.0, "FECHAMOV_2016/04/20": 0.0, "FECHAMOV_2016/04/21": 2.563832276424455e-07, "FECHAMOV_2016/04/22": 0.0, "FECHAMOV_2016/04/25": 0.0, "FECHAMOV_2016/04/26": 0.0, "FECHAMOV_2016/04/27": 4.750392904786045e-05, "FECHAMOV_2016/04/29": 0.0, "FECHAMOV_2016/05/03": 1.0958371975103756e-06, "FECHAMOV_2016/05/04": 0.0, "FECHAMOV_2016/05/05": 6.751655747795379e-06, "FECHAMOV_2016/05/06": 0.0, "FECHAMOV_2016/05/09": 0.0, "FECHAMOV_2016/05/10": 0.0, "FECHAMOV_2016/05/11": 0.0, "FECHAMOV_2016/05/12": 2.7656103010234573e-05, "FECHAMOV_2016/05/13": 0.0, "FECHAMOV_2016/05/16": 0.0, "FECHAMOV_2016/05/17": 1.840763323594847e-05, "FECHAMOV_2016/05/18": 0.0, "FECHAMOV_2016/05/19": 0.0, "FECHAMOV_2016/05/20": 0.0, "FECHAMOV_2016/05/23": 0.0, "FECHAMOV_2016/05/24": 0.0, "FECHAMOV_2016/05/25": 0.0, "FECHAMOV_2016/05/26": 0.0, "FECHAMOV_2016/05/27": 1.2964981562691673e-05, "FECHAMOV_2016/05/30": 6.774298681761124e-06, "FECHAMOV_2016/05/31": 2.4924500843960777e-06, "FECHAMOV_2016/06/01": 0.0, "FECHAMOV_2016/06/02": 0.0, "FECHAMOV_2016/06/03": 1.4720863948316301e-06, "FECHAMOV_2016/06/06": 0.0, "FECHAMOV_2016/06/07": 5.169159261938692e-06, "FECHAMOV_2016/06/08": 0.0, "FECHAMOV_2016/06/09": 0.0, "FECHAMOV_2016/06/10": 0.0, "FECHAMOV_2016/06/13": 1.2487902445007718e-06, "FECHAMOV_2016/06/14": 0.0, "FECHAMOV_2016/06/16": 0.0, "FECHAMOV_2016/06/17": 0.0, "FECHAMOV_2016/06/20": 0.0, "FECHAMOV_2016/06/21": 0.0, "FECHAMOV_2016/06/22": 0.0, "FECHAMOV_2016/06/23": 0.0, "FECHAMOV_2016/06/28": 0.0, "FECHAMOV_2016/06/29": 3.167797746205399e-05, "FECHAMOV_2016/06/30": 0.0, "FECHAMOV_2016/07/01": 0.0, "FECHAMOV_2016/07/04": 0.0, "FECHAMOV_2016/07/05": 6.379218683521713e-05, "FECHAMOV_2016/07/06": 0.0, "FECHAMOV_2016/07/07": 0.00022501867715960232, "FECHAMOV_2016/07/11": 4.0910904095044775e-06, "FECHAMOV_2016/07/12": 0.0, "FECHAMOV_2016/07/13": 0.0, "FECHAMOV_2016/07/14": 0.0, "FECHAMOV_2016/07/18": 0.0, "FECHAMOV_2016/07/19": 0.0, "FECHAMOV_2016/07/20": 0.00026108158223636186, "FECHAMOV_2016/07/21": 0.0, "FECHAMOV_2016/07/22": 1.9850610873664656e-05, "FECHAMOV_2016/07/25": 0.0001766483658795636, "FECHAMOV_2016/07/26": 0.0, "FECHAMOV_2016/07/27": 0.0, "FECHAMOV_2016/07/28": 6.580949739026395e-05, "FECHAMOV_2016/07/29": 0.0, "FECHAMOV_2016/08/01": 4.447622202522792e-05, "FECHAMOV_2016/08/02": 0.0007995977936774211, "FECHAMOV_2016/08/03": 0.0, "FECHAMOV_2016/08/04": 0.0, "FECHAMOV_2016/08/05": 0.0001853793839367503, "FECHAMOV_2016/08/08": 0.00020025583714417824, "FECHAMOV_2016/08/09": 0.0, "FECHAMOV_2016/08/10": 0.00024274827956877564, "FECHAMOV_2016/08/11": 0.0, "FECHAMOV_2016/08/12": 6.841104871567156e-05, "FECHAMOV_2016/08/16": 0.00017434579981397155, "FECHAMOV_2016/08/17": 2.699779637788025e-07, "FECHAMOV_2016/08/18": 0.0, "FECHAMOV_2016/08/19": 0.0, "FECHAMOV_2016/08/22": 8.735915223413236e-07, "FECHAMOV_2016/08/23": 0.0, "FECHAMOV_2016/08/24": 0.0, "FECHAMOV_2016/08/25": 1.1543183768396499e-06, "FECHAMOV_2016/08/26": 0.0, "FECHAMOV_2016/08/29": 0.00028923837793587247, "FECHAMOV_2016/08/30": 0.0, "FECHAMOV_2016/08/31": 1.4767218208293521e-06, "FECHAMOV_2016/09/01": 0.0, "FECHAMOV_2016/09/02": 0.00012352717068788528, "FECHAMOV_2016/09/05": 1.375978302641015e-06, "FECHAMOV_2016/09/06": 0.0002948661141888159, "FECHAMOV_2016/09/07": 0.0, "FECHAMOV_2016/09/08": 0.0, "FECHAMOV_2016/09/09": 6.298394469733534e-05, "FECHAMOV_2016/09/12": 6.79613880904601e-05, "FECHAMOV_2016/09/13": 0.0, "FECHAMOV_2016/09/14": 0.0, "FECHAMOV_2016/09/15": 0.0, "FECHAMOV_2016/09/16": 0.0, "FECHAMOV_2016/09/20": 0.0, "FECHAMOV_2016/09/21": 0.0, "FECHAMOV_2016/09/22": 0.0, "FECHAMOV_2016/09/23": 9.446516070883646e-09, "FECHAMOV_2016/09/26": 0.0, "FECHAMOV_2016/09/27": 1.0349373698703905e-06, "FECHAMOV_2016/09/28": 0.0, "FECHAMOV_2016/09/29": 3.2825036582866748e-06, "FECHAMOV_2016/09/30": 0.0, "FECHAMOV_2016/10/03": 0.0, "FECHAMOV_2016/10/04": 0.0, "FECHAMOV_2016/10/05": 0.0, "FECHAMOV_2016/10/06": 0.0, "FECHAMOV_2016/10/07": 2.1787947407271805e-05, "FECHAMOV_2016/10/11": 0.0, "FECHAMOV_2016/10/12": 0.0, "FECHAMOV_2016/10/13": 0.0, "FECHAMOV_2016/10/14": 3.328143137551166e-06, "FECHAMOV_2016/10/17": 1.1280268445827366e-08, "FECHAMOV_2016/10/18": 0.0, "FECHAMOV_2016/10/19": 2.838463014926513e-05, "FECHAMOV_2016/10/20": 1.0018229203259587e-05, "FECHAMOV_2016/10/21": 8.070836758597462e-08, "FECHAMOV_2016/10/24": 2.6835850138180604e-06, "FECHAMOV_2016/10/25": 3.898252305651957e-05, "FECHAMOV_2016/10/26": 9.677051182467724e-06, "FECHAMOV_2016/10/27": 0.0, "FECHAMOV_2016/10/28": 0.0, "FECHAMOV_2016/11/02": 0.0003044833603605169, "FECHAMOV_2016/11/03": 0.0, "FECHAMOV_2016/11/04": 0.0, "FECHAMOV_2016/11/07": 1.7265924399058525e-06, "FECHAMOV_2016/11/08": 0.00010355656388976227, "FECHAMOV_2016/11/09": 1.1015580471030733e-05, "FECHAMOV_2016/11/10": 1.7826717129080172e-06, "FECHAMOV_2016/11/11": 0.0, "FECHAMOV_2016/11/14": 1.0410864983107929e-07, "FECHAMOV_2016/11/15": 5.174554342476265e-05, "FECHAMOV_2016/11/16": 6.04974798421389e-06, "FECHAMOV_2016/11/17": 0.0, "FECHAMOV_2016/11/18": 5.7556652710235825e-05, "FECHAMOV_2016/11/21": 2.691532114852449e-07, "FECHAMOV_2016/11/22": 2.785965477160587e-05, "FECHAMOV_2016/11/23": 5.665235884556449e-06, "FECHAMOV_2016/11/24": 4.6624156166900307e-05, "FECHAMOV_2016/11/28": 0.00018741368528863155, "FECHAMOV_2016/11/29": 9.191453896282717e-06, "FECHAMOV_2016/11/30": 0.0, "FECHAMOV_2016/12/01": 0.0, "FECHAMOV_2016/12/02": 7.517495309984553e-05, "FECHAMOV_2016/12/05": 3.689774438094742e-05, "FECHAMOV_2016/12/06": 8.303188358252704e-07, "FECHAMOV_2016/12/07": 1.0609987928244294e-06, "FECHAMOV_2016/12/09": 0.0, "FECHAMOV_2016/12/12": 0.0005125177464362659, "FECHAMOV_2016/12/13": 0.00022911563065461014, "FECHAMOV_2016/12/15": 3.617562712780209e-05, "FECHAMOV_2016/12/16": 1.46500863739427e-06, "FECHAMOV_2016/12/19": 0.0001040891104949968, "FECHAMOV_2016/12/20": 0.0, "FECHAMOV_2016/12/21": 0.00014161709118995386, "FECHAMOV_2016/12/22": 0.0, "FECHAMOV_2016/12/23": 0.0, "FECHAMOV_2016/12/26": 4.9446680705594946e-05, "FECHAMOV_2016/12/27": 5.365854905942924e-05, "FECHAMOV_2016/12/28": 0.00024553233336153, "FECHAMOV_2016/12/29": 1.5325009754143346e-06, "FECHAMOV_2016/12/30": 0.0, "FECHAMOV_2017/01/03": 3.017348455428395e-05, "FECHAMOV_2017/01/04": 0.00022662244233491484, "FECHAMOV_2017/01/05": 5.312173268255561e-05, "FECHAMOV_2017/01/06": 6.282432436497321e-07, "FECHAMOV_2017/01/09": 6.617284231645667e-05, "FECHAMOV_2017/01/10": 4.129986047779468e-05, "FECHAMOV_2017/01/11": 0.0, "FECHAMOV_2017/01/12": 3.8058692705540497e-07, "FECHAMOV_2017/01/13": 5.830756132625898e-06, "FECHAMOV_2017/01/16": 6.1211826202827515e-06, "FECHAMOV_2017/01/17": 8.261711927880214e-06, "FECHAMOV_2017/01/18": 0.0, "FECHAMOV_2017/01/19": 0.0, "FECHAMOV_2017/01/20": 0.0, "FECHAMOV_2017/01/23": 0.0, "FECHAMOV_2017/01/24": 1.631967668463377e-05, "FECHAMOV_2017/01/25": 0.0005637075215171836, "FECHAMOV_2017/01/26": 5.090984832936666e-07, "FECHAMOV_2017/01/27": 4.620055026825115e-05, "FECHAMOV_2017/01/30": 0.0, "FECHAMOV_2017/01/31": 0.0, "FECHAMOV_2017/02/01": 0.0, "FECHAMOV_2017/02/02": 0.00010460641425161589, "FECHAMOV_2017/02/03": 0.0, "FECHAMOV_2017/02/06": 0.0003147482904471897, "FECHAMOV_2017/02/07": 1.4185753107358967e-05, "FECHAMOV_2017/02/08": 0.0, "FECHAMOV_2017/02/09": 9.659610423102475e-06, "FECHAMOV_2017/02/10": 0.0, "FECHAMOV_2017/02/13": 1.4472401209311968e-05, "FECHAMOV_2017/02/14": 1.2545935256159448e-05, "FECHAMOV_2017/02/15": 7.763236913572565e-05, "FECHAMOV_2017/02/16": 0.0, "FECHAMOV_2017/02/17": 5.604527345121875e-06, "FECHAMOV_2017/02/20": 0.0, "FECHAMOV_2017/02/21": 6.622252424824906e-05, "FECHAMOV_2017/02/22": 0.00011786852188582736, "FECHAMOV_2017/02/23": 2.1415128086117387e-05, "FECHAMOV_2017/02/24": 0.0, "FECHAMOV_2017/02/27": 3.601660486867131e-05, "FECHAMOV_2017/02/28": 0.0, "FECHAMOV_2017/03/01": 0.0, "FECHAMOV_2017/03/02": 5.1169446205694536e-05, "FECHAMOV_2017/03/03": 0.0003767474516571105, "FECHAMOV_2017/03/06": 0.0, "FECHAMOV_2017/03/07": 4.7093479156107333e-05, "FECHAMOV_2017/03/08": 5.37259724584616e-05, "FECHAMOV_2017/03/09": 0.0, "FECHAMOV_2017/03/10": 0.0, "FECHAMOV_2017/03/13": 7.050284295774593e-05, "FECHAMOV_2017/03/14": 2.0520882523182307e-05, "FECHAMOV_2017/03/15": 9.625588153513153e-05, "FECHAMOV_2017/03/16": 2.2502657594111636e-06, "FECHAMOV_2017/03/17": 0.0001162029844402297, "FECHAMOV_2017/03/20": 0.0, "FECHAMOV_2017/03/21": 0.00023714749552118073, "FECHAMOV_2017/03/22": 1.5407286212378266e-06, "FECHAMOV_2017/03/23": 3.5504044394084984e-05, "FECHAMOV_2017/03/24": 4.652132031057227e-05, "FECHAMOV_2017/03/27": 1.3798352187193875e-05, "FECHAMOV_2017/03/28": 5.524947692792285e-07, "FECHAMOV_2017/03/29": 0.00011204001655130608, "FECHAMOV_2017/03/30": 0.00030828156716094666, "FECHAMOV_2017/03/31": 4.442751653022876e-05, "FECHAMOV_2017/04/03": 0.0, "FECHAMOV_2017/04/04": 0.00016661071712147435, "FECHAMOV_2017/04/05": 2.1450265465178305e-05, "FECHAMOV_2017/04/06": 0.0, "FECHAMOV_2017/04/07": 0.0, "FECHAMOV_2017/04/10": 0.0, "FECHAMOV_2017/04/11": 3.815590989140556e-05, "FECHAMOV_2017/04/12": 0.00019068829040770556, "FECHAMOV_2017/04/13": 0.0, "FECHAMOV_2017/04/17": 9.190931005483206e-05, "FECHAMOV_2017/04/18": 0.00022209458856267312, "FECHAMOV_2017/04/20": 0.00024091033683377482, "FECHAMOV_2017/04/21": 9.116729904036188e-05, "FECHAMOV_2017/04/24": 0.0, "FECHAMOV_2017/04/25": 0.0, "FECHAMOV_2017/04/26": 1.908902450560825e-05, "FECHAMOV_2017/04/27": 7.064319815694242e-07, "FECHAMOV_2017/04/28": 6.607697715243522e-05, "FECHAMOV_2017/05/02": 8.731669899462197e-05, "FECHAMOV_2017/05/03": 0.00024945462197923637, "FECHAMOV_2017/05/04": 3.86778260108889e-05, "FECHAMOV_2017/05/05": 0.0, "FECHAMOV_2017/05/08": 0.0, "FECHAMOV_2017/05/09": 0.0001353573052028192, "FECHAMOV_2017/05/10": 8.149770751218665e-05, "FECHAMOV_2017/05/11": 0.0, "FECHAMOV_2017/05/12": 5.7561839776498476e-05, "FECHAMOV_2017/05/15": 3.860544249501024e-05, "FECHAMOV_2017/05/16": 0.0, "FECHAMOV_2017/05/17": 0.0004983932655641388, "FECHAMOV_2017/05/18": 0.0, "FECHAMOV_2017/05/19": 2.6339204810838838e-05, "FECHAMOV_2017/05/22": 6.195061417582661e-07, "FECHAMOV_2017/05/23": 0.00044470172574265644, "FECHAMOV_2017/05/24": 1.4717866941838e-05, "FECHAMOV_2017/05/25": 0.0, "FECHAMOV_2017/05/26": 4.102850817646004e-05, "FECHAMOV_2017/05/29": 6.440166123378319e-07, "FECHAMOV_2017/05/30": 0.0, "FECHAMOV_2017/05/31": 1.8252196844972803e-07, "FECHAMOV_2017/06/01": 1.7302493685999444e-07, "FECHAMOV_2017/06/02": 0.0, "FECHAMOV_2017/06/05": 5.964792828663956e-07, "FECHAMOV_2017/06/06": 8.430963839274247e-05, "FECHAMOV_2017/06/07": 8.460419441753023e-06, "FECHAMOV_2017/06/08": 3.7548748027450338e-06, "FECHAMOV_2017/06/09": 0.0002116895156112968, "FECHAMOV_2017/06/12": 0.0003191550902871262, "FECHAMOV_2017/06/13": 3.4139657887068966e-05, "FECHAMOV_2017/06/14": 0.0001342966513447987, "FECHAMOV_2017/06/15": 5.170110224139562e-05, "FECHAMOV_2017/06/16": 0.0002486141260204471, "FECHAMOV_2017/06/19": 3.368650051690055e-05, "FECHAMOV_2017/06/20": 0.0001844689759528084, "FECHAMOV_2017/06/21": 0.0, "FECHAMOV_2017/06/22": 0.00032648108588179684, "FECHAMOV_2017/06/23": 0.0003455526565898015, "FECHAMOV_2017/06/27": 0.00012437245514034992, "FECHAMOV_2017/06/28": 0.00011938992809203875, "FECHAMOV_2017/06/29": 0.0, "FECHAMOV_2017/06/30": 2.656032329672466e-06, "FECHAMOV_2017/07/03": 3.7475041170406386e-06, "FECHAMOV_2017/07/04": 1.6547812324887434e-06, "FECHAMOV_2017/07/05": 7.562299584857517e-05, "FECHAMOV_2017/07/06": 0.0, "FECHAMOV_2017/07/07": 0.00036352350726906436, "FECHAMOV_2017/07/10": 0.000829082351247852, "FECHAMOV_2017/07/11": 0.0, "FECHAMOV_2017/07/12": 5.1691367502629826e-05, "FECHAMOV_2017/07/13": 0.00015414657768449076, "FECHAMOV_2017/07/14": 0.0003980104194402659, "FECHAMOV_2017/07/17": 1.1858474332814148e-06, "FECHAMOV_2017/07/18": 0.0, "FECHAMOV_2017/07/19": 0.00019161425409162708, "FECHAMOV_2017/07/20": 0.0002835797427295307, "FECHAMOV_2017/07/21": 0.0, "FECHAMOV_2017/07/24": 0.00019397840113846078, "FECHAMOV_2017/07/25": 0.0005226318564568134, "FECHAMOV_2017/07/26": 7.01744697171269e-05, "FECHAMOV_2017/07/27": 0.00028363187009140686, "FECHAMOV_2017/07/28": 1.8644234002996918e-05, "FECHAMOV_2017/07/31": 3.713874099453495e-05, "FECHAMOV_2017/08/01": 4.644299964442799e-06, "FECHAMOV_2017/08/02": 2.9327726274983275e-05, "FECHAMOV_2017/08/03": 2.71963932179135e-06, "FECHAMOV_2017/08/04": 6.338397209929026e-05, "FECHAMOV_2017/08/07": 4.696535690822728e-06, "FECHAMOV_2017/08/08": 0.00016479628206442264, "FECHAMOV_2017/08/09": 0.0, "FECHAMOV_2017/08/10": 1.4529780081011866e-05, "FECHAMOV_2017/08/11": 8.393593136848453e-05, "FECHAMOV_2017/08/14": 1.4948095905779932e-05, "FECHAMOV_2017/08/16": 0.0, "FECHAMOV_2017/08/17": 0.0004282197069904128, "FECHAMOV_2017/08/18": 0.00013991276097560746, "FECHAMOV_2017/08/21": 6.671849507112715e-06, "FECHAMOV_2017/08/22": 0.0001182923744918714, "FECHAMOV_2017/08/23": 6.150310828264325e-05, "FECHAMOV_2017/08/24": 7.120088261431141e-05, "FECHAMOV_2017/08/25": 0.00018757534359739128, "FECHAMOV_2017/08/28": 6.504918240875561e-05, "FECHAMOV_2017/08/29": 2.691760392403416e-06, "FECHAMOV_2017/08/30": 5.2358669838748314e-05, "FECHAMOV_2017/08/31": 1.8394717660350028e-05, "FECHAMOV_2017/09/01": 0.00010516514016982647, "FECHAMOV_2017/09/04": 0.00014643616153343504, "FECHAMOV_2017/09/05": 1.431733992575171e-07, "FECHAMOV_2017/09/06": 0.0010139478418891106, "FECHAMOV_2017/09/07": 5.455073051237961e-06, "FECHAMOV_2017/09/08": 7.566593158044942e-05, "FECHAMOV_2017/09/11": 5.8925350856802064e-06, "FECHAMOV_2017/09/12": 9.984191876590148e-07, "FECHAMOV_2017/09/13": 2.8123368943783922e-05, "FECHAMOV_2017/09/14": 0.0014132633027770347, "FECHAMOV_2017/09/15": 0.0, "FECHAMOV_2017/09/20": 8.410529742942429e-09, "FECHAMOV_2017/09/21": 5.567701722481161e-05, "FECHAMOV_2017/09/22": 3.37349586426108e-06, "FECHAMOV_2017/09/25": 9.298400314813428e-05, "FECHAMOV_2017/09/26": 0.00012987966003753852, "FECHAMOV_2017/09/27": 0.0, "FECHAMOV_2017/09/28": 0.00019153363084594227, "FECHAMOV_2017/09/29": 1.0387366837112952e-05, "FECHAMOV_2017/10/02": 1.49670146092821e-05, "FECHAMOV_2017/10/03": 2.3203944470866107e-05, "FECHAMOV_2017/10/04": 0.0002831291273410462, "FECHAMOV_2017/10/05": 4.458586731371951e-05, "FECHAMOV_2017/10/06": 0.00019925455467495955, "FECHAMOV_2017/10/10": 6.63791035606959e-05, "FECHAMOV_2017/10/11": 2.919543474507421e-05, "FECHAMOV_2017/10/12": 1.2218195735772575e-05, "FECHAMOV_2017/10/13": 0.000379014175714616, "FECHAMOV_2017/10/16": 0.00017811388040016969, "FECHAMOV_2017/10/17": 0.00029636483616482187, "FECHAMOV_2017/10/18": 4.870447923536827e-05, "FECHAMOV_2017/10/19": 0.00013850935349182, "FECHAMOV_2017/10/20": 1.6262197852656402e-05, "FECHAMOV_2017/10/23": 2.1633548141841717e-06, "FECHAMOV_2017/10/24": 0.00031363177366043897, "FECHAMOV_2017/10/25": 5.3528777522932275e-05, "FECHAMOV_2017/10/26": 4.723742454082854e-05, "FECHAMOV_2017/10/30": 0.0007272819301965513, "FECHAMOV_2017/10/31": 0.001374865295521185, "FECHAMOV_2017/11/02": 0.0, "FECHAMOV_2017/11/03": 0.0014168285412964195, "FECHAMOV_2017/11/06": 0.0001312001773661511, "FECHAMOV_2017/11/07": 0.0005690370482978228, "FECHAMOV_2017/11/08": 2.2204742778511386e-05, "FECHAMOV_2017/11/09": 2.479954060070424e-05, "FECHAMOV_2017/11/10": 0.0003510362393213841, "FECHAMOV_2017/11/13": 0.0001814200616368609, "FECHAMOV_2017/11/14": 3.2257665986884164e-05, "FECHAMOV_2017/11/15": 0.00011854565612700537, "FECHAMOV_2017/11/16": 0.0, "FECHAMOV_2017/11/17": 4.270038287501427e-05, "FECHAMOV_2017/11/20": 0.0003759456784611347, "FECHAMOV_2017/11/21": 0.0002128634334313502, "FECHAMOV_2017/11/22": 0.0001696515854593129, "FECHAMOV_2017/11/23": 0.0012786634821105468, "FECHAMOV_2017/11/27": 0.00013412490804097046, "FECHAMOV_2017/11/28": 0.00010321834853734354, "FECHAMOV_2017/11/29": 1.1033337960363297e-05, "FECHAMOV_2017/11/30": 2.280510208676351e-07, "FECHAMOV_2017/12/01": 1.664384125651202e-05, "FECHAMOV_2017/12/04": 0.0, "FECHAMOV_2017/12/05": 0.0003915507256090564, "FECHAMOV_2017/12/06": 2.3074261529915767e-06, "FECHAMOV_2017/12/07": 5.854463081137521e-05, "FECHAMOV_2017/12/11": 1.1767685426074918e-05, "FECHAMOV_2017/12/12": 0.000633852618771295, "FECHAMOV_2017/12/13": 0.000302987981234448, "FECHAMOV_2017/12/14": 0.0, "FECHAMOV_2017/12/15": 0.00021576037458525039, "FECHAMOV_2017/12/18": 0.0, "FECHAMOV_2017/12/19": 2.5039219365486108e-06, "FECHAMOV_2017/12/20": 0.0005376300984468564, "FECHAMOV_2017/12/21": 0.0009049301690601089, "FECHAMOV_2017/12/22": 9.13461619376833e-05, "FECHAMOV_2017/12/26": 0.0, "FECHAMOV_2017/12/27": 0.00017999728087683385, "FECHAMOV_2017/12/28": 7.531491099951659e-06, "FECHAMOV_2017/12/29": 1.6472847770137907e-07, "FECHAMOV_2018/01/02": 8.034404070930504e-05, "FECHAMOV_2018/01/03": 0.00029804488197826047, "FECHAMOV_2018/01/04": 0.0011608586021151408, "FECHAMOV_2018/01/05": 0.00012163882618985095, "FECHAMOV_2018/01/08": 0.0002367488326182294, "FECHAMOV_2018/01/09": 0.0, "FECHAMOV_2018/01/10": 0.0, "FECHAMOV_2018/01/11": 0.0003603886542003583, "FECHAMOV_2018/01/12": 0.0003634632362364389, "FECHAMOV_2018/01/15": 6.209897644438994e-05, "FECHAMOV_2018/01/16": 6.875249876819995e-05, "FECHAMOV_2018/01/17": 0.0, "FECHAMOV_2018/01/18": 4.836427659911269e-05, "FECHAMOV_2018/01/19": 0.0001796954731939645, "FECHAMOV_2018/01/22": 0.0, "FECHAMOV_2018/01/23": 0.00023648166356651573, "FECHAMOV_2018/01/24": 5.248052356765589e-06, "FECHAMOV_2018/01/25": 0.0, "FECHAMOV_2018/01/26": 0.0004207307787970563, "FECHAMOV_2018/01/29": 0.00032469574939594234, "FECHAMOV_2018/01/30": 0.0005407424069978653, "FECHAMOV_2018/01/31": 1.0122780970904916e-06, "FECHAMOV_2018/02/01": 0.0, "FECHAMOV_2018/02/02": 3.913264137842471e-05, "FECHAMOV_2018/02/05": 0.00027128721155667847, "FECHAMOV_2018/02/06": 0.0004524268817046724, "FECHAMOV_2018/02/07": 0.00019285393556244025, "FECHAMOV_2018/02/08": 0.00032787687796831426, "FECHAMOV_2018/02/09": 0.0, "FECHAMOV_2018/02/12": 6.499612925330133e-05, "FECHAMOV_2018/02/13": 4.037687334240579e-05, "FECHAMOV_2018/02/14": 0.0004681585432651905, "FECHAMOV_2018/02/15": 1.4456483276784177e-05, "FECHAMOV_2018/02/16": 5.792348054954088e-05, "FECHAMOV_2018/02/19": 0.0, "FECHAMOV_2018/02/20": 9.194039220181663e-05, "FECHAMOV_2018/02/21": 8.047321297220909e-10, "FECHAMOV_2018/02/22": 0.000495323753231529, "FECHAMOV_2018/02/23": 0.0001140760733843205, "FECHAMOV_2018/02/26": 2.7655093227279255e-05, "FECHAMOV_2018/02/27": 2.0564564519730364e-05, "FECHAMOV_2018/02/28": 0.0016079479956939346, "FECHAMOV_2018/03/01": 8.007130361249335e-06, "FECHAMOV_2018/03/02": 0.0002279204434214336, "FECHAMOV_2018/03/05": 0.00010391079317064346, "FECHAMOV_2018/03/06": 0.0005101467721771718, "FECHAMOV_2018/03/07": 0.00024333556782129008, "FECHAMOV_2018/03/08": 0.00031672029621235525, "FECHAMOV_2018/03/09": 3.8526259974104314e-05, "FECHAMOV_2018/03/12": 0.0, "FECHAMOV_2018/03/13": 0.0002570436831121066, "FECHAMOV_2018/03/14": 0.00023822645116022056, "FECHAMOV_2018/03/15": 6.542024360160114e-05, "FECHAMOV_2018/03/16": 1.2218922642563627e-05, "FECHAMOV_2018/03/19": 0.001538285239299524, "FECHAMOV_2018/03/20": 1.2059219236708587e-05, "FECHAMOV_2018/03/21": 0.0031358247226357158, "FECHAMOV_2018/03/22": 9.633786874447441e-05, "FECHAMOV_2018/03/23": 3.501764156227337e-05, "FECHAMOV_2018/03/26": 0.00010766417794560534, "FECHAMOV_2018/03/27": 0.0013933077981611636, "FECHAMOV_2018/03/28": 0.0, "FECHAMOV_2018/03/29": 0.0024158364710441227, "FECHAMOV_2018/04/03": 8.89198505156771e-05, "FECHAMOV_2018/04/04": 9.374955293833526e-05, "FECHAMOV_2018/04/05": 7.187188592362308e-05, "FECHAMOV_2018/04/06": 0.0005686377860831704, "FECHAMOV_2018/04/09": 0.001336603473180995, "FECHAMOV_2018/04/10": 0.00040112579702440503, "FECHAMOV_2018/04/11": 7.860259485022384e-05, "FECHAMOV_2018/04/12": 0.00014740119567176928, "FECHAMOV_2018/04/13": 0.0007308139468862583, "FECHAMOV_2018/04/16": 4.779031277794154e-06, "FECHAMOV_2018/04/17": 0.0003481298294022919, "FECHAMOV_2018/04/18": 0.0005108733256251118, "FECHAMOV_2018/04/19": 0.0002264407373234249, "FECHAMOV_2018/04/20": 0.0003275232480795678, "FECHAMOV_2018/04/23": 4.2584689207339715e-05, "FECHAMOV_2018/04/24": 1.5330106043033728e-06, "FECHAMOV_2018/04/25": 4.765470312638856e-05, "FECHAMOV_2018/04/26": 0.00140723631462498, "FECHAMOV_2018/04/27": 6.072404720113441e-05, "FECHAMOV_2018/04/30": 0.00020550017541677752, "FECHAMOV_2018/05/02": 0.0, "FECHAMOV_2018/05/03": 0.0, "FECHAMOV_2018/05/04": 0.0006737181177415921, "FECHAMOV_2018/05/07": 0.00045289689951467216, "FECHAMOV_2018/05/08": 0.0005265819536384029, "FECHAMOV_2018/05/09": 8.996131274527811e-05, "FECHAMOV_2018/05/10": 0.0004155336416327176, "FECHAMOV_2018/05/11": 0.0007780561105939557, "FECHAMOV_2018/05/14": 0.00018100933863834842, "FECHAMOV_2018/05/15": 0.0001546320516647765, "FECHAMOV_2018/05/16": 0.00023166867396212974, "FECHAMOV_2018/05/17": 5.0605090119564996e-05, "FECHAMOV_2018/05/18": 0.0, "FECHAMOV_2018/05/22": 0.0, "FECHAMOV_2018/05/23": 0.00018973943132103842, "FECHAMOV_2018/05/24": 2.6549723421258955e-05, "FECHAMOV_2018/05/25": 0.0, "FECHAMOV_2018/05/28": 6.205123252296978e-05, "MEDIOPAGO_DIRECTO": 0.040896375267312844, "MEDIOPAGO_POR EMPLEADOR/DESCUENTO POR PLANILLA": 0.011479591400478639, "MEDIOPAGO_TARJETA DE CREDITO": 0.009042314514680903}
01CNWBAY48NGAXRW2FJG9JD23N	training	0.745275267050123	0.823361823361823	0.977672530446549	0.711122047244094	0.385983263598326	{"fn": 33.0, "fp": 587.0, "tn": 369.0, "tp": 1445.0}	{"fpr": [0.0, 0.6140167364016736, 1.0], "tpr": [0.0, 0.9776725304465493, 1.0], "aoc": 0.6818278970224378}	{"TIPOOPERACION": 0.05743269808737461, "FECFINVIG": 0.20440281030615345, "FECHA_INICIO_INTERMEDIARIO": 0.18270534766317806, "PROD": 0.09147445267471604, "NOMBREPRODUCTO": 0.09013863883592325, "PERIDICIDADPAGO": 0.10080792131037347, "OPERACION": 0.03456907960056085, "ACTIVIDADLABORAL": 0.11111107349694681, "FECHAMOV": 0.06593969684230107, "MEDIOPAGO": 0.06141828118247239}
\.


--
-- Data for Name: cx_ml_statistic; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cx_ml_statistic (id_statistic, report_name, report_desc, total_entries, positive_level_name, negative_level_name, positive_level, negative_level, positive_level_csv, negative_level_csv, id_cliente, id_usuario) FROM stdin;
cxstat001	churn	insurance churn in 2018 v1	49	ANU	ACT	23	26	s3://artucx/predictions/stats/cxstat001/pos.csv	s3://artucx/predictions/stats/cxstat001/neg.csv	vidasecurity	ccortez
\.


--
-- Data for Name: cx_model; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cx_model (id_model, category, version, "desc", industry, name, creation_time) FROM stdin;
cxm001	default_rate	1	{\n\t"type": "csv",\n\t"title": "insurance churn Model",\n\t"SALDODESPUESRETIRO": "int",\n\t"TIPOOPERACION": "varchar",\n\t"SALDOANTESRETIRO": "int",\n\t"CANT_RETIROSPAGADOS": "int",\n\t"FECFINVIG": "date",\n\t"FECHA_INICIO_INTERMEDIARIO": "date",\n\t"PROD": "varchar",\n\t"NOMBREPRODUCTO": "varchar",\n\t"PRIMABRUTA": "double",\n\t"PERIDICIDADPAGO": "varchar",\n\t"OPERACION": "varchar",\n\t"MTOBRUTO": "double",\n\t"ACTIVIDADLABORAL": "varchar",\n\t"FECHAMOV": "date",\n\t"MEDIOPAGO": "varchar",\n\t"STSPOL": "bool"\n}	insurance	default rate model	2018-06-22
\.


--
-- Data for Name: cx_notification_change; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cx_notification_change (id_notification_change, entity_type, entity_id, action_on_entity, actor_id_usuario, actor_id_cliente, created_at, entity_name) FROM stdin;
01CMRRADBZ0A65KM7YEQDK2ZWJ	CxCampaign	01CMRRAD7AZDY8WP17ZPJBZ8FV	created	ccortez	vidasecurity	2018-08-12 23:22:18.239227	\N
01CMRREEC0WTV06NR2RXV2AKJQ	CxCampaign	01CMRREE787M1V36QT0NSMK9BY	created	ccortez	vidasecurity	2018-08-12 23:24:30.336839	\N
01CMRRGEGC4QE7CSG53YSHACTK	CxCampaign	01CMRREE787M1V36QT0NSMK9BY	deleted	ccortez	vidasecurity	2018-08-12 23:25:36.012228	\N
01CMS2MP65RTB30GXQWPJ6A3D5	CxCampaign	01CMS2MP1XMDXEWQXXJT43F6GZ	created	ccortez	vidasecurity	2018-08-13 02:22:40.709426	campaign_test
01CMS3J1ZTFFPDC1K0SGTJHRXZ	CxCampaign	01CMS3J1VH6PPFYYBEE6RWSVSR	created	ccortez	vidasecurity	2018-08-13 02:38:43.066673	campaign_test
01CMS3SM5022YY0Y8WPNKF8X7C	CxCampaign	01CMS3SM0TH5HSMTMH8S5DFQ9T	created	ccortez	vidasecurity	2018-08-13 02:42:51.040143	campaign_test
01CP0BVMJRG51QVFKFZE3D6TV8	CxCampaign	01CP0BVMHM9DW7WEHHVSX9684B	created	ccortez	vidasecurity	2018-08-28 08:34:08.472809	campaign_test
01CP0BXMFTTH5HYA7R341ZAJBK	CxCampaign	01CP0BXMESHBM3Z29E3131C5MF	created	ccortez	vidasecurity	2018-08-28 08:35:13.914125	campaign_test
01CP0BYMN4B1GJG80SN4YE8FWH	CxCampaign	01CP0BYMM6K85BEP0NV0CBB8XB	created	ccortez	vidasecurity	2018-08-28 08:35:46.852819	campaign_test
01CP0BZW1CJHDPYCPFCMXCXH3X	CxCampaign	01CP0BZW0EFK72KE5X7RF23P9X	created	ccortez	vidasecurity	2018-08-28 08:36:27.180552	campaign_test
01CP0D05A43J6XF402BP0P34HM	CxCampaign	01CP0D05917E2C41Q7TNY9D05A	created	ccortez	vidasecurity	2018-08-28 08:54:05.252678	campaign_test
01CP0D2N05JZ2GAC15T6HM6Y7G	CxCampaign	01CP0D2MZ5KEMH363FKR9HGJT5	created	ccortez	vidasecurity	2018-08-28 08:55:26.853655	campaign_test
01CP0D473Z9R90X17KGP1A9XCX	CxCampaign	01CP0D472Z269R5ARPD7X6J9YD	created	ccortez	vidasecurity	2018-08-28 08:56:18.17522	campaign_test
01CP0D8DVY2M68E4HF8D9FA4N5	CxCampaign	01CP0D8DTYJ26KH21TDMBQ8AYB	created	ccortez	vidasecurity	2018-08-28 08:58:36.158178	campaign_test
01CP0E42K9GCGZDBJNSR36QG67	CxCampaign	01CP0E42J9PBG94D8BCQR3ZMG9	created	ccortez	vidasecurity	2018-08-28 09:13:42.121512	campaign_test
01CP0ET2RWAJ9ZNEF09J4NVNQC	CxCampaign	01CP0ET2QXQPWJR6VAF3N6THDR	created	ccortez	vidasecurity	2018-08-28 09:25:43.196208	campaign_test
01CP0EV5S5E5S3PJANRKPZCZMW	CxCampaign	01CP0EV5R6B5XK86XP56QWVC51	created	ccortez	vidasecurity	2018-08-28 09:26:19.045832	campaign_test
01CP0EW0X9A2TC3N8S30MQP1H2	CxCampaign	01CP0EW0WA961RCN2QABE7MNNT	created	ccortez	vidasecurity	2018-08-28 09:26:46.825151	campaign_test
01CP0EZHYQCY87W5XYPF7CTT0C	CxCampaign	01CP0EZHXSW1E3RKYH5EHD0WR8	created	ccortez	vidasecurity	2018-08-28 09:28:42.583492	campaign_test
01CP0F384N47WETXN46N9X8BY9	CxCampaign	01CP0F383NN678JPDR6GJ1TA9W	created	ccortez	vidasecurity	2018-08-28 09:30:43.60505	campaign_test
01CP0FMTZ4K638Q2HCWAYEV9MN	CxCampaign	01CP0FMTY47GD65N2BKF5MRYYD	created	ccortez	vidasecurity	2018-08-28 09:40:19.940374	campaign_test
01CP0FQ45QKQY6E2D85KPSRAEJ	CxCampaign	01CP0FQ44QVMQEV3VVCA9FY9ZN	created	ccortez	vidasecurity	2018-08-28 09:41:34.903054	campaign_test
01CP0FRQHDV0NSA4BHDB82F9ES	CxCampaign	01CP0FRQG6FW0ZZDD2BXR08K5N	created	ccortez	vidasecurity	2018-08-28 09:42:27.501681	campaign_test
01CP0G06HQ0TW1Z8G6SMA90G2N	CxCampaign	01CP0G06GQG9B2Z1DGDQTGKGCA	created	ccortez	vidasecurity	2018-08-28 09:46:32.247631	campaign_test
\.


--
-- Data for Name: cx_prediction; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cx_prediction (id_prediction, id_training, name, creation_time, on_dashboard, id_job, job_type, job_start_time, job_state, input_csv_url, output_csv_url, job_finish_time, id_cliente, id_usuario, id_statistic) FROM stdin;
01CH3CEFX5EN8SQ3KX9GJSP41Y	cxtrain001	cxtrain001	2018-06-28	f	01CH3CEFX5J9679TBDXBGCQH73	batch	2018-06-24	in progress	s3://artucx/predictions/cxpred001/input.csv	\N	\N	vidasecurity	ccortez	\N
01CH3CGY54N18XSYA3AFCC06MJ	cxtrain001	cxtrain001	2018-06-28	f	01CH3CGY54GQ6WR5HZREPBXG4T	batch	2018-06-24	in progress	s3://artucx/predictions/cxpred001/input.csv	\N	\N	vidasecurity	ccortez	\N
01CHDV6CCT3A2HM7QK9ACYQ7NR	cxtrain001	cxtrain001	2018-07-02	f	01CHDV6CCVQHHXSFX4TNWY1696	batch	2018-06-24	in progress	s3://artucx/predictions/cxpred001/input.csv	\N	\N	vidasecurity	ccortez	\N
01CHDW7V2X7W3BA5FE1MRXY5Y6	cxtrain001	cxtrain001	2018-07-02	f	01CHDW7V2XNMKX22C9Q2H7X2S1	batch	2018-06-24	in progress	s3://artucx/predictions/cxpred001/input.csv	\N	\N	vidasecurity	ccortez	\N
01CHDWCK05Q74X5J9GK62TMZEX	cxtrain001	cxtrain001	2018-07-02	f	01CHDWCK05BPE0SV5B2RH69B50	batch	2018-06-24	in progress	s3://artucx/predictions/cxpred001/input.csv	\N	\N	vidasecurity	ccortez	\N
01CHDWE7BGM70G2CVMEH4XBQXV	cxtrain001	cxtrain001	2018-07-02	f	01CHDWE7BGFZ03M7QHFD41A8GP	batch	2018-06-24	in progress	s3://artucx/predictions/cxpred001/input.csv	\N	\N	vidasecurity	ccortez	\N
cxpred001	cxtrain001	churn_julio_2018	2018-06-17	f	cxjob001	batch	2018-06-17	finished	s3://artucxstorage/vidasecurity_train_dataset.csv		2018-06-17	vidasecurity	ccortez	cxstat001
\.


--
-- Data for Name: cx_project; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cx_project (id_project, name, "desc", creation_date, id_cliente, id_usuario, from_address) FROM stdin;
01CH2Q0HPS81AT9VWGDPAB62QC	proyecto testing	testing	2018-06-28	vidasecurity	ccortez	\N
cxproj001	proyecto_1	churn julio 2018	2018-06-17	vidasecurity	ccortez	criscalovis@gmail.com
\.


--
-- Data for Name: cx_read_notification; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cx_read_notification (id_read_notification, id_usuario, id_notification_change, read_at) FROM stdin;
01CMS3SM50RT6V0Q40V013WV5S	ccortez	01CMS3SM5022YY0Y8WPNKF8X7C	2018-08-13 02:42:51.040315
01CMS5YWTMWETRZ11DSD2WR6J0	cristobal	01CMS3SM5022YY0Y8WPNKF8X7C	2018-08-13 03:20:40.91631
01CP0BVMJSF81V66Z8G9CYWJ2W	ccortez	01CP0BVMJRG51QVFKFZE3D6TV8	2018-08-28 08:34:08.473046
01CP0BXMFT23TH8Q6AF2H17C8E	ccortez	01CP0BXMFTTH5HYA7R341ZAJBK	2018-08-28 08:35:13.914282
01CP0BYMN4WCS44MHE3DPJXFAJ	ccortez	01CP0BYMN4B1GJG80SN4YE8FWH	2018-08-28 08:35:46.852975
01CP0BZW1CBCQD85PZFC5RWQRD	ccortez	01CP0BZW1CJHDPYCPFCMXCXH3X	2018-08-28 08:36:27.180706
01CP0D05A4A8WGGGMYS9FVTGSS	ccortez	01CP0D05A43J6XF402BP0P34HM	2018-08-28 08:54:05.252829
01CP0D2N05DA21XGFF4KHDMQHJ	ccortez	01CP0D2N05JZ2GAC15T6HM6Y7G	2018-08-28 08:55:26.85381
01CP0D473ZXSBN4QP0TCNFCXX6	ccortez	01CP0D473Z9R90X17KGP1A9XCX	2018-08-28 08:56:18.175413
01CP0D8DVY8FNF0HRCRXCPW0TJ	ccortez	01CP0D8DVY2M68E4HF8D9FA4N5	2018-08-28 08:58:36.158331
01CP0E42K9JYFMKNHB23A5F16J	ccortez	01CP0E42K9GCGZDBJNSR36QG67	2018-08-28 09:13:42.12167
01CP0ET2RWT40THVFDED65QHEN	ccortez	01CP0ET2RWAJ9ZNEF09J4NVNQC	2018-08-28 09:25:43.196363
01CP0EV5S526QG77T1AEQYT2D5	ccortez	01CP0EV5S5E5S3PJANRKPZCZMW	2018-08-28 09:26:19.045981
01CP0EW0X9PSA9525R9B1TX467	ccortez	01CP0EW0X9A2TC3N8S30MQP1H2	2018-08-28 09:26:46.825303
01CP0EZHYQY5AF5KMVYQ76FHJM	ccortez	01CP0EZHYQCY87W5XYPF7CTT0C	2018-08-28 09:28:42.583644
01CP0F384NDF0H7Z8WA86SRVEK	ccortez	01CP0F384N47WETXN46N9X8BY9	2018-08-28 09:30:43.605201
01CP0FMTZ4S1FJ7PSPEHFQEFGB	ccortez	01CP0FMTZ4K638Q2HCWAYEV9MN	2018-08-28 09:40:19.940615
01CP0FQ45QYQ9W77TK43JZMXRN	ccortez	01CP0FQ45QKQY6E2D85KPSRAEJ	2018-08-28 09:41:34.903206
01CP0FRQHDB2YEEEF7ZC2XH32X	ccortez	01CP0FRQHDV0NSA4BHDB82F9ES	2018-08-28 09:42:27.501833
01CP0G06HQS83ZXPK4Q8K5DN5M	ccortez	01CP0G06HQ0TW1Z8G6SMA90G2N	2018-08-28 09:46:32.247782
\.


--
-- Data for Name: cx_segment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cx_segment (id_segment, id_project, name, "desc", campaign_input_url, total_entries, creation_time, id_cliente, id_usuario, channel_type) FROM stdin;
cxseg001	cxproj001	julio 2018 v1	segmento de cancelacion 2018 Vida Más 	s3://artucxstorage/segment.csv	2	2018-06-17	vidasecurity	ccortez	\N
\.


--
-- Data for Name: cx_training; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cx_training (id_training, id_model, state, type, subtype, modeling_data, is_free, id_cliente, id_usuario, id_metric, creation_time, name, dataset_url_training, saved_model, training_label) FROM stdin;
01CK2D3NFB2TAPC0F6JQ1YE054	cxm001	processing	type	subtype	\N	false	vidasecurity	ccortez	\N	2018-07-22	name	s3://artucxstorage/vidasecurity_train_dataset.csv	\N	STSPOL
01CK2DGFEHY4K5X635W01YX6M5	cxm001	processing	type	subtype	\N	false	vidasecurity	ccortez	\N	2018-07-22	name	s3://artucxstorage/vidasecurity_train_dataset.csv	\N	STSPOL
01CK2DGQDNEKP8TF1HZCZN6DQY	cxm001	processing	type	subtype	\N	false	vidasecurity	ccortez	\N	2018-07-22	name	s3://artucxstorage/vidasecurity_train_dataset.csv	\N	STSPOL
01CK32KQ3BFP0SKGXM5A7XCDMX	cxm001	processing	type	subtype	\N	false	vidasecurity	ccortez	\N	2018-07-23	name	s3://artucxstorage/vidasecurity_train_dataset.csv	\N	STSPOL
01CK32MDYMDNRZW4W51NJB18KW	cxm001	processing	type	subtype	\N	false	vidasecurity	ccortez	\N	2018-07-23	name	s3://artucxstorage/vidasecurity_train_dataset.csv	\N	STSPOL
01CMRQR2QW3AQNCYSRQXZMQKS1	cxm001	processing	type	subtype	\N	false	vidasecurity	ccortez	\N	2018-08-12	name	s3://artucxstorage/vidasecurity_train_dataset.csv	\N	STSPOL
01CMRR0D0YT8H3K7VFH010VFKZ	cxm001	processing	type	subtype	\N	false	vidasecurity	ccortez	\N	2018-08-12	name	s3://artucxstorage/vidasecurity_train_dataset.csv	\N	STSPOL
01CMRR1JHX918S1Y1GA5KHSAF8	cxm001	processing	type	subtype	\N	false	vidasecurity	ccortez	\N	2018-08-12	name	s3://artucxstorage/vidasecurity_train_dataset.csv	\N	STSPOL
01CMRR2J47M6XKT1MCR7G33S63	cxm001	processing	type	subtype	\N	false	vidasecurity	ccortez	\N	2018-08-12	name	s3://artucxstorage/vidasecurity_train_dataset.csv	\N	STSPOL
01CMRR35JPYZS10ZCD8HBFX6X5	cxm001	processing	type	subtype	\N	false	vidasecurity	ccortez	\N	2018-08-12	name	s3://artucxstorage/vidasecurity_train_dataset.csv	\N	STSPOL
cxtrain001	cxm001	ready	supervised	classification	{}	true	vidasecurity	ccortez	01CNWBAY48NGAXRW2FJG9JD23N	2018-07-15		s3://artucxstorage/vidasecurity_train_dataset.csv	s3://artucxstorage/trainings/output/vidasecurity-cxtrain001-2018-08-26-19-08-03.pkl	STSPOL
\.


--
-- Name: cx_campaign cx_campaign_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_campaign
    ADD CONSTRAINT cx_campaign_pkey PRIMARY KEY (id_campaign);


--
-- Name: cx_campaign_stats cx_campaign_stats_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_campaign_stats
    ADD CONSTRAINT cx_campaign_stats_pkey PRIMARY KEY (id_c_stat);


--
-- Name: cx_mailtemplate cx_mailtemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_mailtemplate
    ADD CONSTRAINT cx_mailtemplate_pkey PRIMARY KEY (id_mailtemplate);


--
-- Name: cx_marketplace cx_marketplace_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_marketplace
    ADD CONSTRAINT cx_marketplace_pkey PRIMARY KEY (id_marketplace);


--
-- Name: cx_ml_metric cx_ml_metric_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_ml_metric
    ADD CONSTRAINT cx_ml_metric_pkey PRIMARY KEY (id_metric);


--
-- Name: cx_ml_statistic cx_ml_statistic_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_ml_statistic
    ADD CONSTRAINT cx_ml_statistic_pkey PRIMARY KEY (id_statistic);


--
-- Name: cx_model cx_model_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_model
    ADD CONSTRAINT cx_model_pkey PRIMARY KEY (id_model);


--
-- Name: cx_notification_change cx_notification_change_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_notification_change
    ADD CONSTRAINT cx_notification_change_pkey PRIMARY KEY (id_notification_change);


--
-- Name: cx_prediction cx_prediction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_prediction
    ADD CONSTRAINT cx_prediction_pkey PRIMARY KEY (id_prediction);


--
-- Name: cx_project cx_project_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_project
    ADD CONSTRAINT cx_project_pkey PRIMARY KEY (id_project);


--
-- Name: cx_read_notification cx_read_notification_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_read_notification
    ADD CONSTRAINT cx_read_notification_pk PRIMARY KEY (id_read_notification);


--
-- Name: cx_segment cx_segment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_segment
    ADD CONSTRAINT cx_segment_pkey PRIMARY KEY (id_segment);


--
-- Name: cx_training cx_training_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_training
    ADD CONSTRAINT cx_training_pkey PRIMARY KEY (id_training);


--
-- Name: unique_cx_client_model; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX unique_cx_client_model ON public.cx_client_model USING btree (id_cliente, id_model);


--
-- Name: cx_campaign cx_campaign_id_mailtemplate_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_campaign
    ADD CONSTRAINT cx_campaign_id_mailtemplate_fkey FOREIGN KEY (id_mailtemplate) REFERENCES public.cx_mailtemplate(id_mailtemplate);


--
-- Name: cx_campaign cx_campaign_id_segment_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_campaign
    ADD CONSTRAINT cx_campaign_id_segment_fkey FOREIGN KEY (id_segment) REFERENCES public.cx_segment(id_segment);


--
-- Name: cx_campaign_stats cx_campaign_stats_id_campaign_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_campaign_stats
    ADD CONSTRAINT cx_campaign_stats_id_campaign_fkey FOREIGN KEY (id_campaign) REFERENCES public.cx_campaign(id_campaign);


--
-- Name: cx_client_model cx_client_model_id_model_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_client_model
    ADD CONSTRAINT cx_client_model_id_model_fkey FOREIGN KEY (id_model) REFERENCES public.cx_model(id_model);


--
-- Name: cx_marketplace cx_marketplace_id_model_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_marketplace
    ADD CONSTRAINT cx_marketplace_id_model_fkey FOREIGN KEY (id_model) REFERENCES public.cx_model(id_model);


--
-- Name: cx_prediction cx_prediction_id_statistic_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_prediction
    ADD CONSTRAINT cx_prediction_id_statistic_fkey FOREIGN KEY (id_statistic) REFERENCES public.cx_ml_statistic(id_statistic);


--
-- Name: cx_prediction cx_prediction_id_training_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_prediction
    ADD CONSTRAINT cx_prediction_id_training_fkey FOREIGN KEY (id_training) REFERENCES public.cx_training(id_training);


--
-- Name: cx_segment cx_segment_id_project_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_segment
    ADD CONSTRAINT cx_segment_id_project_fkey FOREIGN KEY (id_project) REFERENCES public.cx_project(id_project);


--
-- Name: cx_training cx_training_id_metric_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_training
    ADD CONSTRAINT cx_training_id_metric_fkey FOREIGN KEY (id_metric) REFERENCES public.cx_ml_metric(id_metric);


--
-- Name: cx_training cx_training_id_model_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cx_training
    ADD CONSTRAINT cx_training_id_model_fkey FOREIGN KEY (id_model) REFERENCES public.cx_model(id_model);


--
-- PostgreSQL database dump complete
--

