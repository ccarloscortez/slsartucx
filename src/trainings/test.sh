#!/usr/bin/env sh
export ENVIRONMENT="Local"

set -e

bold='\033[1m'
normal='\033[0m'
green='\033[0;32m'

sls plugin install -n serverless-python-requirements
echo
echo "${green} ${bold}✓ sls invoke local -f cx_list_trainings -d '{\"fecha\": \"2018-06-24 17:05:55\", \"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\", \"id_cliente\": \"vidasecurity\", \"id_usuario\": \"ccortez\"}'${normal}"
echo
sls invoke local -f cx_list_trainings -d '{"fecha": "2018-06-24 17:05:55", "id_app": "artucx", "connection_string": "dbname=ac user=postgres password=postgres host=localhost", "id_cliente": "vidasecurity", "id_usuario": "ccortez"}'

echo
echo "${green} ${bold}✓ sls invoke local -f cx_training_details -d '{\"fecha\": \"2018-06-24 17:05:55\", \"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\", \"id_cliente\": \"vidasecurity\", \"id_usuario\": \"ccortez\", \"id_metric\":\"cxmetric001\"}'${normal}"
echo
sls invoke local -f cx_training_details -d '{"fecha": "2018-06-24 17:05:55", "id_app": "artucx", "connection_string": "dbname=ac user=postgres password=postgres host=localhost", "id_cliente": "vidasecurity", "id_usuario": "ccortez", "id_metric":"cxmetric001"}'


echo
echo "${green} ${bold}✓ sls invoke local -f cx_train_model -d '{\"fecha\": \"2018-06-24 17:05:55\", \"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\", \"id_cliente\": \"vidasecurity\", \"id_usuario\": \"ccortez\", \"id_metric\":\"cxmetric001\"}'${normal}"
echo
sls invoke local -f cx_train_model -d '{"fecha": "2018-06-24 17:05:55", "id_app": "artucx", "connection_string": "dbname=ac user=postgres password=postgres host=localhost", "id_cliente": "vidasecurity", "id_usuario": "ccortez", "id_training":"cxtrain001", "access_key_dest": "AKIAIOOOOOOOOOO", "region_dest": "us-east-1", "secret_key_dest": "11111111111111111111111111111"}'

echo
echo "${green} ${bold}✓ sls invoke local -f cx_uploads3_traininput -d '{\"access_key_dest\": \"AKIAIOOOOOOOOOO\", \"region_dest\": \"us-east-1\", \"secret_key_dest\": \"11111111111111111111111111111\", \"id_cliente\": \"vidasecurity\", \"name\": \"test\", \"extension\": \"csv\", \"content\": \"QmVnaW4gd2l0aCB0aGUgInR5cGUgKG9yIHBhc3RlKSBoZXJlLi4uIiBhcmVhIHRvIGVudGVyIHlvdXIgZGF0YSwgdGhlbiBoaXQgdGhlICJlbmNvZGUiIG9yICJkZWNvZGUiIGJ1dHRvbnMgcmVzcGVjdGl2ZWx5LiBBZnRlciBhIGJsaW5rIG9mIGFueSBleWUsIHRoZSByZXN1bHRzIHdpbGwgYmUgc2hvd24gYmVsb3cgdGhlc2UgYnV0dG9ucy4gVGhhdCdzIGFsbCENCg==\"}'${normal}"
echo
sls invoke local -f cx_uploads3_traininput -d '{"access_key_dest": "AKIAIOOOOOOOOOO", "region_dest": "us-east-1", "secret_key_dest": "11111111111111111111111111111", "id_cliente": "vidasecurity", "name": "test", "extension": "csv", "content": "QmVnaW4gd2l0aCB0aGUgInR5cGUgKG9yIHBhc3RlKSBoZXJlLi4uIiBhcmVhIHRvIGVudGVyIHlvdXIgZGF0YSwgdGhlbiBoaXQgdGhlICJlbmNvZGUiIG9yICJkZWNvZGUiIGJ1dHRvbnMgcmVzcGVjdGl2ZWx5LiBBZnRlciBhIGJsaW5rIG9mIGFueSBleWUsIHRoZSByZXN1bHRzIHdpbGwgYmUgc2hvd24gYmVsb3cgdGhlc2UgYnV0dG9ucy4gVGhhdCdzIGFsbCENCg=="}'


echo
echo "${green} ${bold}✓ sls invoke local -f cx_create_training -d '{\"access_key_dest\": \"AKIAIOOOOOOOOOO\", \"region_dest\": \"us-east-1\", \"secret_key_dest\": \"11111111111111111111111111111\", \"id_cliente\": \"vidasecurity\", \"name\": \"test\", \"extension\": \"csv\", \"content\": \"QmVnaW4gd2l0aCB0aGUgInR5cGUgKG9yIHBhc3RlKSBoZXJlLi4uIiBhcmVhIHRvIGVudGVyIHlvdXIgZGF0YSwgdGhlbiBoaXQgdGhlICJlbmNvZGUiIG9yICJkZWNvZGUiIGJ1dHRvbnMgcmVzcGVjdGl2ZWx5LiBBZnRlciBhIGJsaW5rIG9mIGFueSBleWUsIHRoZSByZXN1bHRzIHdpbGwgYmUgc2hvd24gYmVsb3cgdGhlc2UgYnV0dG9ucy4gVGhhdCdzIGFsbCENCg==\"}'${normal}"
echo
sls invoke local -f cx_create_training -d '{"access_key_dest": "AKIAIOOOOOOOOOO", "region_dest": "us-east-1", "secret_key_dest": "11111111111111111111111111111", "id_cliente": "vidasecurity", "connection_string": "dbname=ac user=postgres password=postgres host=localhost", "id_model":"cxm001", "name": "name", "type":"type", "dataset_url_training": "s3://artucxstorage/vidasecurity_train_dataset.csv", "training_label": "STSPOL", "subtype": "subtype", "is_free": false, "id_usuario": "ccortez", "datos_api_endpoint": {"FunctionName": "FunctionName"}}'
