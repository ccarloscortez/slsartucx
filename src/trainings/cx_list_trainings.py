from common.db.session import db_session
from common.db.models import CxTraining
from common.utils import to_postgresql
import http


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:
        trainings = db.query(CxTraining).\
            filter(CxTraining.id_cliente == event["id_cliente"],
                   CxTraining.id_usuario == event["id_usuario"])

        for t in trainings:
            response.append({
                "creation_time": t.creation_time.strftime("%Y-%m-%d %H:%M:%S") if t.creation_time else None,
                "id_training": t.id_training,
                "id_model": t.id_model,
                "state": t.state,
                "type": t.type,
                "subtype": t.subtype,
                "modeling_data": t.modeling_data,
                "is_free": t.is_free,
                "id_metric": t.id_metric,
                "name": t.name,

            })

    return {"status": http.HTTPStatus.OK, "response": response}
