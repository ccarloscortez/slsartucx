from common.db.session import db_session
from common.db.models import  CxTraining
from common.utils import to_postgresql
from common.logs import logger
import http
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.exc import MultipleResultsFound
import ulid


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:
        try:
            training = db.query(CxTraining).\
                filter(CxTraining.id_cliente == event["id_cliente"],
                       CxTraining.id_usuario == event["id_usuario"],
                       CxTraining.id_metric == event["id_metric"]).one()
        except MultipleResultsFound:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }
        except NoResultFound:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.NOT_FOUND,
                "error_id": error_id.str,
            }
        if training.cx_ml_metric:
            response.append({
                "id_model": training.id_model,
                "id_training": training.id_training,
                "id_metric": training.cx_ml_metric.id_metric,
                "type": training.cx_ml_metric.type,
                "accuracy": training.cx_ml_metric.accuracy,
                "fscore": training.cx_ml_metric.fscore,
                "recall": training.cx_ml_metric.recall,
                "precision": training.cx_ml_metric.precision,
                "roc": training.cx_ml_metric.roc,
                "roc_curve": training.cx_ml_metric.roc_curve,
                "importances": training.cx_ml_metric.importances,
                "confusion_matrix": training.cx_ml_metric.confusion_matrix,
            })

    return {"status": http.HTTPStatus.OK, "response": response}
