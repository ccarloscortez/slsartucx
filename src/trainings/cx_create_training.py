from common.db.session import db_session
from common.db.models import CxTraining
from common.logs import logger
from common.utils import to_postgresql, get_boto3_settings
import http
import ulid
import datetime
import boto3
import json


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:

        cx_training = CxTraining(id_training=ulid.new().str,
                                 id_model=event["id_model"],
                                 name=event["name"],
                                 state="processing",
                                 type=event["type"],
                                 dataset_url_training=event["dataset_url_training"],
                                 training_label=event["training_label"],
                                 subtype=event["subtype"],
                                 is_free=event["is_free"],
                                 creation_time=datetime.datetime.now(),
                                 id_cliente=event["id_cliente"],
                                 id_usuario=event["id_usuario"])

        db.add(cx_training)

        try:
            db.commit()
        except:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }

        kinesis_client = boto3.client(u'kinesis', **get_boto3_settings(event))
        event["id_training"] = cx_training.id_training
        kinesis_client.put_record(
            StreamName='artucx-training-stream',
            Data=json.dumps(event),
            PartitionKey='shardId-000000000000',
        )

        response.append({
            "creation_time": cx_training.creation_time.strftime(
                "%Y-%m-%d %H:%M:%S") if cx_training.creation_time else None,
            "id_training": cx_training.id_training,
            "id_model": cx_training.id_model,
            "state": cx_training.state,
            "type": cx_training.type,
            "subtype": cx_training.subtype,
            "modeling_data": cx_training.modeling_data,
            "is_free": cx_training.is_free,
            "id_metric": cx_training.id_metric,
            "name": cx_training.name,
        })

    return {"status": http.HTTPStatus.OK, "response": response}
