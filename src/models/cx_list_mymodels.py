from common.db.session import db_session
from common.db.models import CxClientModel, CxModel
from common.logs import logger
from common.utils import to_postgresql
import http
import json
import ulid


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:
        client_models = db.query(CxClientModel).\
            filter(CxClientModel.id_cliente == event["id_cliente"],
                   CxClientModel.addition_user == event["id_usuario"])

        for cm in client_models:
            response.append({
                "id_model": cm.cx_model.id_model,
                "name": cm.cx_model.name,
                "desc": cm.cx_model.desc,
                "category": cm.cx_model.category,
                "industry": cm.cx_model.industry,
                "version": cm.cx_model.version,
                "addition_date": cm.addition_date.strftime("%Y-%m-%d %H:%M:%S"),
                "creation_time": cm.cx_model.creation_time.strftime("%Y-%m-%d %H:%M:%S")

            })

    return {"status": http.HTTPStatus.OK, "response": response}
