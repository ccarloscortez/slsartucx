#!/usr/bin/env sh

export ENVIRONMENT="Local"

set -e

bold='\033[1m'
normal='\033[0m'
green='\033[0;32m'

sls plugin install -n serverless-python-requirements
echo
echo "${green} ${bold}✓ sls invoke local -f cx_list_mymodels -d '{\"fecha\": \"2018-06-24 17:05:55\", \"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\", \"id_cliente\": \"vidasecurity\"}'${normal}"
echo
sls invoke local -f cx_list_mymodels -d '{"fecha": "2018-06-24 17:05:55", "id_app": "artucx", "connection_string": "dbname=ac user=postgres password=postgres host=localhost", "id_cliente": "vidasecurity", "id_usuario":"ccortez"}'