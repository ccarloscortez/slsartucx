import datetime
import http
import ulid
from common.db.models import CxReadNotification
from common.db.session import db_session
from common.logs import logger
from common.utils import to_postgresql


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:

        rns = CxReadNotification(id_read_notification=ulid.new().str,
                                 id_usuario=event["id_usuario"],
                                 id_notification_change=event["id_notification_change"],
                                 read_at=datetime.datetime.now())

        db.add(rns)

        try:
            db.commit()
        except:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }

        response.append({
            "id_read_notification": rns.id_read_notification,
            "id_usuario": rns.id_usuario,
            "id_notification_change": rns.id_notification_change,
            "read_at": rns.read_at.strftime("%Y-%m-%d %H:%M:%S"),
        })

    return {"status": http.HTTPStatus.OK, "response": response}
