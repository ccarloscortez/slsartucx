import http

from common.db.models import CxReadNotification
from common.db.session import db_session
from common.utils import to_postgresql


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:
        reads = db.query(CxReadNotification). \
            filter(CxReadNotification.id_usuario == event["id_usuario"])

        for read in reads:
            response.append({
                "id_read_notification": read.id_read_notification,
                "id_usuario": read.id_usuario,
                "id_notification_change": read.id_notification_change,
                "read_at": read.read_at.strftime("%Y-%m-%d %H:%M:%S"),
            })

        return {"status": http.HTTPStatus.OK, "response": response}
