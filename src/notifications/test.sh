#!/usr/bin/env sh
export ENVIRONMENT="Local"

set -e

bold='\033[1m'
normal='\033[0m'
green='\033[0;32m'

sls plugin install -n serverless-python-requirements
echo
echo "${green} ${bold}✓ sls invoke local -f cx_list_notifications -d '{\"fecha\": \"2018-06-24 17:05:55\", \"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\", \"id_cliente\": \"vidasecurity\", \"id_usuario\": \"ccortez\"}'${normal}"
echo
sls invoke local -f cx_list_notifications -d '{"fecha": "2018-06-24 17:05:55", "id_app": "artucx", "connection_string": "dbname=ac user=postgres password=postgres host=localhost", "id_cliente": "vidasecurity", "id_usuario": "ccortez"}'

echo
echo "${green} ${bold}✓ sls invoke local -f cx_list_notification_reads -d '{\"fecha\": \"2018-06-24 17:05:55\", \"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\", \"id_cliente\": \"vidasecurity\", \"id_usuario\": \"ccortez\"}'${normal}"
echo
sls invoke local -f cx_list_notification_reads -d '{"fecha": "2018-06-24 17:05:55", "id_app": "artucx", "connection_string": "dbname=ac user=postgres password=postgres host=localhost", "id_cliente": "vidasecurity", "id_usuario": "ccortez"}'

echo
echo "${green} ${bold}✓ sls invoke local -f cx_create_notification_read -d '{\"fecha\": \"2018-06-24 17:05:55\", \"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\", \"id_cliente\": \"vidasecurity\", \"id_usuario\": \"ccortez\"}'${normal}"
echo
sls invoke local -f cx_create_notification_read -d '{"fecha": "2018-06-24 17:05:55", "id_app": "artucx", "connection_string": "dbname=ac user=postgres password=postgres host=localhost", "id_cliente": "vidasecurity", "id_usuario": "cristobal", "id_notification_change": "01CMS3SM5022YY0Y8WPNKF8X7C"}'
