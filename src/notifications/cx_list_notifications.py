from common.db.session import db_session
from common.db.models import CxNotificationChange
from common.utils import to_postgresql
import http


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:
        notifications = db.query(CxNotificationChange).\
            filter(CxNotificationChange.actor_id_cliente == event["id_cliente"])

        for notification in notifications:
            response.append({
                "id_notification_change": notification.id_notification_change,
                "entity_type": notification.entity_type,
                "entity_id":  notification.entity_id,
                "entity_name":  notification.entity_name,
                "action_on_entity": notification.action_on_entity.name,
                "actor_id_usuario": notification.actor_id_usuario,
                "actor_id_cliente": notification.actor_id_cliente,
                "created_at":  notification.created_at.strftime("%Y-%m-%d %H:%M:%S"),

            })

    return {"status": http.HTTPStatus.OK, "response": response}
