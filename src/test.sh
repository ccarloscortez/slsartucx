#!/usr/bin/env bash

bold='\033[1m'
normal='\033[0m'
red='\033[0;31m'

echo "${bold}${red}predictions${normal}\n"
(cd predictions && sh test.sh)

echo "${bold}${red}models${normal}\n"
(cd models && sh test.sh)

echo "${bold}${red}trainings${normal}\n"
(cd trainings && sh test.sh)

echo "${bold}${red}campaigns${normal}\n"
(cd campaigns && sh test.sh)

echo "${bold}${red}indicators${normal}\n"
(cd indicators && sh test.sh)

echo "${bold}${red}templates${normal}\n"
(cd templates && sh test.sh)
