from common.db.session import db_session
from common.db.models import CxClientModel
from common.logs import logger
from common.utils import to_postgresql
from dateutil.parser import parse

import http
import ulid


def handler(event, context):

    with db_session(to_postgresql(event["connection_string"])) as db:
        try:
            ccm = CxClientModel(id_cliente=event["id_cliente"],
                                id_model=event["id_model"],
                                addition_date=parse(event["fecha"]),
                                addition_user=event["id_usuario"])
            db.add(ccm)
            db.commit()
        except Exception:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status":  http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }

    return {
        "status": http.HTTPStatus.OK,
        "response": [{
            "id_model": ccm.id_model,
            "id_cliente": ccm.id_cliente,
            "addition_date": ccm.addition_date.strftime("%Y-%m-%d %H:%M:%S"),
            "addition_user": ccm.id_usuario,
        }]
    }
