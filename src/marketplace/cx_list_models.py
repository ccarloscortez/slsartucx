from common.db.session import db_session
from common.db.models import CxMarketplace, CxModel, CxClientModel
from common.utils import to_postgresql
import http


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:
        models = db.query(CxModel).join(CxMarketplace).filter(CxMarketplace.name == "artucx")
        for m in models:
            response.append({
                "id_app": event["id_app"],
                "id_model": m.id_model,
                "name": m.name,
                "desc": m.desc,
                "category": m.category,
                "industry": m.industry,
                "version": m.version,
                "creation_time": m.creation_time.strftime("%Y-%m-%d %H:%M:%S"),
                "id_added": db.query(CxClientModel).
                                filter(CxClientModel.id_model == m.id_model,
                                       CxClientModel.id_cliente == event["id_cliente"],
                                       CxClientModel.addition_user == event["id_usuario"]).count() != 0

            })

    return {"status": http.HTTPStatus.OK, "response": response}
