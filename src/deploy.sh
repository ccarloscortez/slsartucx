#!/usr/bin/env bash

bold='\033[1m'
normal='\033[0m'
red='\033[0;31m'

echo "${bold}${red}deploy predictions${normal}\n"
(cd predictions && sls deploy)

echo "${bold}${red}deploy models${normal}\n"
(cd models && sls deploy)
q!
echo "${bold}${red}deploy trainings${normal}\n"
(cd trainings && sls deploy)

echo "${bold}${red}deploy campaigns${normal}\n"
(cd campaigns && sls deploy)

echo "${bold}${red}deploy indicators${normal}\n"
(cd indicators && sls deploy)

echo "${bold}${red}deploy marketplace${normal}\n"
(cd marketplace && sls deploy)

echo "${bold}${red}deploy templates${normal}\n"
(cd templates && sls deploy)
