import codecs
import copy
import csv
import datetime
import http
import io
from urllib.parse import urlparse

import boto3
import ulid
from common.db.models import CxPrediction, CxProject
from common.db.session import db_session
from common.logs import logger
from common.utils import to_postgresql, get_boto3_settings, upload_to_s3
from cx_create_project import handler as create_project
from cx_create_segment import handler as create_segment
from sqlalchemy.orm.exc import MultipleResultsFound
from sqlalchemy.orm.exc import NoResultFound


def handler(event, context):
    pinpoint_client = boto3.client('pinpoint',
                                   aws_access_key_id=event["access_key_dest"],
                                   aws_secret_access_key=event["secret_key_dest"],
                                   region_name=event["region_dest"])

    prediction_name = ""
    positive_level_csv = ""
    id_cliente = ""
    # get prediction
    with db_session(to_postgresql(event["connection_string"])) as db:
        try:
            prediction = db.query(CxPrediction). \
                filter(CxPrediction.id_cliente == event["id_cliente"],
                       CxPrediction.id_prediction == event["id_prediction"]).one()
        except MultipleResultsFound:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }
        except NoResultFound:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.NOT_FOUND,
                "error_id": error_id.str,
            }
        prediction_name = prediction.name
        if prediction.cx_ml_statistic is None:
            error_id = ulid.new()
            logger.error(f"id_error:  {error_id.str}, event: {event}, prediction without not statistic")
            return {
                "status": http.HTTPStatus.NOT_FOUND,
                "error_id": error_id.str,
            }

        positive_level_csv = prediction.cx_ml_statistic.positive_level_csv
        id_cliente = prediction.id_cliente

    if positive_level_csv == "":
        error_id = ulid.new()
        logger.error(f"id_error:  {error_id.str}, event: {event}, prediction without  positive_level_csv")
        return {
            "status": http.HTTPStatus.NOT_FOUND,
            "error_id": error_id.str,
        }
    csv_url = urlparse(positive_level_csv)
    #
    # check if positive_level_csv have ChannelType and Address, also if custom attributes is < 20
    #
    s3 = boto3.resource(u's3', **get_boto3_settings(event))
    # get a handle on the bucket that holds your file
    bucket = s3.Bucket(csv_url.netloc)
    # get a handle on the object you want (i.e. your file)
    obj = bucket.Object(key=csv_url.path.strip('/'))
    s3_response = obj.get()

    csv_file = csv.DictReader(codecs.getreader('utf-8')(s3_response[u'Body']))
    fieldnames = csv_file.fieldnames

    if ("ChannelType" not in fieldnames) or ("Address" not in fieldnames):
        error_id = ulid.new()
        logger.error(f"id_error:  {error_id.str}, event: {event}, missing ChannelType or Address in csv")
        return {
            "status": http.HTTPStatus.NOT_FOUND,
            "error_id": error_id.str,
        }

    if len(fieldnames) > 22:
        error_id = ulid.new()
        logger.error(f"id_error:  {error_id.str}, event: {event}, maximum numbers of custom attributes (20) reached")
        return {
            "status": http.HTTPStatus.NOT_FOUND,
            "error_id": error_id.str,
        }

    custom_fieldnames = []
    for f in fieldnames:
        if f in ["ChannelType", "Address"]:
            custom_fieldnames.append(f)
        else:
            custom_fieldnames.append(f'Attributes.{f}')

    # create new segment csv
    output = io.StringIO()
    csv_file.fieldnames = custom_fieldnames
    writer = csv.DictWriter(output, fieldnames=custom_fieldnames)
    writer.writeheader()
    for row in csv_file:
        writer.writerow(row)
    upload_event = copy.deepcopy(event)

    upload_event["content"] = output.getvalue()
    date = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    segment_url = upload_to_s3(upload_event,
                               bucket="artucxstorage",
                               key=f'segments/uploads/campaign-input-{id_cliente}-{date}.csv')

    # create project
    project_event = copy.deepcopy(event)
    project_event["name"] = f"[Smart Campaign] ${prediction_name}"
    project_event["desc"] = f"proyecto creado usando smart campaign para predicción: {prediction_name}"
    create_project_response = create_project(project_event, context)
    if create_project_response["status"] == http.HTTPStatus.OK:
        # project have been create, now create the segment
        segment_event = copy.deepcopy(event)
        segment_event["id_project"] = create_project_response["response"][0]["id_project"]
        segment_event["name"] = prediction_name
        segment_event["desc"] = f"segmento creado desde smart campaign para predicción: {prediction_name}"
        segment_event["channel_type"] = "email"
        segment_event["campaign_input_url"] = segment_url
        create_segment_response = create_segment(segment_event, context)
        if create_segment_response["status"] == http.HTTPStatus.OK:
            return {"status": http.HTTPStatus.OK, "response": [{
                "segment": create_segment_response["response"],
                "project": create_project_response["response"]}
            ]}
        else:
            # delete project because segment creation failed
            pinpoint_client.delete_app(ApplicationId=create_project_response["response"][0]["id_project"])
            with db_session(to_postgresql(event["connection_string"])) as db:
                db.query(CxProject). \
                    filter(CxProject.id_project == create_project_response["response"][0]["id_project"]).delete()
                db.commit()
            return create_segment_response
    else:
        return create_project_response
