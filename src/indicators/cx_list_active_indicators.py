import http

from common.db.models import CxPrediction
from common.db.session import db_session
from common.utils import to_postgresql


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:
        predictions = db.query(CxPrediction). \
            filter(CxPrediction.id_cliente == event["id_cliente"],
                   CxPrediction.id_usuario == event["id_usuario"],
                   CxPrediction.on_dashboard == True)
        for prediction in predictions:
            if prediction.cx_ml_statistic is None:
                continue
            response.append({
                "id_prediction": prediction.id_prediction,
                "on_dashboard": prediction.on_dashboard,
                "id_statistic": prediction.id_statistic,
                "report_name": prediction.cx_ml_statistic.report_name,
                "report_desc": prediction.cx_ml_statistic.report_desc,
                "total_entries": prediction.cx_ml_statistic.total_entries,
                "positive_level_name": prediction.cx_ml_statistic.positive_level_name,
                "negative_level_name": prediction.cx_ml_statistic.negative_level_name,
                "positive_level": prediction.cx_ml_statistic.positive_level,
                "negative_level": prediction.cx_ml_statistic.negative_level,
                "positive_level_csv": prediction.cx_ml_statistic.positive_level_csv,
                "negative_level_csv": prediction.cx_ml_statistic.negative_level_csv,
                "id_cliente": prediction.cx_ml_statistic.id_cliente,
                "id_usuario": prediction.cx_ml_statistic.id_usuario,
            })

    return {"status": http.HTTPStatus.OK, "response": response}
