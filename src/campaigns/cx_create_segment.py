from common.db.session import db_session
from common.db.models import CxSegment, CxCampaign
from common.utils import to_postgresql, get_boto3_settings
import http
import boto3
from common.logs import logger
import ulid
import datetime


def handler(event, context):
    response = []

    with db_session(to_postgresql(event["connection_string"])) as db:
        pinpoint_client = boto3.client('pinpoint', **get_boto3_settings(event))

        try:
            create_importjob_response = pinpoint_client.create_import_job(
                ApplicationId=event["id_project"],
                ImportJobRequest={
                    "DefineSegment": True,
                    "Format": 'CSV',
                    "RegisterEndpoints": True,
                    "RoleArn": event["datos_api_endpoint"]["RoleArn"],
                    "S3Url": event["campaign_input_url"],
                    "SegmentName": event["name"],
                })
        except:
            # delete segment before leave
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }

        segment = CxSegment(id_segment=create_importjob_response['ImportJobResponse']['Definition']["SegmentId"],
                            id_project=event["id_project"],
                            name=event["name"],
                            desc=event["desc"],
                            campaign_input_url=event["campaign_input_url"],
                            creation_time=datetime.datetime.now(),
                            id_cliente=event["id_cliente"],
                            id_usuario=event["id_usuario"],
                            channel_type=event["channel_type"])
        db.add(segment)

        try:
            db.commit()
        except:
            # delete segment before leave
            pinpoint_client. \
                delete_segment(ApplicationId=event["id_project"],
                               SegmentId=create_importjob_response['ImportJobResponse']['Definition']["SegmentId"])

            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }

        response.append({
            "id_project": segment.id_project,
            "id_segment": segment.id_segment,
            "name": segment.name,
            "desc": segment.desc,
            "campaign_input_url": segment.campaign_input_url,
            "creation_time": segment.creation_time.strftime("%Y-%m-%d %H:%M:%S"),
            "channel_type": segment.channel_type,
            "id_app": event["id_app"],
            "id_cliente": segment.id_cliente,
            "id_usuario": segment.id_usuario,
            "campaign_count": db.query(CxCampaign).filter(CxCampaign.id_segment == segment.id_segment).count()
        })

    return {"status": http.HTTPStatus.OK, "response": response}
