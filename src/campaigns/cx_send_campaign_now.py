import codecs
import csv
import http
from urllib.parse import urlparse

import boto3
import pystache
import sendgrid
import ulid
from common.db.models import CxCampaign
from common.db.session import db_session
from common.logs import logger
from common.utils import to_postgresql, get_boto3_settings
from sendgrid.helpers.mail import *
from sqlalchemy.orm.exc import MultipleResultsFound
from sqlalchemy.orm.exc import NoResultFound
import json
import psycopg2
from psycopg2.extras import RealDictCursor
from psycopg2 import errorcodes as pg_errorcodes

def handler(event, context):
    response = []

    with db_session(to_postgresql(event["connection_string"])) as db:
        try:
            campaign = db.query(CxCampaign). \
                filter(CxCampaign.id_campaign == event["id_campaign"],
                       CxCampaign.id_cliente == event["id_cliente"],
                       CxCampaign.id_usuario == event["id_usuario"]).one()
            print("\n********** campaign.cx_segment")
            print(campaign.cx_segment)
            print("**************\n")
        except MultipleResultsFound:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }
        except NoResultFound:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.NOT_FOUND,
                "error_id": error_id.str,
            }

        # load csv
        connection_string = event["connection_string"]
        conn = psycopg2.connect(connection_string)
        conn.autocommit = True
        cur = conn.cursor(cursor_factory=RealDictCursor)
        query = "select * from cx_campaign a inner join cx_segment b on a.id_segment=b.id_segment where a.id_campaign='"+ event["id_campaign"] +"';"
        cur.execute(query)
        res=cur.fetchall()


        csv_url = urlparse(res[0]["campaign_input_url"])

        # get template ans title
        template_body = campaign.cx_mailtemplate.body
        template_title = campaign.cx_mailtemplate.title

        s3 = boto3.resource(u's3', **get_boto3_settings(event))
        # get a handle on the bucket that holds your file
        bucket = s3.Bucket(csv_url.netloc)
        # get a handle on the object you want (i.e. your file)
        obj = bucket.Object(key=csv_url.path.strip('/'))

        # get the object
        s3_response = obj.get()
        """
        sg = sendgrid.SendGridAPIClient(apikey=event["datos_api_endpoint"]["SENDGRID_API_KEY"])
        for row in csv.DictReader(codecs.getreader('utf-8')(s3_response[u'Body'])):
            template_model = {}
            for key, value in row.items():
                if key.startswith("Attributes."):
                    template_model[key.replace("Attributes.", "")] = value

            rendered_body = pystache.render(template_body, template_model)
            rendered_title = pystache.render(template_title, template_model)
            try:
                from_email = Email(campaign.cx_segment.cx_project.from_address)
                to_email = Email(row["Address"])
                subject = rendered_title
                content = Content('text/html', rendered_body)
                mail = Mail(from_email, subject, to_email, content)
                mail.add_category(Category(name=campaign.id_campaign))
                tracking_settings = TrackingSettings()
                tracking_settings.click_tracking = ClickTracking(enable=True, enable_text=True)
                tracking_settings.open_tracking = OpenTracking(enable=True, substitution_tag="%opentrack")
                mail.tracking_settings = tracking_settings
                response = sg.client.mail.send.post(request_body=mail.get())
            except Exception as e:
                error_id = ulid.new()
                logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
        """

    return {"status": http.HTTPStatus.OK, "response": response}
