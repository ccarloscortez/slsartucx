from common.db.session import db_session
from common.db.models import CxProject, CxSegment
from common.utils import to_postgresql
import http
import boto3
from common.logs import logger
import ulid
import datetime


def handler(event, context):
    response = []

    from_address = event.get("from_address", event["datos_api_endpoint"]["FromAddress"])

    with db_session(to_postgresql(event["connection_string"])) as db:
        pinpoint_client = boto3.client('pinpoint',
                                       aws_access_key_id=event["access_key_dest"],
                                       aws_secret_access_key=event["secret_key_dest"],
                                       region_name=event["region_dest"])
        try:
            pinpoint_response = pinpoint_client.create_app(CreateApplicationRequest={'Name': event["name"]})
        except:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }

        try:
            pinpoint_client.update_email_channel(
                ApplicationId=pinpoint_response["ApplicationResponse"]["Id"],
                EmailChannelRequest={
                    'Enabled': True,
                    'FromAddress': from_address,
                    'Identity': event["datos_api_endpoint"]["Identity"],
                    'RoleArn': event["datos_api_endpoint"]["RoleArn"],
                }
            )
        except:
            # delete app before leave
            pinpoint_client.delete_app(ApplicationId=pinpoint_response["ApplicationResponse"]["Id"])

            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }

        project = CxProject(id_project=pinpoint_response["ApplicationResponse"]["Id"],
                            name=event["name"],
                            desc=event["desc"],
                            from_address=from_address,
                            creation_date=datetime.datetime.now(),
                            id_cliente=event["id_cliente"],
                            id_usuario=event["id_usuario"])
        db.add(project)

        try:
            db.commit()
        except:
            # delete app before leave
            pinpoint_client.delete_app(ApplicationId=pinpoint_response["ApplicationResponse"]["Id"])

            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }
        response.append({
            "id_project": project.id_project,
            "name": project.name,
            "desc": project.desc,
            "from_address": project.from_address,
            "creation_date": project.creation_date.strftime("%Y-%m-%d %H:%M:%S"),
            "id_cliente": project.id_cliente,
            "id_usuario": project.id_usuario,
            "segment_count": 0
        })

    return {"status": http.HTTPStatus.OK, "response": response}
