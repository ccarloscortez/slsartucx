from common.utils import upload_to_s3
import http
import datetime


def handler(event, context):
    response = []
    date = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    client = event["id_cliente"]
    url = upload_to_s3(event,
                       bucket="artucxstorage",
                       key=f'segments/uploads/campaign-input-{client}-{date}.{event["extension"]}')

    response.append({"url": url})
    
    return {"status": http.HTTPStatus.OK, "response": response}