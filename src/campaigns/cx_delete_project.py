from common.db.session import db_session
from common.db.models import CxSegment, CxProject
from common.utils import to_postgresql, get_boto3_settings
import http
import boto3
from common.logs import logger
import ulid
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.exc import MultipleResultsFound


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:
        # get project
        try:
            project = db.query(CxProject). \
                filter(CxProject.id_cliente == event["id_cliente"],
                       CxProject.id_usuario == event["id_usuario"],
                       CxProject.id_project == event["id_project"]).one()
        except MultipleResultsFound:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }
        except NoResultFound:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.NOT_FOUND,
                "error_id": error_id.str,
            }

        project_response = {
            "id_project": project.id_project
        }

        # delete project on aws
        pinpoint_client = boto3.client('pinpoint', **get_boto3_settings(event))

        try:
            pinpoint_client.delete_app(ApplicationId=project.id_project)
        except:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }

        # delete segment in the database
        db.delete(project)

        try:
            db.commit()
        except:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }

        response.append(project_response)

    return {"status": http.HTTPStatus.OK, "response": response}
