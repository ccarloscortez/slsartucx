from common.db.session import db_session
from common.db.models import CxProject, CxSegment
from common.utils import to_postgresql
import http


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:
        projects = db.query(CxProject). \
            filter(CxProject.id_cliente == event["id_cliente"],
                   CxProject.id_usuario == event["id_usuario"])

        for p in projects:
            response.append({
                "id_project": p.id_project,
                "name": p.name,
                "desc": p.desc,
                "creation_date": p.creation_date.strftime("%Y-%m-%d %H:%M:%S"),
                "id_app": event["id_app"],
                "id_cliente": p.id_cliente,
                "id_usuario": p.id_usuario,
                "segment_count": db.query(CxSegment).filter(CxSegment.id_cliente == event["id_cliente"],
                                                            CxSegment.id_project == p.id_project,
                                                            CxSegment.id_usuario == event["id_usuario"]).count()
            })

    return {"status": http.HTTPStatus.OK, "response": response}
