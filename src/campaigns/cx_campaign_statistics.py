from common.db.session import db_session
from common.db.models import CxCampaign
from common.utils import to_postgresql, get_postmark_settings
import http
import boto3
from common.logs import logger
import ulid
import datetime
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.exc import MultipleResultsFound
from postmarker.core import PostmarkClient


def handler(event, context):
    response = []

    with db_session(to_postgresql(event["connection_string"])) as db:
        try:
            campaign = db.query(CxCampaign).\
                filter(CxCampaign.id_campaign == event["id_campaign"],
                       CxCampaign.id_cliente == event["id_cliente"],
                       CxCampaign.id_usuario == event["id_usuario"]).one()
        except MultipleResultsFound:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }
        except NoResultFound:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.NOT_FOUND,
                "error_id": error_id.str,
            }

        postmark = PostmarkClient(**get_postmark_settings(event))

        stats = postmark.stats.overview(tag=campaign.id_campaign)

    response.append({"fecha": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                     "id_app": event["id_app"],
                     "id_cliente": campaign.id_cliente,
                     "id_usuario": campaign.id_usuario,
                     "id_campaign": campaign.id_campaign,
                     "id_segment": campaign.id_segment,
                     "campaign_name": campaign.name,
                     "open_rate": 0,
                     "delivered":  0,
                     "state": campaign.state,
                     "stats_data": stats
                    })

    return {"status": http.HTTPStatus.OK, "response": response}
