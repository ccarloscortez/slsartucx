from common.db.session import db_session
from common.db.models import CxCampaign
from common.utils import to_postgresql
import http
import datetime


def handler(event, context):
    response = []

    with db_session(to_postgresql(event["connection_string"])) as db:
        campaigns = db.query(CxCampaign).filter(CxCampaign.id_cliente == event["id_cliente"],
                                                CxCampaign.id_usuario == event["id_usuario"])

        for campaign in campaigns:
            response.append({"fecha": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                             "id_app": event["id_app"],
                             "creation_time": campaign.creation_time.strftime("%Y-%m-%d %H:%M:%S"),
                             "id_cliente": campaign.id_cliente,
                             "id_usuario": campaign.id_usuario,
                             "id_campaign": campaign.id_campaign,
                             "id_segment": campaign.id_segment,
                             "frequency": campaign.frequency,
                             "start_time": campaign.start_time.strftime(
                                 "%Y-%m-%d %H:%M:%S") if campaign.start_time else None,
                             "end_time": campaign.end_time.strftime("%Y-%m-%d %H:%M:%S") if campaign.end_time else None,
                             "campaign_name": campaign.name,
                             "campaign_desc": campaign.desc,
                             "email_images": campaign.email_images,
                             "schedule_expression": campaign.schedule_expression,
                             "id_mailtemplate": campaign.id_mailtemplate,
                             "state": campaign.state,
                             "replacement_data_url": campaign.replacement_data_url})

    return {"status": http.HTTPStatus.OK, "response": response}
