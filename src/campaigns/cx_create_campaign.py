import datetime
import http
import json

import boto3
import ulid
from common.db.models import CxCampaign
from common.db.session import db_session
from common.logs import logger
from common.utils import to_postgresql, get_boto3_settings
from dateutil.parser import parse


def decrypt(texto,event):
    client = boto3.client('lambda',aws_access_key_id=event['access_key_dest'], aws_secret_access_key=event['secret_key_dest'], region_name=event['region_dest'])
    resp = client.invoke(
        FunctionName='crypt_rsa',
        InvocationType='RequestResponse',
        LogType='None',
        Payload=json.dumps({"action":"decrypt","texto":texto}),
        )
    resultado = json.loads(resp['Payload'].read().decode("utf-8"))
    if 'res' in resultado:
      return resultado["res"]
    else:
      return json.dumps(resultado)
    
def encrypt(texto,event):
    client = boto3.client('lambda',aws_access_key_id=event['access_key_dest'], aws_secret_access_key=event['secret_key_dest'], region_name=event['region_dest'])
    resp = client.invoke(
        FunctionName='crypt_rsa',
        InvocationType='RequestResponse',
        LogType='None',
        Payload=json.dumps({"action":"encrypt","texto":texto}),
        )
    resultado=json.loads(resp['Payload'].read().decode("utf-8"))
    if 'res' in resultado:
      return resultado["res"]
    else:
      return json.dumps(resultado)


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:
        campaign = CxCampaign(id_campaign=ulid.new().str,
                              id_segment=event["id_segment"],
                              creation_time=datetime.datetime.now(),
                              name=event["campaign_name"],
                              desc=event.get("campaign_desc", ""),
                              email_images=event["email_images"],
                              frequency=event["frequency"],
                              start_time=parse(
                                  event["start_time"]) if "start_time" in event else datetime.datetime.now(),
                              end_time=parse(event["end_time"]) if "end_time" in event else None,
                              schedule_expression=event["schedule_expression"],
                              id_cliente=event["id_cliente"],
                              id_usuario=event["id_usuario"],
                              id_mailtemplate=event["id_mailtemplate"],
                              replacement_data_url=event.get("replacement_data_url", ""),
                              state=True,
                              estado_envio='creado',
                              )
        #events_client = boto3.client('events', **get_boto3_settings(event))
        lambda_client = boto3.client('lambda', **get_boto3_settings(event))

        try:
          """
            rule_response = events_client.put_rule(Name=f"CAMPAIGN_{campaign.id_campaign}",
                                                   ScheduleExpression=campaign.schedule_expression,
                                                   State="ENABLED")
            lambda_client.add_permission(
                FunctionName=event["datos_api_endpoint"]["Name"],
                StatementId=f"CAMPAIGN_EVENT_{campaign.id_campaign}",
                Action='lambda:InvokeFunction',
                Principal='events.amazonaws.com',
                SourceArn=rule_response['RuleArn'],
            )
          """
        except:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }

       
        input_next=[{
        "Input": {
        "connection_string": encrypt(event["connection_string"],event),
        "id_campaign": campaign.id_campaign,
        "id_cliente": campaign.id_cliente,
        "id_usuario": campaign.id_usuario,
        "access_key_dest": encrypt(event["access_key_dest"],event),
        "secret_key_dest": encrypt(event["secret_key_dest"],event),
        "region_dest": event["region_dest"],
        "datos_api_endpoint": {
        "SENDGRID_API_KEY": event["datos_api_endpoint"]["SENDGRID_API_KEY"]}
        },
        "Arn": event["datos_api_endpoint"]["Arn"],
        "zona_horaria": event["zona_horaria"],
        "Id": f"CAMPAIGN_{campaign.id_campaign}_TRIGGER",
        }]

        campaign.datos_cx_campaign=json.dumps(input_next)
          


        db.add(campaign)
        db.commit()
        response.append({"fecha": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                         "id_app": event["id_app"],
                         "id_cliente": campaign.id_cliente,
                         "creation_time": campaign.creation_time.strftime("%Y-%m-%d %H:%M:%S"),
                         "id_usuario": campaign.id_usuario,
                         "id_campaign": campaign.id_campaign,
                         "id_segment": campaign.id_segment,
                         "frequency": campaign.frequency,
                         "start_time": campaign.start_time.strftime(
                             "%Y-%m-%d %H:%M:%S") if campaign.start_time else None,
                         "end_time": campaign.end_time.strftime("%Y-%m-%d %H:%M:%S") if campaign.end_time else None,
                         "campaign_name": campaign.name,
                         "campaign_desc": campaign.desc,
                         "email_images": campaign.email_images,
                         "schedule_expression": campaign.schedule_expression,
                         "id_mailtemplate": campaign.id_mailtemplate,
                         "state": campaign.state,
                         "replacement_data_url": campaign.replacement_data_url})

    return {"status": http.HTTPStatus.OK, "response": response}
