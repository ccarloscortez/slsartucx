#!/usr/bin/env sh

set -e
export ENVIRONMENT="Local"

bold='\033[1m'
normal='\033[0m'
green='\033[0;32m'

sls plugin install -n serverless-python-requirements
echo
echo "${green} ${bold}✓ sls invoke local -f cx_list_projects -d '{\"fecha\": \"2018-06-24 17:05:55\", \"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\", \"id_cliente\": \"vidasecurity\",  \"id_usuario\": \"ccortez\"}'${normal}"
echo
sls invoke local -f cx_list_projects -d '{"fecha": "2018-06-24 17:05:55", "id_app": "artucx", "connection_string": "dbname=ac user=postgres password=postgres host=localhost", "id_cliente": "vidasecurity", "id_usuario": "ccortez"}'

echo
echo "${green} ${bold}✓ sls invoke local -f cx_list_segments -d '{\"fecha\": \"2018-06-24 17:05:55\", \"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\", \"id_cliente\": \"vidasecurity\", \"id_project\":\"cxproj001\", \"id_usuario\": \"ccortez\"}'${normal}"
echo
sls invoke local -f cx_list_segments -d '{"fecha": "2018-06-24 17:05:55", "id_app": "artucx", "connection_string": "dbname=ac user=postgres password=postgres host=localhost", "id_cliente": "vidasecurity", "id_project":"cxproj001", "id_usuario": "ccortez"}'


echo
echo "${green} ${bold}✓ sls invoke local -f cx_create_campaign -d '{\"fecha\": \"2018-06-24 17:05:55\",  \"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\", \"id_cliente\": \"vidasecurity\", \"id_usuario\": \"ccortez\", \"access_key_dest\": \"AKIAIOOOOOOOOOO\",\"region_dest\": \"us-east-1\",\"secret_key_dest\": \"11111111111111111111111111111\",\"datos_api_endpoint\": {\"Arn\": \"LAMBDA_FUNCTION_ARN\", \"RoleArn\": \"IAM_ROLE_ARN\"}, \"id_segment\":\"cxseg001\", \"frequency\":\"daily\", \"start_time\":\"2018-06-24 17:05:55\", \"campaign_name\": \"campaign_test\", \"email_images\": {\"img1\": \"url1\", \"img2\":\"ulr2\"}, \"scheduleExpression\": \"cron(0 10 * * ? *)\", \"id_mailtemplate\": \"cxtemplate001\",\"replacement_data_url\": \"s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json\"}'${normal}"
echo
sls invoke local -f cx_create_campaign -d '
{ "fecha": "2018-06-24 17:05:55",
  "id_app": "artucx",
  "connection_string":
  "dbname=ac user=postgres password=postgres host=localhost",
  "id_cliente": "vidasecurity",
  "id_usuario": "ccortez",
  "access_key_dest": "AKIAIOOOOOOOOOO",
  "region_dest": "us-east-1",
  "schedule_expression": "cron(0 10 * * 1,2,3,4,5,6,7 *)",
  "secret_key_dest": "11111111111111111111111111111",
  "datos_api_endpoint": {"ScheduleExpression": "cron(0 10 * * ? *)", "Arn": "LAMBDA_FUNCTION_ARN", "RoleArn": "IAM_ROLE_ARN", "X-Postmark-Server-Token": "POSTMARK_API_TEST"},
  "id_segment":"cxseg001",
  "frequency":"daily",
  "start_time":"2018-06-24 17:05:55",
  "campaign_name": "campaign_test",
  "email_images": {"img1": "url1", "img2":"ulr2"},
  "id_mailtemplate": "cxtemplate001",
  "replacement_data_url": "s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json"
}'

echo
echo "${green} ${bold}✓ sls invoke local -f cx_list_campaigns -d '{\"fecha\": \"2018-06-24 17:05:55\", \"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\", \"id_cliente\": \"vidasecurity\", \"id_usuario\": \"ccortez\"}'${normal}"
echo
sls invoke local -f cx_list_campaigns -d '{ "fecha": "2018-06-24 17:05:55", "id_app": "artucx", "connection_string": "dbname=ac user=postgres password=postgres host=localhost", "id_cliente": "vidasecurity", "id_usuario": "ccortez"}'


echo
campaing_id=`sls invoke local -f cx_create_campaign -d '
{ "fecha": "2018-06-24 17:05:55",
  "id_app": "artucx",
  "connection_string":
  "dbname=ac user=postgres password=postgres host=localhost",
  "id_cliente": "vidasecurity",
  "id_usuario": "ccortez",
  "access_key_dest": "AKIAIOOOOOOOOOO",
  "region_dest": "us-east-1",
  "secret_key_dest": "11111111111111111111111111111",
  "datos_api_endpoint": {"ScheduleExpression": "cron(0 10 * * ? *)", "Arn": "LAMBDA_FUNCTION_ARN", "RoleArn": "IAM_ROLE_ARN", "X-Postmark-Server-Token": "POSTMARK_API_TEST"},
  "id_segment":"cxseg001",
  "frequency":"daily",
  "start_time":"2018-06-24 17:05:55",
  "campaign_name": "campaign_test",
  "email_images": {"img1": "url1", "img2":"ulr2"},
  "id_mailtemplate": "cxtemplate001",
  "replacement_data_url": "s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json"
}' | grep -Po '"id_campaign": "[0-9A-Z]+"' | grep -Po  '[A-Z0-9]+'`
echo "${green} ${bold}✓ sls invoke local -f cx_delete_campaign -d '{\"fecha\": \"2018-06-24 17:05:55\", \"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\", \"access_key_dest\": \"AKIAIOOOOOOOOOO\",\"region_dest\": \"us-east-1\",\"secret_key_dest\": \"11111111111111111111111111111\",  \"id_cliente\": \"vidasecurity\", \"id_usuario\": \"ccortez\", \"id_campaign\": \"${campaing_id}\"}'${normal}"
echo
sls invoke local -f cx_delete_campaign -d '{ "fecha": "2018-06-24 17:05:55", "id_app": "artucx", "connection_string": "dbname=ac user=postgres password=postgres host=localhost", "access_key_dest": "AKIAIOOOOOOOOOO", "region_dest": "us-east-1", "secret_key_dest": "11111111111111111111111111111", "id_cliente": "vidasecurity", "id_usuario": "ccortez", "id_campaign": "'"${campaing_id}"'"}'


echo
campaing_id=`sls invoke local -f cx_create_campaign -d '
{ "fecha": "2018-06-24 17:05:55",
  "id_app": "artucx",
  "connection_string":
  "dbname=ac user=postgres password=postgres host=localhost",
  "id_cliente": "vidasecurity",
  "id_usuario": "ccortez",
  "access_key_dest": "AKIAIOOOOOOOOOO",
  "region_dest": "us-east-1",
  "secret_key_dest": "11111111111111111111111111111",
  "datos_api_endpoint": {"ScheduleExpression": "cron(0 10 * * ? *)", "Arn": "LAMBDA_FUNCTION_ARN", "RoleArn": "IAM_ROLE_ARN", "X-Postmark-Server-Token": "POSTMARK_API_TEST"},
  "id_segment":"cxseg001",
  "frequency":"daily",
  "start_time":"2018-06-24 17:05:55",
  "campaign_name": "campaign_test",
  "email_images": {"img1": "url1", "img2":"ulr2"},
  "id_mailtemplate": "cxtemplate001",
  "replacement_data_url": "s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json"
}' | grep -Po '"id_campaign": "[0-9A-Z]+"' | grep -Po  '[A-Z0-9]+'`
echo "${green} ${bold}✓ sls invoke local -f cx_campaign_state -d '{\"fecha\": \"2018-06-24 17:05:55\", \"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\", \"access_key_dest\": \"AKIAIOOOOOOOOOO\",\"region_dest\": \"us-east-1\",\"secret_key_dest\": \"11111111111111111111111111111\", \"id_cliente\": \"vidasecurity\", \"id_usuario\": \"ccortez\", \"id_campaign\": \"${campaing_id}\"}'${normal}"
echo "disable"
sls invoke local -f cx_campaign_state -d '{ "fecha": "2018-06-24 17:05:55", "id_app": "artucx", "connection_string": "dbname=ac user=postgres password=postgres host=localhost", "access_key_dest": "AKIAIOOOOOOOOOO", "region_dest": "us-east-1", "secret_key_dest": "11111111111111111111111111111", "id_cliente": "vidasecurity", "id_usuario": "ccortez", "id_campaign": "'"${campaing_id}"'"}'
echo "enabled"
sls invoke local -f cx_campaign_state -d '{ "fecha": "2018-06-24 17:05:55", "id_app": "artucx", "connection_string": "dbname=ac user=postgres password=postgres host=localhost", "access_key_dest": "AKIAIOOOOOOOOOO", "region_dest": "us-east-1", "secret_key_dest": "11111111111111111111111111111", "id_cliente": "vidasecurity", "id_usuario": "ccortez", "id_campaign": "'"${campaing_id}"'"}'

echo
echo "${green} ${bold}✓ sls invoke local -f cx_campaign_statistics -d '{\"fecha\": \"2018-06-24 17:05:55\", \"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\", \"datos_api_endpoint\": {\"X-Postmark-Server-Token\":\"POSTMARK_API_TEST\"},  \"id_cliente\": \"vidasecurity\", \"id_usuario\": \"ccortez\"}'${normal}"
echo
sls invoke local -f cx_campaign_statistics -d '{ "fecha": "2018-06-24 17:05:55", "id_app": "artucx", "connection_string": "dbname=ac user=postgres password=postgres host=localhost", "datos_api_endpoint": {"X-Postmark-Server-Token":"POSTMARK_API_TEST"}, "id_cliente": "vidasecurity", "id_usuario": "ccortez", "id_campaign": "'"${campaing_id}"'"}'


echo
echo "${green} ${bold}✓ sls invoke local -f cx_upload_segment -d '{\"access_key_dest\": \"AKIAIOOOOOOOOOO\", \"region_dest\": \"us-east-1\", \"secret_key_dest\": \"11111111111111111111111111111\", \"id_cliente\": \"vidasecurity\", \"name\": \"test\", \"extension\": \"csv\", \"content\": \"QmVnaW4gd2l0aCB0aGUgInR5cGUgKG9yIHBhc3RlKSBoZXJlLi4uIiBhcmVhIHRvIGVudGVyIHlvdXIgZGF0YSwgdGhlbiBoaXQgdGhlICJlbmNvZGUiIG9yICJkZWNvZGUiIGJ1dHRvbnMgcmVzcGVjdGl2ZWx5LiBBZnRlciBhIGJsaW5rIG9mIGFueSBleWUsIHRoZSByZXN1bHRzIHdpbGwgYmUgc2hvd24gYmVsb3cgdGhlc2UgYnV0dG9ucy4gVGhhdCdzIGFsbCENCg==\"}'${normal}"
echo
sls invoke local -f cx_upload_segment -d '{"access_key_dest": "AKIAIOOOOOOOOOO", "region_dest": "us-east-1", "secret_key_dest": "11111111111111111111111111111", "id_cliente": "vidasecurity", "name": "test", "extension": "csv", "content": "QmVnaW4gd2l0aCB0aGUgInR5cGUgKG9yIHBhc3RlKSBoZXJlLi4uIiBhcmVhIHRvIGVudGVyIHlvdXIgZGF0YSwgdGhlbiBoaXQgdGhlICJlbmNvZGUiIG9yICJkZWNvZGUiIGJ1dHRvbnMgcmVzcGVjdGl2ZWx5LiBBZnRlciBhIGJsaW5rIG9mIGFueSBleWUsIHRoZSByZXN1bHRzIHdpbGwgYmUgc2hvd24gYmVsb3cgdGhlc2UgYnV0dG9ucy4gVGhhdCdzIGFsbCENCg=="}'


echo
echo "${green} ${bold}✓ sls invoke local -f cx_uploadimages_campaign -d '{\"access_key_dest\": \"AKIAIOOOOOOOOOO\", \"region_dest\": \"us-east-1\", \"secret_key_dest\": \"11111111111111111111111111111\", \"id_cliente\": \"vidasecurity\", \"name\": \"test\", \"extension\": \"csv\", \"content\": \"QmVnaW4gd2l0aCB0aGUgInR5cGUgKG9yIHBhc3RlKSBoZXJlLi4uIiBhcmVhIHRvIGVudGVyIHlvdXIgZGF0YSwgdGhlbiBoaXQgdGhlICJlbmNvZGUiIG9yICJkZWNvZGUiIGJ1dHRvbnMgcmVzcGVjdGl2ZWx5LiBBZnRlciBhIGJsaW5rIG9mIGFueSBleWUsIHRoZSByZXN1bHRzIHdpbGwgYmUgc2hvd24gYmVsb3cgdGhlc2UgYnV0dG9ucy4gVGhhdCdzIGFsbCENCg==\"}'${normal}"
echo
sls invoke local -f cx_uploadimages_campaign -d '{"access_key_dest": "AKIAIOOOOOOOOOO", "region_dest": "us-east-1", "secret_key_dest": "11111111111111111111111111111", "id_cliente": "vidasecurity", "name": "test", "extension": "csv", "content": "QmVnaW4gd2l0aCB0aGUgInR5cGUgKG9yIHBhc3RlKSBoZXJlLi4uIiBhcmVhIHRvIGVudGVyIHlvdXIgZGF0YSwgdGhlbiBoaXQgdGhlICJlbmNvZGUiIG9yICJkZWNvZGUiIGJ1dHRvbnMgcmVzcGVjdGl2ZWx5LiBBZnRlciBhIGJsaW5rIG9mIGFueSBleWUsIHRoZSByZXN1bHRzIHdpbGwgYmUgc2hvd24gYmVsb3cgdGhlc2UgYnV0dG9ucy4gVGhhdCdzIGFsbCENCg=="}'

echo
echo "${green} ${bold}✓ sls invoke local -f cx_create_campaign -d '{\"fecha\": \"2018-06-24 17:05:55\",  \"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\", \"id_cliente\": \"vidasecurity\", \"id_usuario\": \"ccortez\", \"access_key_dest\": \"AKIAIOOOOOOOOOO\",\"region_dest\": \"us-east-1\",\"secret_key_dest\": \"11111111111111111111111111111\",\"datos_api_endpoint\": {\"Arn\": \"LAMBDA_FUNCTION_ARN\", \"RoleArn\": \"IAM_ROLE_ARN\"}, \"id_segment\":\"cxseg001\", \"frequency\":\"daily\", \"start_time\":\"2018-06-24 17:05:55\", \"campaign_name\": \"campaign_test\", \"email_images\": {\"img1\": \"url1\", \"img2\":\"ulr2\"}, \"scheduleExpression\": \"cron(0 10 * * ? *)\", \"id_mailtemplate\": \"cxtemplate001\",\"replacement_data_url\": \"s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json\"}'${normal}"
echo
sls invoke local -f cx_send_mailcampaign -d '
{ "fecha": "2018-06-24 17:05:55",
  "id_app": "artucx",
  "connection_string":
  "dbname=ac user=postgres password=postgres host=localhost",
  "id_cliente": "vidasecurity",
  "id_usuario": "ccortez",
  "access_key_dest": "AKIAIOOOOOOOOOO",
  "region_dest": "us-east-1",
  "schedule_expression": "cron(0 10 * * 1,2,3,4,5,6,7 *)",
  "secret_key_dest": "11111111111111111111111111111",
  "datos_api_endpoint": {"ScheduleExpression": "cron(0 10 * * ? *)", "Arn": "LAMBDA_FUNCTION_ARN", "RoleArn": "IAM_ROLE_ARN", "SENDGRID_API_KEY": "XXXX"},
  "id_segment":"cxseg001",
  "frequency":"daily",
  "start_time":"2018-06-24 17:05:55",
  "campaign_name": "campaign_test",
  "email_images": {"img1": "url1", "img2":"ulr2"},
  "id_mailtemplate": "cxtemplate001",
  "replacement_data_url": "s3://id_app/id_cliente/id_project/id_segment/id_campaign/TemplateModel.json"
}'
