from common.db.session import db_session
from common.db.models import CxSegment, CxCampaign
from common.utils import to_postgresql
import http


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:
        segments = db.query(CxSegment).\
            filter(CxSegment.id_cliente == event["id_cliente"],
                   CxSegment.id_project == event["id_project"],
                   CxSegment.id_usuario == event["id_usuario"])

        for s in segments:
            response.append({
                "id_segment": s.id_segment,
                "id_project": s.id_project,
                "name": s.name,
                "desc": s.desc,
                "campaign_input_url":  s.campaign_input_url,
                "total_entries": s.total_entries,
                "creation_time": s.creation_time.strftime("%Y-%m-%d %H:%M:%S"),
                "id_cliente":  s.id_cliente,
                "id_usuario": s.id_usuario,
                "channel_type": s.channel_type,
                "campaign_count": db.query(CxCampaign).filter(CxCampaign.id_segment == s.id_segment).count()
            })

    return {"status": http.HTTPStatus.OK, "response": response}