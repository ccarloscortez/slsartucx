from common.db.session import db_session
from common.db.models import CxSegment, CxCampaign
from common.utils import to_postgresql, get_boto3_settings
import http
import boto3
from common.logs import logger
import ulid
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.exc import MultipleResultsFound
from cx_delete_campaign import handler as delete_campaign
import copy


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:
        # get segment
        try:
            segment = db.query(CxSegment). \
                filter(CxSegment.id_cliente == event["id_cliente"],
                       CxSegment.id_usuario == event["id_usuario"],
                       CxSegment.id_segment == event["id_segment"]).one()
        except MultipleResultsFound:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }
        except NoResultFound:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.NOT_FOUND,
                "error_id": error_id.str,
            }

        segment_response = {
            "id_segment": segment.id_segment,
        }

        # delete segment on aws
        pinpoint_client = boto3.client('pinpoint', **get_boto3_settings(event))

        try:
            pinpoint_client.delete_segment(ApplicationId=segment.id_project, SegmentId=segment.id_segment)
        except:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }

        # delete segment in the database
        db.delete(segment)

        try:
            db.commit()
        except:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }

        response.append(segment_response)

    return {"status": http.HTTPStatus.OK, "response": response}
