import os
import boto3
import ulid
import base64
import os


def to_postgresql(connection_string):
    """ transform the raw connection_string into a proper postgres url that uses the pg8000 plugin

    >>> to_postgresql("dbname=artucx_db user=artucx_user password=artucx_psw host=example.artucx.ai")
    'postgres+pg8000://example.artucx.ai:5432/artucx_db?user=artucx_user&password=artucx_psw'

    """
    cs = connection_string.split(" ")
    db = cs[0].replace("dbname=", "")
    user = cs[1].replace("user=", "")
    password = cs[2].replace("password=", "")
    host = cs[3].replace("host=", "")

    return f"postgres+pg8000://{host}:5432/{db}?user={user}&password={password}"


def get_boto3_settings(event):
    if os.getenv("ENVIRONMENT", "Production") == "Local":
        print(event["access_key_dest"] and event["region_dest"] and event["secret_key_dest"] and "")
        s3 = boto3.resource(u's3', **{
            "region_name": "us-west-1",
            "endpoint_url": "http://localhost:5000",
        })

        s3.create_bucket(Bucket="artucxstorage")
        s3.Bucket("artucxstorage").upload_file(
            os.path.join(os.path.dirname(os.path.realpath(__file__)), 'vidasecurity_train_dataset.csv'),
            'vidasecurity_train_dataset.csv')
        s3.Bucket("artucxstorage").upload_file(
            os.path.join(os.path.dirname(os.path.realpath(__file__)), 'segment.csv'),
            'segment.csv')

        return {
            "region_name": "us-west-1",
            "endpoint_url": "http://localhost:5000",
        }
    else:
        return {
            "aws_access_key_id": event["access_key_dest"],
            "aws_secret_access_key": event["secret_key_dest"],
            "region_name": event["region_dest"]
        }


def get_postmark_settings(event):
    if os.getenv("ENVIRONMENT", "Production") == "Local":
        print(event["datos_api_endpoint"]["X-Postmark-Server-Token"] and "")
        return {
            "server_token": "POSTMARK_API_TEST",
        }
    else:
        return {
            "server_token": event["datos_api_endpoint"]["X-Postmark-Server-Token"],
        }


def upload_to_s3(event, bucket=None, key=None):
    s3 = boto3.resource(u's3', **get_boto3_settings(event))
    # get a handle on the bucket that holds your file
    s3_bucket = s3.Bucket(bucket)
    s3_bucket.put_object(Body=base64.b64decode(event["content"]),
                         Key=key)
    return f"s3://{bucket}/{key}"
    pass


if __name__ == '__main__':
    import doctest

    doctest.testmod()
