# coding: utf-8
import datetime
import enum
import logging

import ulid
from sqlalchemy import Enum, Boolean, Column, DateTime, ForeignKey, String, UniqueConstraint, PrimaryKeyConstraint, \
    event
from sqlalchemy.dialects.postgresql.json import JSONB
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.types import Text

logger = logging.getLogger()

Base = declarative_base()
metadata = Base.metadata


class CxMailtemplate(Base):
    __tablename__ = 'cx_mailtemplate'
    id_mailtemplate = Column(String(255), primary_key=True)
    name = Column(String(255))
    desc = Column(String(255))
    creation_date = Column(DateTime)
    title = Column(String(255))
    body = Column(Text())
    images = Column(JSONB(astext_type=Text()))
    id_cliente = Column(String(255))
    id_usuario = Column(String(255))


class CxMlMetric(Base):
    __tablename__ = 'cx_ml_metric'
    id_metric = Column(String(255), primary_key=True)
    type = Column(String(255))
    accuracy = Column(String(255))
    fscore = Column(String(255))
    recall = Column(String(255))
    precision = Column(String(255))
    roc = Column(String(255))
    confusion_matrix = Column(JSONB(astext_type=Text()))
    roc_curve = Column(JSONB(astext_type=Text()))
    importances = Column(JSONB(astext_type=Text()))


class CxMlStatistic(Base):
    __tablename__ = 'cx_ml_statistic'

    id_statistic = Column(String(255), primary_key=True)
    report_name = Column(String(255))
    report_desc = Column(String(255))
    total_entries = Column(String(255))
    positive_level_name = Column(String(255))
    negative_level_name = Column(String(255))
    positive_level = Column(String(255))
    negative_level = Column(String(255))
    positive_level_csv = Column(String(255))
    negative_level_csv = Column(String(255))
    id_cliente = Column(String(255))
    id_usuario = Column(String(255))


class CxModel(Base):
    __tablename__ = 'cx_model'

    id_model = Column(String(255), primary_key=True)
    category = Column(String(255))
    version = Column(String(255))
    desc = Column(JSONB(astext_type=Text()))
    industry = Column(String(255))
    name = Column(String(255))
    creation_time = Column(DateTime)


class CxProject(Base):
    __tablename__ = 'cx_project'

    id_project = Column(String(255), primary_key=True)
    name = Column(String(255))
    desc = Column(String(255))
    creation_date = Column(DateTime)
    id_cliente = Column(String(255))
    id_usuario = Column(String(255))
    from_address = Column(String(255))


class CxClientModel(Base):
    __tablename__ = 'cx_client_model'
    id_cliente = Column(String(255), nullable=False)
    id_model = Column(ForeignKey('cx_model.id_model'), nullable=False)
    addition_date = Column(DateTime)
    addition_user = Column(String(255))
    __table_args__ = (PrimaryKeyConstraint('id_cliente', 'id_model'),
                      UniqueConstraint('id_cliente', 'id_model', name='unique_cx_client_model'),)
    cx_model = relationship('CxModel')


class CxMarketplace(Base):
    __tablename__ = 'cx_marketplace'

    id_marketplace = Column(String(20), primary_key=True)
    name = Column(String(20))
    id_app = Column(String(20))
    id_model = Column(ForeignKey('cx_model.id_model'))
    creation_time = Column(DateTime)

    cx_model = relationship('CxModel')


class CxSegment(Base):
    __tablename__ = 'cx_segment'

    id_segment = Column(String(255), primary_key=True)
    id_project = Column(ForeignKey('cx_project.id_project'))
    name = Column(String(255))
    desc = Column(String(255))
    campaign_input_url = Column(String(255))
    total_entries = Column(String(255))
    creation_time = Column(DateTime)
    id_cliente = Column(String(255))
    id_usuario = Column(String(255))
    channel_type = Column(String(255))

    cx_project = relationship('CxProject')


class CxTraining(Base):
    __tablename__ = 'cx_training'

    id_training = Column(String(255), primary_key=True)
    id_model = Column(ForeignKey('cx_model.id_model'))
    name = Column(String(255), default="")
    state = Column(String(255))
    type = Column(String(255))
    dataset_url_training = Column(String(255))
    saved_model = Column(String(255))
    training_label = Column(String(255))
    subtype = Column(String(255))
    modeling_data = Column(JSONB(astext_type=Text()))
    is_free = Column(String(255))
    id_cliente = Column(String(255))
    id_usuario = Column(String(255))
    id_metric = Column(ForeignKey('cx_ml_metric.id_metric'))
    creation_time = Column(DateTime)
    cx_ml_metric = relationship('CxMlMetric')
    cx_model = relationship('CxModel')


class CxCampaign(Base):
    __tablename__ = 'cx_campaign'

    id_campaign = Column(String(255), primary_key=True)
    id_segment = Column(ForeignKey('cx_segment.id_segment'))
    creation_time = Column(DateTime)
    name = Column(String(255))
    desc = Column(String(255))
    email_title = Column(String(255))
    email_body = Column(String(255))
    email_images = Column(JSONB(astext_type=Text()))
    datos_cx_campaign = Column(String(2048))
    frequency = Column(String(255))
    start_time = Column(DateTime)
    end_time = Column(DateTime)
    timezone = Column(String)
    id_cliente = Column(String(255))
    estado_envio = Column(String(255))
    ultima_ejecucion = Column(DateTime)
    id_usuario = Column(String(255))
    state = Column(Boolean, default=False)
    id_mailtemplate = Column(ForeignKey('cx_mailtemplate.id_mailtemplate'))
    replacement_data_url = Column(String(255))
    schedule_expression = Column(String(255))
    cx_mailtemplate = relationship('CxMailtemplate')
    cx_segment = relationship('CxSegment'),



class CxPrediction(Base):
    __tablename__ = 'cx_prediction'

    id_prediction = Column(String(255), primary_key=True)
    id_training = Column(ForeignKey('cx_training.id_training'))
    name = Column(String(255))
    creation_time = Column(DateTime)
    on_dashboard = Column(Boolean)
    id_job = Column(String(255))
    job_type = Column(String(255))
    job_start_time = Column(DateTime)
    job_state = Column(String(255))
    input_csv_url = Column(String(255))
    output_csv_url = Column(String(255))
    job_finish_time = Column(DateTime)
    id_cliente = Column(String(255))
    id_usuario = Column(String(255))
    id_statistic = Column(ForeignKey('cx_ml_statistic.id_statistic'))

    cx_ml_statistic = relationship('CxMlStatistic')
    cx_training = relationship('CxTraining')


class CxCampaignStat(Base):
    __tablename__ = 'cx_campaign_stats'

    id_c_stat = Column(String(255), primary_key=True)
    name = Column(String(255))
    id_campaign = Column(ForeignKey('cx_campaign.id_campaign'))
    open_rate = Column(String(255))
    delivered = Column(String(255))
    id_cliente = Column(String(255))
    id_usuario = Column(String(255))
    cx_campaign = relationship('CxCampaign')


class CxReadNotification(Base):
    __tablename__ = 'cx_read_notification'

    id_read_notification = Column(String(255), primary_key=True)
    id_usuario = Column(String(255))
    id_notification_change = Column(ForeignKey('cx_notification_change.id_notification_change', ondelete='CASCADE'))
    read_at = Column(DateTime)


class NotificationAction(enum.Enum):
    created = 1
    deleted = 2


class CxNotificationChange(Base):
    __tablename__ = 'cx_notification_change'

    id_notification_change = Column(String(255), primary_key=True)
    entity_type = Column(String(255))
    entity_id = Column(String(255))
    entity_name = Column(String(255))
    action_on_entity = Column(Enum(NotificationAction))
    actor_id_usuario = Column(String(255))
    actor_id_cliente = Column(String(255))
    created_at = Column(DateTime)
    reads = relationship(CxReadNotification, backref="notification_change", passive_deletes=True)


def event_entity_ids(mapper, target, n_change):
    if mapper.class_ == CxMailtemplate:
        n_change.actor_id_cliente = target.id_cliente
        n_change.actor_id_usuario = target.id_usuario
        n_change.entity_id = target.id_mailtemplate
        n_change.entity_name = target.name
    elif mapper.class_ == CxProject:
        n_change.actor_id_cliente = target.id_cliente
        n_change.actor_id_usuario = target.id_usuario
        n_change.entity_id = target.id_project
        n_change.entity_name = target.name
    elif mapper.class_ == CxClientModel:
        n_change.actor_id_cliente = target.id_cliente
        n_change.actor_id_usuario = target.addition_user
        n_change.entity_id = f"{target.id_cliente}_{target.id_model}"
        n_change.entity_name = target.cx_model.name
    elif mapper.class_ == CxSegment:
        n_change.actor_id_cliente = target.id_cliente
        n_change.actor_id_usuario = target.id_usuario
        n_change.entity_id = target.id_segment
        n_change.entity_name = target.name
    elif mapper.class_ == CxTraining:
        n_change.actor_id_cliente = target.id_cliente
        n_change.actor_id_usuario = target.id_usuario
        n_change.entity_id = target.id_training
        n_change.entity_name = target.name
    elif mapper.class_ == CxCampaign:
        n_change.actor_id_cliente = target.id_cliente
        n_change.actor_id_usuario = target.id_usuario
        n_change.entity_id = target.id_campaign
        n_change.entity_name = target.name
    elif mapper.class_ == CxPrediction:
        n_change.actor_id_cliente = target.id_cliente
        n_change.actor_id_usuario = target.id_usuario
        n_change.entity_id = target.id_prediction
        n_change.entity_name = target.name


def notify_create(mapper, connect, target):
    db_session = scoped_session(sessionmaker(autocommit=False, autoflush=True, bind=connect))
    n_change = CxNotificationChange(id_notification_change=ulid.new().str,
                                    entity_type=mapper.class_.__name__,
                                    created_at=datetime.datetime.now(),
                                    action_on_entity=NotificationAction.created)
    event_entity_ids(mapper, target, n_change)
    read = CxReadNotification(id_read_notification=ulid.new().str,
                              id_usuario=n_change.actor_id_usuario,
                              id_notification_change=n_change.id_notification_change,
                              read_at=datetime.datetime.now())
    db_session.add(n_change)
    db_session.add(read)
    try:
        db_session.commit()
    except Exception as e:
        logger.exception(e)


def notify_delete(mapper, connect, target):
    db_session = scoped_session(sessionmaker(autocommit=False, autoflush=True, bind=connect))
    n_change = CxNotificationChange(id_notification_change=ulid.new().str,
                                    entity_type=mapper.class_.__name__,
                                    created_at=datetime.datetime.now(),
                                    action_on_entity=NotificationAction.deleted)
    event_entity_ids(mapper, target, n_change)
    read = CxReadNotification(id_read_notification=ulid.new().str,
                              id_usuario=n_change.actor_id_usuario,
                              id_notification_change=n_change.id_notification_change,
                              read_at=datetime.datetime.now())
    db_session.add(n_change)
    db_session.add(read)

    try:
        db_session.commit()
    except Exception as e:
        logger.exception(e)


event.listen(CxMailtemplate, "after_insert", notify_create)
event.listen(CxProject, "after_insert", notify_create)
event.listen(CxClientModel, "after_insert", notify_create)
event.listen(CxSegment, "after_insert", notify_create)
event.listen(CxTraining, "after_insert", notify_create)
event.listen(CxCampaign, "after_insert", notify_create)
event.listen(CxPrediction, "after_insert", notify_create)

event.listen(CxMailtemplate, "after_delete", notify_delete)
event.listen(CxProject, "after_delete", notify_delete)
event.listen(CxClientModel, "after_delete", notify_delete)
event.listen(CxSegment, "after_delete", notify_delete)
event.listen(CxTraining, "after_delete", notify_delete)
event.listen(CxCampaign, "after_delete", notify_delete)
event.listen(CxPrediction, "after_delete", notify_delete)
