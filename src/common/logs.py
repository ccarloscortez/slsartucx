import logging
import os

logger = logging.getLogger()

if os.getenv("ENVIRONMENT", "Production") == "Production":
    logging.basicConfig()
    logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)
    logger.setLevel(logging.INFO)
else:
    logger.setLevel(logging.ERROR)

