import datetime
import http

import ulid
from common.db.models import CxMailtemplate
from common.db.session import db_session
from common.logs import logger
from common.utils import to_postgresql
from sqlalchemy.orm.exc import MultipleResultsFound
from sqlalchemy.orm.exc import NoResultFound


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:

        try:
            template = db.query(CxMailtemplate). \
                filter(CxMailtemplate.id_mailtemplate == event["id_mailtemplate"],
                       CxMailtemplate.id_cliente == event["id_cliente"],
                       CxMailtemplate.id_usuario == event["id_usuario"]).one()
        except MultipleResultsFound:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }
        except NoResultFound:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.NOT_FOUND,
                "error_id": error_id.str,
            }

        db.delete(template)
        try:
            db.commit()
        except:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }

        response.append({
            "fecha": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "id_app": event["id_app"],
            "id_cliente": template.id_cliente,
            "id_usuario": template.id_usuario,
            "id_mailtemplate": template.id_mailtemplate,
            "name": template.name,
            "desc": template.desc
        })

    return {"status": http.HTTPStatus.OK, "response": response}
