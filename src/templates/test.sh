#!/usr/bin/env sh

set -e
export ENVIRONMENT="Local"

bold='\033[1m'
normal='\033[0m'
green='\033[0;32m'

sls plugin install -n serverless-python-requirements
echo
echo "${green} ${bold}✓ sls invoke local -f cx_create_template -d '{\"fecha\": \"2018-06-24 17:05:55\", \"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\",\"id_cliente\": \"vidasecurity\", \"id_usuario\": \"ccortez\", \"datos_api_endpoint\": {\"X-Postmark-Server-Token\":\"POSTMARK_API_TEST\"}, \"name\": \"ejemplo de campaña producto\", \"title\": \"Te saludamos de {{company.name}}!\", \"body\": \"<html><body>Hola, {{name}}!<a href=\"http://artucx-eng.s3-website-us-east-1.amazonaws.com\">Visita Nuestro Producto Artu CX</a></body></html>\"}'${normal}"
echo
sls invoke local -f cx_create_template -d '
{
	"fecha": "2018-06-24 17:05:55",
	"id_app": "artucx",
	"connection_string": "dbname=ac user=postgres password=postgres host=localhost",
	"id_cliente": "vidasecurity",
	"id_usuario": "ccortez",
	"desc": "new template",
	"datos_api_endpoint": {"X-Postmark-Server-Token":"POSTMARK_API_TEST"},
	"name": "ejemplo de campaña producto",
	"title": "Te saludamos de {{company.name}}!",
	"body": "<html><body>Hola, {{name}}!<a href=\"http://artucx-eng.s3-website-us-east-1.amazonaws.com\">Visita Nuestro Producto Artu CX</a></body></html>"
}
'

echo
id_mailtemplate=`sls invoke local -f cx_create_template -d '
{
	"fecha": "2018-06-24 17:05:55",
	"id_app": "artucx",
	"connection_string": "dbname=ac user=postgres password=postgres host=localhost",
	"id_cliente": "vidasecurity",
	"id_usuario": "ccortez",
	"datos_api_endpoint": {"X-Postmark-Server-Token":"POSTMARK_API_TEST"},
	"name": "ejemplo de campaña producto",
	"title": "Te saludamos de {{company.name}}!",
	"body": "<html><body>Hola, {{name}}!<a href=\"http://artucx-eng.s3-website-us-east-1.amazonaws.com\">Visita Nuestro Producto Artu CX</a></body></html>"
}
' | grep -Po '"id_mailtemplate": "[0-9A-Z]+"' | grep -Po  '[A-Z0-9]+'`

echo "${green} ${bold}✓ sls invoke local -f cx_delete_template -d '{\"fecha\": \"2018-06-24 17:05:55\", \"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\",\"id_cliente\": \"vidasecurity\", \"id_usuario\": \"ccortez\", \"datos_api_endpoint\": {\"X-Postmark-Server-Token\":\"POSTMARK_API_TEST\"}, \"id_mailtemplate\": \"${id_mailtemplate}\"}'${normal}"
echo
sls invoke local -f cx_delete_template -d '
{
	"fecha": "2018-06-24 17:05:55",
	"id_app": "artucx",
	"connection_string": "dbname=ac user=postgres password=postgres host=localhost",
	"id_cliente": "vidasecurity",
	"id_usuario": "ccortez",
	"datos_api_endpoint": {"X-Postmark-Server-Token":"POSTMARK_API_TEST"},
	"id_mailtemplate": "'"${id_mailtemplate}"'"
}
'


echo
id_mailtemplate=`sls invoke local -f cx_create_template -d '
{
	"fecha": "2018-06-24 17:05:55",
	"id_app": "artucx",
	"connection_string": "dbname=ac user=postgres password=postgres host=localhost",
	"id_cliente": "vidasecurity",
	"id_usuario": "ccortez",
	"datos_api_endpoint": {"X-Postmark-Server-Token":"POSTMARK_API_TEST"},
	"name": "ejemplo de campaña producto",
	"title": "Te saludamos de {{company.name}}!",
	"body": "<html><body>Hola, {{name}}!<a href=\"http://artucx-eng.s3-website-us-east-1.amazonaws.com\">Visita Nuestro Producto Artu CX</a></body></html>"
}
' | grep -Po '"id_mailtemplate": "[0-9A-Z]+"' | grep -Po  '[A-Z0-9]+'`
echo "${green} ${bold}✓ sls invoke local -f cx_edit_template -d '{\"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\",\"id_cliente\": \"vidasecurity\", \"id_usuario\": \"ccortez\"}'${normal}"
echo
sls invoke local -f cx_edit_template -d '
{
	"id_app": "artucx",
	"id_cliente": "vidasecurity",
	"connection_string": "dbname=ac user=postgres password=postgres host=localhost",
	"id_usuario": "ccortez",
    "id_mailtemplate": "cxtemplate001",
    "name": "cambio"

}
'
