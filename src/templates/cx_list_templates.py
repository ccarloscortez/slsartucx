from common.db.session import db_session
from common.db.models import CxMailtemplate, CxCampaign
from common.utils import to_postgresql
import http


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:
        templates = db.query(CxMailtemplate). \
            filter(CxMailtemplate.id_cliente == event["id_cliente"],
                   CxMailtemplate.id_usuario == event["id_usuario"])

        for template in templates:
            response.append({
                "creation_date": template.creation_date.strftime("%Y-%m-%d %H:%M:%S"),
                "id_cliente": template.id_cliente,
                "id_usuario": template.id_usuario,
                "id_mailtemplate": template.id_mailtemplate,
                "name": template.name,
                "desc": template.desc,
                "title": template.title,
                "body": template.body,
                "images": template.images,
                "campaign_count": db.query(CxCampaign).filter(
                    CxCampaign.id_mailtemplate == template.id_mailtemplate).count()

            })

    return {"status": http.HTTPStatus.OK, "response": response}
