import datetime
import http

import ulid
from common.db.models import CxMailtemplate
from common.db.session import db_session
from common.logs import logger
from common.utils import to_postgresql


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:
        template = CxMailtemplate(creation_date=datetime.datetime.now(),
                                  id_mailtemplate=ulid.new().str,
                                  name=event["name"],
                                  desc=event.get("desc", ""),
                                  title=event["title"],
                                  body=event["body"],
                                  id_cliente=event["id_cliente"],
                                  id_usuario=event["id_usuario"])

        db.add(template)
        try:
            db.commit()
        except:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }

        response.append({
            "fecha": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "id_app": event["id_app"],
            "creation_date": template.creation_date.strftime("%Y-%m-%d %H:%M:%S"),
            "id_cliente": template.id_cliente,
            "id_usuario": template.id_usuario,
            "id_mailtemplate": template.id_mailtemplate,
            "name": template.name,
            "desc": template.desc,
            "title": template.title,
            "body": template.body,
            "images": template.images,
            "campaign_count": 0,
        })

    return {"status": http.HTTPStatus.OK, "response": response}
