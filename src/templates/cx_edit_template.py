from common.db.session import db_session
from common.db.models import CxMailtemplate, CxCampaign
from common.utils import to_postgresql, get_postmark_settings
import http
import ulid
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.exc import MultipleResultsFound
from common.logs import logger


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:

        try:
            template = db.query(CxMailtemplate). \
                filter(CxMailtemplate.id_mailtemplate == event["id_mailtemplate"],
                       CxMailtemplate.id_cliente == event["id_cliente"],
                       CxMailtemplate.id_usuario == event["id_usuario"]).one()
        except MultipleResultsFound:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }
        except NoResultFound:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.NOT_FOUND,
                "error_id": error_id.str,
            }

        template.name = event.get("name", template.name)
        template.desc = event.get("desc", template.desc)
        template.title = event.get("title", template.title)
        template.body = event.get("body", template.body)

        try:
            db.commit()
        except:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.NOT_FOUND,
                "error_id": error_id.str,
            }

        response.append({
            "creation_date": template.creation_date.strftime("%Y-%m-%d %H:%M:%S"),
            "id_cliente": template.id_cliente,
            "id_usuario": template.id_usuario,
            "id_mailtemplate": template.id_mailtemplate,
            "name": template.name,
            "desc": template.desc,
            "title": template.title,
            "body": template.body,
            "images": template.images,
            "campaign_count": db.query(CxCampaign).filter(CxCampaign.id_mailtemplate == template.id_mailtemplate).count()
        })

    return {"status": http.HTTPStatus.OK, "response": response}
