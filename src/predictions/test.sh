#!/usr/bin/env sh

export ENVIRONMENT="Local"

set -e

bold='\033[1m'
normal='\033[0m'
green='\033[0;32m'

sls plugin install -n serverless-python-requirements
echo
echo "${green} ${bold}✓ sls invoke local -f cx_list_predictions -d '{\"fecha\": \"2018-06-24 17:05:55\", \"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\", \"id_cliente\": \"vidasecurity\", \"id_prediction\": \"cxpred001\", \"id_statistic\": \"cxstat001\", \"id_training\":\"cxtrain001\", \"id_usuario\": \"ccortez\"}'${normal}"
echo
sls invoke local -f cx_list_predictions -d '{"fecha": "2018-06-24 17:05:55", "id_app": "artucx", "connection_string": "dbname=ac user=postgres password=postgres host=localhost", "id_cliente": "vidasecurity", "id_prediction": "cxpred001", "id_statistic": "cxstat001", "id_training":"cxtrain001", "id_usuario": "ccortez"}'


echo
echo "${green} ${bold}✓ sls invoke local -f cx_prediction_state -d '{\"fecha\": \"2018-06-24 17:05:55\", \"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\", \"id_cliente\": \"vidasecurity\", \"id_prediction\": \"cxpred001\", \"id_statistic\": \"cxstat001\", \"id_usuario\": \"ccortez\"}'${normal}"
echo
sls invoke local -f cx_prediction_state -d '{"fecha": "2018-06-24 17:05:55", "id_app": "artucx", "connection_string": "dbname=ac user=postgres password=postgres host=localhost", "id_cliente": "vidasecurity", "id_prediction": "cxpred001", "id_statistic": "cxstat001", "id_usuario": "ccortez"}'


echo
echo "${green} ${bold}✓ sls invoke local -f cx_prediction_results -d '{\"fecha\": \"2018-06-24 17:05:55\", \"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\", \"id_cliente\": \"vidasecurity\", \"id_prediction\": \"cxpred001\", \"id_statistic\": \"cxstat001\", \"id_usuario\": \"ccortez\"}'${normal}"
echo
sls invoke local -f cx_prediction_results -d '{"fecha": "2018-06-24 17:05:55", "id_app": "artucx", "connection_string": "dbname=ac user=postgres password=postgres host=localhost", "id_cliente": "vidasecurity", "id_prediction": "cxpred001", "id_statistic": "cxstat001", "id_usuario": "ccortez"}'


echo
echo "${green} ${bold}✓ sls invoke local -f cx_create_prediction -d '{\"fecha\": \"2018-06-24 17:05:55\", \"id_app\": \"artucx\", \"connection_string\": \"dbname=ac user=postgres password=postgres host=localhost\",  \"id_usuario\": \"ccortez\", \"id_cliente\": \"vidasecurity\", \"id_training\":\"cxtrain001\", \"name\":\"cxtrain001\", \"input_csv_url\":\"s3://artucx/predictions/cxpred001/input.csv\", \"job_type\": \"batch\", \"job_start_time\": \"2018-06-24 17:05:55\", \"job_state\": \"in progress\"}'${normal}"
echo
sls invoke local -f cx_create_prediction -d '{"access_key_dest": "AKIAIOOOOOOOOOO", "region_dest": "us-east-1", "secret_key_dest": "11111111111111111111111111111", "fecha": "2018-06-24 17:05:55", "id_app": "artucx", "connection_string": "dbname=ac user=postgres password=postgres host=localhost", "id_usuario": "ccortez", "id_cliente": "vidasecurity", "id_training":"cxtrain001", "name":"cxtrain001", "input_csv_url":"s3://artucx/predictions/cxpred001/input.csv", "job_type": "batch", "job_start_time": "2018-06-24 17:05:55", "job_state": "in progress", "datos_api_endpoint": {"FunctionName": "FunctionName"}}'


echo
echo "${green} ${bold}✓ sls invoke local -f cx_uploads3_predictinput -d '{\"access_key_dest\": \"AKIAIOOOOOOOOOO\", \"region_dest\": \"us-east-1\", \"secret_key_dest\": \"11111111111111111111111111111\", \"id_cliente\": \"vidasecurity\", \"name\": \"test\", \"extension\": \"csv\", \"content\": \"QmVnaW4gd2l0aCB0aGUgInR5cGUgKG9yIHBhc3RlKSBoZXJlLi4uIiBhcmVhIHRvIGVudGVyIHlvdXIgZGF0YSwgdGhlbiBoaXQgdGhlICJlbmNvZGUiIG9yICJkZWNvZGUiIGJ1dHRvbnMgcmVzcGVjdGl2ZWx5LiBBZnRlciBhIGJsaW5rIG9mIGFueSBleWUsIHRoZSByZXN1bHRzIHdpbGwgYmUgc2hvd24gYmVsb3cgdGhlc2UgYnV0dG9ucy4gVGhhdCdzIGFsbCENCg==\"}'${normal}"
echo
sls invoke local -f cx_uploads3_predictinput -d '{"access_key_dest": "AKIAIOOOOOOOOOO", "region_dest": "us-east-1", "secret_key_dest": "11111111111111111111111111111", "id_cliente": "vidasecurity", "name": "test", "extension": "csv", "content": "QmVnaW4gd2l0aCB0aGUgInR5cGUgKG9yIHBhc3RlKSBoZXJlLi4uIiBhcmVhIHRvIGVudGVyIHlvdXIgZGF0YSwgdGhlbiBoaXQgdGhlICJlbmNvZGUiIG9yICJkZWNvZGUiIGJ1dHRvbnMgcmVzcGVjdGl2ZWx5LiBBZnRlciBhIGJsaW5rIG9mIGFueSBleWUsIHRoZSByZXN1bHRzIHdpbGwgYmUgc2hvd24gYmVsb3cgdGhlc2UgYnV0dG9ucy4gVGhhdCdzIGFsbCENCg=="}'


echo
echo "${green} ${bold}✓ sls invoke local -f cx_predict -d '{\"access_key_dest\": \"AKIAIOOOOOOOOOO\", \"region_dest\": \"us-east-1\", \"secret_key_dest\": \"11111111111111111111111111111\", \"id_cliente\": \"vidasecurity\", \"name\": \"test\", \"extension\": \"csv\", \"content\": \"QmVnaW4gd2l0aCB0aGUgInR5cGUgKG9yIHBhc3RlKSBoZXJlLi4uIiBhcmVhIHRvIGVudGVyIHlvdXIgZGF0YSwgdGhlbiBoaXQgdGhlICJlbmNvZGUiIG9yICJkZWNvZGUiIGJ1dHRvbnMgcmVzcGVjdGl2ZWx5LiBBZnRlciBhIGJsaW5rIG9mIGFueSBleWUsIHRoZSByZXN1bHRzIHdpbGwgYmUgc2hvd24gYmVsb3cgdGhlc2UgYnV0dG9ucy4gVGhhdCdzIGFsbCENCg==\"}'${normal}"
echo
sls invoke local -f cx_predict -d '{"access_key_dest": "AKIAIOOOOOOOOOO", "region_dest": "us-east-1", "secret_key_dest": "11111111111111111111111111111", "id_cliente": "vidasecurity", "id_usuario": "ccortez", "id_prediction": "cxpred001", "connection_string": "dbname=ac user=postgres password=postgres host=localhost"}'
