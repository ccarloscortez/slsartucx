from common.db.session import db_session
from common.db.models import CxPrediction
from common.logs import logger
from common.utils import to_postgresql
import http
import ulid
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.exc import MultipleResultsFound
import datetime


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:
        try:
            prediction = db.query(CxPrediction). \
                filter(CxPrediction.id_cliente == event["id_cliente"],
                       CxPrediction.id_usuario == event["id_usuario"],
                       CxPrediction.id_prediction == event["id_prediction"]).one()
        except MultipleResultsFound:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }
        except NoResultFound:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.NOT_FOUND,
                "error_id": error_id.str,
            }
        prediction.on_dashboard = not prediction.on_dashboard;

        try:
            db.commit()
        except MultipleResultsFound:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }

        response.append({
            "fecha": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "id_prediction": prediction.id_prediction,
            "on_dashboard": prediction.on_dashboard,
            "id_cliente": prediction.id_cliente,
            "id_usuario": prediction.id_usuario
        })

    return {"status": http.HTTPStatus.OK, "response": response}
