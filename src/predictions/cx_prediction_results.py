from common.db.session import db_session
from common.db.models import CxPrediction
from common.logs import logger
from common.utils import to_postgresql
import http
import ulid
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.exc import MultipleResultsFound


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:
        try:
            prediction = db.query(CxPrediction).\
                filter(CxPrediction.id_cliente == event["id_cliente"],
                       CxPrediction.id_usuario == event["id_usuario"],
                       CxPrediction.id_prediction == event["id_prediction"]).one()
        except MultipleResultsFound:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }
        except NoResultFound:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.NOT_FOUND,
                "error_id": error_id.str,
            }
        response.append({
            "id_prediction": prediction.id_prediction,
            "id_statistic": prediction.id_statistic,
            "report_name": prediction.cx_ml_statistic.report_name,
            "report_desc": prediction.cx_ml_statistic.report_desc,
            "total_entries": prediction.cx_ml_statistic.total_entries,
            "positive_level_name": prediction.cx_ml_statistic.positive_level_name,
            "negative_level_name": prediction.cx_ml_statistic.negative_level_name,
            "positive_level": prediction.cx_ml_statistic.positive_level,
            "negative_level": prediction.cx_ml_statistic.negative_level,
            "positive_level_csv": prediction.cx_ml_statistic.positive_level_csv,
            "negative_level_csv": prediction.cx_ml_statistic.negative_level_csv,
            "id_cliente": prediction.cx_ml_statistic.id_cliente,
            "id_usuario": prediction.cx_ml_statistic.id_usuario,
        })

    return {"status": http.HTTPStatus.OK, "response": response}
