from common.db.session import db_session
from common.db.models import CxPrediction
from common.logs import logger
from common.utils import to_postgresql, get_boto3_settings
import http
import ulid
import datetime
import boto3
import json


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:

        prediction = CxPrediction(id_prediction=ulid.new().str,
                                  id_training=event["id_training"],
                                  name=event["name"],
                                  creation_time=datetime.datetime.now(),
                                  on_dashboard=False,
                                  id_job=ulid.new().str,
                                  job_type="batch",
                                  job_start_time=datetime.datetime.now(),
                                  job_state="in progress",
                                  id_cliente=event["id_cliente"],
                                  id_usuario=event["id_usuario"],
                                  input_csv_url=event["input_csv_url"]
                                  )

        db.add(prediction)
        kinesis_client = boto3.client(u'kinesis', **get_boto3_settings(event))
        event["id_prediction"] = prediction.id_prediction
        kinesis_client.put_record(
            StreamName='artucx-prediction-stream',
            Data=json.dumps(event),
            PartitionKey='shardId-000000000000',
        )

        try:
            db.commit()
        except:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")
            return {
                "status": http.HTTPStatus.INTERNAL_SERVER_ERROR,
                "error_id": error_id.str,
            }

        response.append({
            "id_prediction": prediction.id_prediction,
            "id_training": prediction.id_training,
            "name": prediction.name,
            "creation_time": prediction.creation_time.strftime("%Y-%m-%d %H:%M:%S"),
            "on_dashboard": prediction.on_dashboard,
            "id_job": prediction.id_job,
            "job_type": prediction.job_type,
            "job_start_time": prediction.job_start_time.strftime("%Y-%m-%d %H:%M:%S"),
            "job_state": prediction.job_state,
            "input_csv_url": prediction.input_csv_url,
            "output_csv_url": prediction.output_csv_url,
            "job_finish_time": prediction.job_finish_time.strftime(
                "%Y-%m-%d %H:%M:%S") if prediction.job_finish_time else None,
            "id_cliente": prediction.id_cliente,
            "id_usuario": prediction.id_usuario,
            "id_statistic": prediction.id_statistic
        })

    return {"status": http.HTTPStatus.OK, "response": response}
