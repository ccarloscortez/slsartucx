from common.db.session import db_session
from common.db.models import CxPrediction
from common.logs import logger
from common.utils import to_postgresql
import http


def handler(event, context):
    response = []
    with db_session(to_postgresql(event["connection_string"])) as db:
        predictions = db.query(CxPrediction).\
            filter(CxPrediction.id_cliente == event["id_cliente"],
                   CxPrediction.id_usuario == event["id_usuario"],
                   CxPrediction.id_training == event["id_training"])

        for p in predictions:
            response.append({
                "id_prediction": p.id_prediction,
                "id_training": p.id_training,
                "name": p.name,
                "creation_time":  p.creation_time.strftime("%Y-%m-%d %H:%M:%S"),
                "on_dashboard": p.on_dashboard,
                "id_job": p.id_job,
                "job_type": p.job_type,
                "job_start_time": p.job_start_time.strftime("%Y-%m-%d %H:%M:%S"),
                "job_state": p.job_state,
                "input_csv_url": p.input_csv_url,
                "output_csv_url": p.output_csv_url,
                "job_finish_time": p.job_finish_time.strftime("%Y-%m-%d %H:%M:%S") if p.job_finish_time else None,
                "id_cliente": p.id_cliente,
                "id_usuario": p.id_usuario,
                "id_statistic": p.id_statistic,
            })

    return {"status": http.HTTPStatus.OK, "response": response}
