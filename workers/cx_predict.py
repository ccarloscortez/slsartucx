#!/usr/bin/env python3
import datetime
import io
import json
import pickle
import time
from urllib.parse import urlparse

import boto3
import pandas as pd
import ulid
from amazon_kclpy import kcl
from amazon_kclpy.v2 import processor
from common.db.models import CxPrediction
from common.db.session import db_session
from common.logs import logger
from common.utils import to_postgresql, get_boto3_settings
from sklearn.preprocessing import StandardScaler


class RecordProcessor(processor.RecordProcessorBase):
    """
    A RecordProcessor processes data from a shard in a stream. Its methods will be called with this pattern:

    * initialize will be called once
    * process_records will be called zero or more times
    * shutdown will be called if this MultiLangDaemon instance loses the lease to this shard, or the shard ends due
        a scaling change.
    """

    def __init__(self):
        self._SLEEP_SECONDS = 10
        self._CHECKPOINT_RETRIES = 5
        self._CHECKPOINT_FREQ_SECONDS = 60
        self._largest_seq = (None, None)
        self._largest_sub_seq = None
        self._last_checkpoint_time = None

    def initialize(self, initialize_input):
        """
        Called once by a KCLProcess before any calls to process_records

        :param amazon_kclpy.messages.InitializeInput initialize_input: Information about the lease that this record
            processor has been assigned.
        """
        self._largest_seq = (None, None)
        self._last_checkpoint_time = time.time()

    def checkpoint(self, checkpointer, sequence_number=None, sub_sequence_number=None):
        """
        Checkpoints with retries on retryable exceptions.

        :param amazon_kclpy.kcl.Checkpointer checkpointer: the checkpointer provided to either process_records
            or shutdown
        :param str or None sequence_number: the sequence number to checkpoint at.
        :param int or None sub_sequence_number: the sub sequence number to checkpoint at.
        """
        for n in range(0, self._CHECKPOINT_RETRIES):
            try:
                checkpointer.checkpoint(sequence_number, sub_sequence_number)
                return
            except kcl.CheckpointError as e:
                if 'ShutdownException' == e.value:
                    #
                    # A ShutdownException indicates that this record processor should be shutdown. This is due to
                    # some failover event, e.g. another MultiLangDaemon has taken the lease for this shard.
                    #
                    raise Exception('Encountered shutdown exception, skipping checkpoint')
                elif 'ThrottlingException' == e.value:
                    #
                    # A ThrottlingException indicates that one of our dependencies is is over burdened, e.g. too many
                    # dynamo writes. We will sleep temporarily to let it recover.
                    #
                    if self._CHECKPOINT_RETRIES - 1 == n:
                        raise Exception('Failed to checkpoint after {n} attempts, giving up.\n'.format(n=n))
                    else:
                        logger.info('Was throttled while checkpointing, will attempt again in {s} seconds'
                                    .format(s=self._SLEEP_SECONDS))
                elif 'InvalidStateException' == e.value:
                    logger.error('MultiLangDaemon reported an invalid state while checkpointing.\n')
                else:  # Some other error
                    logger.error('Encountered an error while checkpointing, error was {e}.\n'.format(e=e))
            time.sleep(self._SLEEP_SECONDS)

    def process_record(self, data, partition_key, sequence_number, sub_sequence_number):
        """
        Called for each record that is passed to process_records.

        :param str data: The blob of data that was contained in the record.
        :param str partition_key: The key associated with this recod.
        :param int sequence_number: The sequence number associated with this record.
        :param int sub_sequence_number: the sub sequence number associated with this record.
        """
        try:
            context = {}
            event = json.loads(data.decode('utf-8'))
            logger.debug(event)
            with db_session(to_postgresql(event["connection_string"])) as db:
                prediction = db.query(CxPrediction).filter(CxPrediction.id_prediction == event["id_prediction"]).one()

                # load prediction data
                prediction_csv_url = urlparse(prediction.input_csv_url)
                s3 = boto3.client(u's3', **get_boto3_settings(event))
                prediction_obj = s3.get_object(Bucket=prediction_csv_url.netloc, Key=prediction_csv_url.path.strip('/'))
                prediction_df = pd.read_csv(io.BytesIO(prediction_obj['Body'].read()), index_col=0, encoding='latin-1')
                prediction_df['label'] = 'score'

                # load training data
                training_csv_url = urlparse(prediction.cx_training.dataset_url_training)
                training_obj = s3.get_object(Bucket=training_csv_url.netloc, Key=training_csv_url.path.strip('/'))
                training_df = pd.read_csv(io.BytesIO(training_obj['Body'].read()), index_col=0, encoding='latin-1')
                column_names_for_onehot = training_df.select_dtypes(include=['object']).columns

                training_df['label'] = 'train'

                # Concat
                concat_df = pd.concat([training_df, prediction_df])

                features_df = pd.get_dummies(concat_df, columns=column_names_for_onehot, drop_first=True)

                # Split your data
                train_df = features_df[features_df['label'] == 'train']
                score_df = features_df[features_df['label'] == 'score']

                # Drop your labels
                train_df = train_df.drop('label', axis=1)
                score_df = score_df.drop('label', axis=1)
                X = score_df
                df = prediction_df
                # Feature Scaling
                sc = StandardScaler()
                X = sc.fit_transform(X)

                pickle_url = urlparse(prediction.cx_training.saved_model)
                obj = s3.get_object(Bucket=pickle_url.netloc, Key=pickle_url.path.strip('/'))

                loaded_model = pickle.loads(obj['Body'].read())

                # Making prediction based on loaded model
                y_pred = loaded_model.predict(X)
                # Mapping the predicted value back to the test data and copying the 'STSPOL' as 'Original_STSPOL'
                column_name = f"Predicted[{prediction.cx_training.training_label}]"
                df[column_name] = pd.DataFrame(y_pred)
                # Labeling the predicted value as 1 = 'ANU' and 0 = 'ACT'
                df[column_name] = df[column_name].replace(1, "ANU")
                df[column_name] = df[column_name].replace(0, "ACT")

                csv_buffer = io.StringIO()
                df.to_csv(csv_buffer)
                date = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
                bucket = "artucxstorage"
                key = f'predictions/output/predictionoutput-{prediction.id_cliente}-{prediction.id_prediction}-{date}.csv'
                s3 = boto3.resource(u's3', **get_boto3_settings(event))
                s3.Object(bucket, key).put(Body=csv_buffer.getvalue())

                prediction.job_state = "finished"
                prediction.output_csv_url = f"s3://{bucket}/{key}"
                prediction.job_finish_time = datetime.datetime.now()
                db.commit()
                logger.info(f"prediction: {prediction.id_prediction} done successfully")
        except:
            error_id = ulid.new()
            logger.exception(f"id_error:  {error_id.str}, event: {event}, context:  {repr(context)}")

    def should_update_sequence(self, sequence_number, sub_sequence_number):
        """
        Determines whether a new larger sequence number is available

        :param int sequence_number: the sequence number from the current record
        :param int sub_sequence_number: the sub sequence number from the current record
        :return boolean: true if the largest sequence should be updated, false otherwise
        """
        return self._largest_seq == (None, None) or sequence_number > self._largest_seq[0] or \
               (sequence_number == self._largest_seq[0] and sub_sequence_number > self._largest_seq[1])

    def process_records(self, process_records_input):
        """
        Called by a KCLProcess with a list of records to be processed and a checkpointer which accepts sequence numbers
        from the records to indicate where in the stream to checkpoint.

        :param amazon_kclpy.messages.ProcessRecordsInput process_records_input: the records, and metadata about the
            records.
        """
        logger.info('Processing {} records'.format(len(process_records_input.records)))
        try:
            for record in process_records_input.records:
                data = record.binary_data
                seq = int(record.sequence_number)
                sub_seq = record.sub_sequence_number
                key = record.partition_key
                self.process_record(data, key, seq, sub_seq)
                if self.should_update_sequence(seq, sub_seq):
                    self._largest_seq = (seq, sub_seq)

            #
            # Checkpoints every self._CHECKPOINT_FREQ_SECONDS seconds
            #
            if time.time() - self._last_checkpoint_time > self._CHECKPOINT_FREQ_SECONDS:
                self.checkpoint(process_records_input.checkpointer, str(self._largest_seq[0]), self._largest_seq[1])
                self._last_checkpoint_time = time.time()

        except Exception as e:
            logger.error("Encountered an exception while processing records. Exception was {e}\n".format(e=e))

    def shutdown(self, shutdown_input):
        """
        Called by a KCLProcess instance to indicate that this record processor should shutdown. After this is called,
        there will be no more calls to any other methods of this record processor.

        As part of the shutdown process you must inspect :attr:`amazon_kclpy.messages.ShutdownInput.reason` to
        determine the steps to take.

            * Shutdown Reason ZOMBIE:
                **ATTEMPTING TO CHECKPOINT ONCE A LEASE IS LOST WILL FAIL**

                A record processor will be shutdown if it loses its lease.  In this case the KCL will terminate the
                record processor.  It is not possible to checkpoint once a record processor has lost its lease.
            * Shutdown Reason TERMINATE:
                **THE RECORD PROCESSOR MUST CHECKPOINT OR THE KCL WILL BE UNABLE TO PROGRESS**

                A record processor will be shutdown once it reaches the end of a shard.  A shard ending indicates that
                it has been either split into multiple shards or merged with another shard.  To begin processing the new
                shard(s) it's required that a final checkpoint occurs.


        :param amazon_kclpy.messages.ShutdownInput shutdown_input: Information related to the shutdown request
        """
        if shutdown_input.reason == 'TERMINATE':
            # Checkpointing with no parameter will checkpoint at the
            # largest sequence number reached by this processor on this
            # shard id
            logger.info('Was told to terminate, will attempt to checkpoint.')
            self.checkpoint(shutdown_input.checkpointer, None)
        else:  # reason == 'ZOMBIE'
            logger.info('Shutting down due to failover. Will not checkpoint.')


if __name__ == "__main__":
    kclprocess = kcl.KCLProcess(RecordProcessor())
    kclprocess.run()
