#!/usr/bin/env python3
import datetime
import io
import pickle
from urllib.parse import urlparse

import boto3
import pandas as pd
import ulid
from common.db.models import CxTraining, CxMlMetric
from common.db.session import db_session
from common.logs import logger
from common.utils import to_postgresql, get_boto3_settings
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, roc_curve, auc
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler


def handler(event):
    with db_session(to_postgresql(event["connection_string"])) as db:
        training = db.query(CxTraining).filter(CxTraining.id_training == event["id_training"]).one()

        csv_url = urlparse(training.dataset_url_training)
        s3 = boto3.client(u's3', **get_boto3_settings(event))
        obj = s3.get_object(Bucket=csv_url.netloc, Key=csv_url.path.strip('/'))
        df = pd.read_csv(io.BytesIO(obj['Body'].read()), index_col=0, encoding='latin-1')

        column_names_for_onehot = list(df.select_dtypes(include=['object']).columns.values)
        column_names_for_onehot.remove(training.training_label)

        if "Address" in column_names_for_onehot:
            column_names_for_onehot.remove("Address")
        if "ChannelType" in column_names_for_onehot:
            column_names_for_onehot.remove("ChannelType")
        if "ID" in column_names_for_onehot:
            column_names_for_onehot.remove("ID")

        # column_names_for_onehot.remove(training.training_label)
        Xdf = pd.get_dummies(df[column_names_for_onehot], columns=column_names_for_onehot, drop_first=True)
        # Feature Scaling
        sc = StandardScaler()
        X = sc.fit_transform(Xdf)

        # Fitting Random Forest Model to the training dataset
        classifier = RandomForestClassifier(n_estimators=1000,
                                            criterion='gini',
                                            max_features=20,
                                            random_state=0,
                                            max_depth=3)

        y = pd.get_dummies(df[training.training_label], columns=[training.training_label], drop_first=True)
        # shuffle and split training and test sets
        X_train, X_test, y_train, y_test = train_test_split(X, y.values.ravel(), test_size=.5, random_state=0)
        y_score = classifier.fit(X_train, y_train).predict(X_test)

        cm = confusion_matrix(y_test, y_score)
        TN = cm[0][0]
        FN = cm[1][0]
        TP = cm[1][1]
        FP = cm[0][1]

        accuracy = (TP + TN) / (TP + TN + FN + FP)
        precision = TP / (TP + FP)
        recall = TP / (TP + FN)
        specificity = TN / (TN + FP)
        Fscore = 2 * (precision * recall) / (precision + recall)

        pickle_byte_obj = pickle.dumps(classifier)
        s3 = boto3.resource(u's3', **get_boto3_settings(event))

        # get a handle on the bucket that holds your file
        bucket = "artucxstorage"
        s3_bucket = s3.Bucket(bucket)
        date = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
        key = f'trainings/output/{training.id_cliente}-{training.id_training}-{date}.pkl'

        s3_bucket.put_object(Body=pickle_byte_obj,
                             Key=key)
        # importance chart
        pre_importances = dict(zip(list(Xdf.columns.values), list(classifier.feature_importances_)))
        importances = {}
        for k, v in pre_importances.items():
            for co in column_names_for_onehot:
                if k.startswith(co):
                    if co not in importances:
                        importances[co] = 0
                    importances[co] = importances[co] + v

        fpr, tpr, _ = roc_curve(y_test, y_score)

        roc = {
            "fpr": list(fpr),
            "tpr": list(tpr),
            "aoc": auc(fpr, tpr)
        }

        metric = CxMlMetric(id_metric=ulid.new().str,
                            importances=importances,
                            roc_curve=roc,
                            accuracy=accuracy,
                            precision=precision,
                            type="training",
                            recall=recall,
                            fscore=Fscore,
                            confusion_matrix={"fn": float(FN),
                                              "fp": float(FP),
                                              "tn": float(TN),
                                              "tp": float(TP)},
                            roc=specificity)
        db.add(metric)
        training.id_metric = metric.id_metric
        training.state = "ready"
        training.saved_model = f"s3://{bucket}/{key}"
        db.commit()
        logger.info(f"training: {training.id_training} done successfully")


if __name__ == "__main__":
    event = {"connection_string": "dbname=ac user=postgres password=postgres host=localhost",
             "id_cliente": "vidasecurity",
             "id_usuario": "cristobal",
             "id_training": "cxtrain001",
             "access_key_dest": "AKIAIOOOOOOOOOO",
             "region_dest": "us-east-1",
             "secret_key_dest": "1111111111111111111111111111111111111111",
             }

    handler(event)
