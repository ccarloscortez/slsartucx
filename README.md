
# invoke all the functions locally

### requirements:
- docker
- psql
-moto

go to `src folder` and setup a docker container of postgres, create a new database called `ac` then load the `db.sql` file to the new postgres instance

```ssh
cd src
docker run -d -p 5432:5432 postgres
psql -c 'create database ac;' --username=postgres --host=localhost --port=5432 
psql --dbname=ac --username=postgres --host=localhost --port=5432 < db.sql
pip install -r ../requirements/dev.txt
pip install -r ../requirements/prod.txt
```
run moto in another terminal typing `moto_server`

after all is setup run `sh test.sh` and all the function will be tested using `sls invoke local`


# manual testing 

some function are to complex to test automatically due the use of  other aws apis, or external integrations.
 
in those case the best is test them in the aws console, here is the list of function that need to be tested manually


- campaigns:  `cx_create_project`,  `cx_create_segment`
- indicators: `cx_create_smart_campaign`

# deploy


go to `src folder` and run `sh deploy.sh` this will update every function in the aws console
